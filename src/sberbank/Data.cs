﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Sber
{
    public class AccauntInfo
    {
        public string partner_key;
        public string login;
        public string password;

        public static readonly AccauntInfo Test = new AccauntInfo
        {
             partner_key = "B2C26A4E-C09A-4C85-82DB-DBA16D73AA7E"
            ,login = "izhvva"
            ,password = "RITizhvva1976"
        };
    }

    public class Authorization
    {
        public string INN;
        public string KPP;
        public string buFullName;
        public string buID;
        public string UniqueToken;
        public string PersonFullName;
        public string Indetifkey;

        public static Authorization ReadFromXmlString(string xml_text)
        {
            using (StringReader reader = new StringReader(xml_text))
            using (System.Xml.XmlReader xreader = System.Xml.XmlReader.Create(reader))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Authorization));
                return serializer.Deserialize(xreader) as Authorization;
            }
        }
    }

    public static class actionCode
    {
        public const string ActionPagedListGetMy=   "ActionPagedListGetMy";
        public const string PurchaseCreate=         "PurchaseCreate";
        public const string PurchaseCancel =        "PurchaseCancel";
    }

    public static class TSCode
    {
        public static string Bankruptcy = "Bankruptcy";
    }

    public static class purchaseTypeId
    {
        public static long Открытый_аукцион_с_открытыми_ценами = 11;
    }

    public static class buFace
    {
        public static int Тип_организации_текущего_пользователя = 3;
    }
}
