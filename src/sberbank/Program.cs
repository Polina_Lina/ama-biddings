﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace Sber
{
    class Program
    {
        static void Main(string[] args)
        {
            if (0 == args.Length)
            {
            }
            else
            {
                try
                {
                    ExecuteCommand(args);
                }
                catch (Exception ex)
                {
                    Console.Write(ex);
                }
            }
        }

        static void ExecuteCommand(string[] args)
        {
            string cmd = args[0];
            switch (cmd)
            {
                case "test_auth": TestAuthorize(); break;
                case "test_auth_pub": TestAuthorizePublic(); break;
                case "test_get_actions": TestGetActions(); break;
                case "test_get_xsd": TestGetXSD(args[1], args[2]); break;
                case "test_register_document": TestRegisterDocument(args[1]); break;
                case "test_cancel_document": TestCancelDocument(args[1]); break;
            }
        }

        static void Trace(Ast.WebServiceResponse responce)
        {
            Console.WriteLine("Status={0}",responce.Status);
            Console.WriteLine();
            Console.WriteLine("Description:");
            Console.WriteLine(responce.Description);
            if (!string.IsNullOrEmpty(responce.Response))
            {
                Console.WriteLine();
                Console.WriteLine("Response:");
                Console.WriteLine(responce.Response.Replace("\r\n", "\n").Replace("\n", "\r\n").Replace("\r\n                                    ", "\r\n"));
            }
        }

        static string FixVolatileXmlField(string xml_text, string field_name)
        {
            string txt_start = "<" + field_name + ">";
            string txt_stop = "</" + field_name + ">";
            int istart = xml_text.IndexOf(txt_start);
            if (-1 != istart)
            {
                int istop = xml_text.IndexOf(txt_stop, istart + txt_start.Length);
                if (-1 != istop)
                {
                    return xml_text.Substring(0, istart + txt_start.Length) +
                       new String('?',istop-istart-txt_start.Length) + 
                        xml_text.Substring(istop, xml_text.Length - istop);
                }
            }
            return xml_text;
        }

        static void TestAuthorize()
        {
            Console.OutputEncoding = Encoding.GetEncoding("Windows-1251");
            Ast.CommonService service = new Ast.CommonService();
            AccauntInfo ainfo = AccauntInfo.Test;
            Ast.WebServiceResponse auth_responce = service.Authorization(ainfo.partner_key, ainfo.login, ainfo.password);
            auth_responce.Response = FixVolatileXmlField(auth_responce.Response, "UniqueToken");
            Trace(auth_responce);
        }

        static void TestAuthorizePublic()
        {
            Console.OutputEncoding = Encoding.GetEncoding("Windows-1251");
            Ast.CommonService service = new Ast.CommonService();
            AccauntInfo ainfo = AccauntInfo.Test;
            Ast.WebServiceResponse auth_responce = service.AuthorizationPublic(ainfo.partner_key);
            auth_responce.Response = FixVolatileXmlField(auth_responce.Response, "UniqueToken");
            Trace(auth_responce);
        }

        static void TestGetActions()
        {
            Console.OutputEncoding = Encoding.GetEncoding("Windows-1251");
            Ast.CommonService service = new Ast.CommonService();
            AccauntInfo ainfo = AccauntInfo.Test;
            Ast.WebServiceResponse auth_responce = service.Authorization(ainfo.partner_key, ainfo.login, ainfo.password);
            Authorization auth = Authorization.ReadFromXmlString(auth_responce.Response);

            Ast.WebServiceResponse get_actions_responce = service.GetList(
                uniqueToken: auth.UniqueToken
                , TSCode: TSCode.Bankruptcy
                , actionCode: actionCode.ActionPagedListGetMy
                , query: null
                , pageNumber: 0
                , pageSize: 200
                , id: 0
            );

            get_actions_responce.Response = get_actions_responce.Response.Replace("<row>", "\r\n  <row>").Replace("</data>", "\r\n</data>");

            Trace(get_actions_responce);
        }

        static void TestGetXSD(string fnameout, string actionCode)
        {
            Console.OutputEncoding = Encoding.GetEncoding("Windows-1251");
            Ast.CommonService service = new Ast.CommonService();
            AccauntInfo ainfo = AccauntInfo.Test;
            Ast.WebServiceResponse auth_responce = service.Authorization(ainfo.partner_key, ainfo.login, ainfo.password);
            Authorization auth = Authorization.ReadFromXmlString(auth_responce.Response);

            Ast.WebServiceResponse get_xsd_responce = service.GetXSD(
                uniqueToken: auth.UniqueToken
                , TSCode: TSCode.Bankruptcy
                , actionCode: actionCode
                , purchaseTypeId: purchaseTypeId.Открытый_аукцион_с_открытыми_ценами
                , buFace: buFace.Тип_организации_текущего_пользователя
            );

            File.WriteAllText(fnameout,get_xsd_responce.Response);

            get_xsd_responce.Response = null;

            Trace(get_xsd_responce);
        }

        static void TestRegisterDocument(string fnamedoc)
        {
            Console.OutputEncoding = Encoding.GetEncoding("Windows-1251");
            Ast.CommonService service = new Ast.CommonService();
            AccauntInfo ainfo = AccauntInfo.Test;
            Ast.WebServiceResponse auth_responce = service.Authorization(ainfo.partner_key, ainfo.login, ainfo.password);
            Authorization auth = Authorization.ReadFromXmlString(auth_responce.Response);

            Ast.WebServiceResponse register_doc_responce = service.RegisterDocument(
                uniqueToken: auth.UniqueToken
                , TSCode: TSCode.Bankruptcy
                , actionCode: actionCode.PurchaseCreate
                , purchaseTypeId: purchaseTypeId.Открытый_аукцион_с_открытыми_ценами
                , buFace: buFace.Тип_организации_текущего_пользователя
                , docId: 0
                , xmlData: File.ReadAllText(fnamedoc)
                , sign:null
                , isSignedDoc:false
                , ignoreWarning:false
                , serviceSetPriceId:0
            );

            /*
             <Messages><Message><Result>0</Result><ResultMessage>Черновик успешно сохранен</ResultMessage><DocID>10657550</DocID></Message></Messages>
             */

            register_doc_responce.Response = FixVolatileXmlField(register_doc_responce.Response, "DocID");

            Trace(register_doc_responce);
        }

        static void TestCancelDocument(string DocID)
        {
            Console.OutputEncoding = Encoding.GetEncoding("Windows-1251");
            Ast.CommonService service = new Ast.CommonService();
            AccauntInfo ainfo = AccauntInfo.Test;
            Ast.WebServiceResponse auth_responce = service.Authorization(ainfo.partner_key, ainfo.login, ainfo.password);
            Authorization auth = Authorization.ReadFromXmlString(auth_responce.Response);

            Ast.WebServiceResponse cancel_doc_responce = service.RegisterDocument(
                uniqueToken: auth.UniqueToken
                , TSCode: TSCode.Bankruptcy
                , actionCode: actionCode.PurchaseCancel
                , purchaseTypeId: purchaseTypeId.Открытый_аукцион_с_открытыми_ценами
                , buFace: buFace.Тип_организации_текущего_пользователя
                , docId: long.Parse(DocID)
                , xmlData: "<?xml version=\"1.0\" encoding=\"utf-8\"?><purchasecancel xmlns=\"http://www.norbit.ru/XMLSchema\"></purchasecancel>"
                , sign: null
                , isSignedDoc: false
                , ignoreWarning: false
                , serviceSetPriceId: 0
            );
            Trace(cancel_doc_responce);
        }
    }
}
