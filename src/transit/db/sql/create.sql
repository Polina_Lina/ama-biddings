drop table if exists tbl_migration;
create table tbl_migration
(
  version varchar(250),
  apply_time int,

  primary key(version)
);

drop table if exists ETP;
create table ETP
(
  id_ETP int NOT NULL AUTO_INCREMENT,

  Name varchar(250),
  URL varchar(250),
  LegalName varchar(250),

  primary key(id_ETP)
);

drop table if exists access_log;
drop table if exists TransitBidding;
create table TransitBidding
(
  id_TransitBidding int NOT NULL AUTO_INCREMENT,

  token_bidding varchar(250),
  body text,
  URL varchar(255),
  IP varchar(30),
  date_access Datetime,  
  etp varchar(50),

  primary key(id_TransitBidding)
);

drop table if exists Bidding;
drop table if exists User;

create table User
(
  id_User int NOT NULL AUTO_INCREMENT,

  Name varchar(50),

  primary key(id_User)
);

create table Bidding
(
  id_Bidding int NOT NULL AUTO_INCREMENT,

  transit_token varchar(100),
  body      text,

  tmCreated    DateTime NOT NULL,
  tmUrlSet     DateTime,
  tmDownloaded DateTime,
  tmPublished  DateTime,

  id_User INT NOT NULL,

  primary key(id_Bidding)
);

CREATE INDEX BiddingByUser ON Bidding(id_User);
ALTER TABLE Bidding ADD CONSTRAINT RefBiddingUser 
    FOREIGN KEY (id_User)
    REFERENCES User(id_User)
;

-- CREATE table access_log
-- (
--	id_Access_log int NOT NULL AUTO_INCREMENT,
--	IP varchar(30), 
--	TransitBidding_id int NOT NULL,
--	date_access Datetime NOT NULL,
	
--	PRIMARY KEY(id_Access_log),
--	FOREIGN KEY (TransitBidding_id) REFERENCES TransitBidding(id_TransitBidding)
-- );
