LOAD DATA LOCAL INFILE '../../../forms/forms/ama/biddings/base/etp.csv'
INTO TABLE ETP
character set cp1251
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\r\n'
(
  Name,
  URL,
  LegalName
);