<? 

mb_internal_encoding("utf-8");

require_once('../assets/helpers/codec.xml.php');

function TestDecode($codec_file_path,$codec_class_name,$file_path_in,$file_path_out)
{
	require_once($codec_file_path);

	$codec= new $codec_class_name();

	$str = file_get_contents($file_path_in);

	$decoded_data= $codec->Decode($str);
/*
	try
	{
		
	}
	catch (XmlErrorException $exception)
	{
		echo str_replace("\n","\r\n",$exception->getMessage());
		foreach ($exception->errors as $error)
		{
			echo "\r\n";
			echo str_replace("\n","\r\n",print_r($error,true));
		}
		return;
	}
*/
	if (null==$file_path_out)
	{
		echo json_encode($decoded_data);
	}
	else
	{
		//file_put_contents($file_path_out,json_encode($decoded_data));
		file_put_contents($file_path_out,str_replace("\n","\r\n",print_r($decoded_data,true)));
	}
}

function TestEncode($codec_file_path,$codec_class_name,$file_path_in,$file_path_out)
{
	require_once($codec_file_path);

	$codec= new $codec_class_name();

	$str = file_get_contents($file_path_in);
	$data= json_decode($str);

	try
	{
		$encoded_data= $codec->Encode($data);
	}
	catch (XmlErrorException $exception)
	{
		echo str_replace("\n","\r\n",$exception->getMessage());
		foreach ($exception->errors as $error)
		{
			echo "\r\n";
			echo str_replace("\n","\r\n",print_r($error,true));
		}
		return;
	}

	if (null==$file_path_out)
	{
		echo $encoded_data;
	}
	else
	{
		file_put_contents($file_path_out,$encoded_data);
	}
}

if (count($argv) > 1)
{
	$cmd= $argv[1];
	switch ($cmd)
	{
		case 'encode': 
			$codec_file_path=  $argv[2];
			$codec_class_name= $argv[3];
			$file_path_in=     $argv[4];
			$file_path_out=    $argv[5];
			TestEncode($codec_file_path,$codec_class_name,$file_path_in,$file_path_out);
			break;
		case 'decode': 
			$codec_file_path=  $argv[2];
			$codec_class_name= $argv[3];
			$file_path_in=     $argv[4];
			$file_path_out=    $argv[5];
			TestDecode($codec_file_path,$codec_class_name,$file_path_in,$file_path_out);
			break;
	}
}