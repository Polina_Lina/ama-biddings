insert into TransitBidding
set 
  token_bidding= "595b7110deff96.27794286"
  ,body= ""
  ,URL= "http://utptest.sberbank-ast.ru/Bankruptcy/NBT/PurchaseCreate/11/3/10669096/"
  ,IP= "192.234.32.45"
  ,date_access= "2017-01-01"
  ,etp="Сбербанк"
;

insert into TransitBidding
set 
  token_bidding= "595b7110deff96.27798286"
  ,body= ""
  ,URL= "http://utptest.sberbank-ast.ru/Bankruptcy/NBT/PurchaseCreate/11/3/10669096/"
  ,IP= "192.234.32.45"
  ,date_access= "2017-03-03"
  ,etp="Сбербанк"
;

insert into TransitBidding
set 
  token_bidding= "59557110deff96.27794286"
  ,body= ""
  ,URL= "http://yandex.ru"
  ,IP= "193.234.32.45"
  ,date_access= "2017-02-02"
  ,etp="Тестовая ЭТП"
;

insert into TransitBidding
set 
  token_bidding= "59557140deff96.27794286"
  ,body= ""
  ,URL= "http://yandex.ru"
  ,IP= "193.234.32.45"
  ,date_access= "2017-04-04"
  ,etp="Тестовая ЭТП"
;

insert into TransitBidding
set 
  token_bidding= "595b71106eff96.27794286"
  ,body= "<?xml version=\"1.0\" encoding=\"utf-8\"?>
<purchase xmlns=\"http://www.norbit.ru/XMLSchema\">
	<purchaseinfo>
		<idefrsb>0</idefrsb>
		<purchasename>Торги 0</purchasename>
		<purchasetypeinfo>
			<purchasetypename>Аукцион в открытой форме</purchasetypename>
		</purchasetypeinfo>
		<sitepublicdate>04.04.2004</sitepublicdate>
		<paperpublicdate>05.05.2005</paperpublicdate>
		<efrpublicdate>10.10.2017</efrpublicdate>
	</purchaseinfo>
	<debtorinfo>
		<personphis>No</personphis>
		<debtorname>ООО Роги и ноги</debtorname>
		<debtorinn>6449013711</debtorinn>
		<debtorogrn>1026402000657</debtorogrn>
	</debtorinfo>
	<bids>
		<bid>
			<bidpanel>
				<bidinfo>
					<bidno>0</bidno>
					<bidname>Здание торгового комплекса</bidname>
				</bidinfo>
				<biddebtorinfo>
					<debtorbidname>Здание торговое,
10000 кв.м, 3 этажа, подземный паркинг
код 123:3456456:2344523:4539
Точный адрес: Удмуртская Республика, г. Ижевск, Песочная 13</debtorbidname>
					<bidregion>Удмуртия</bidregion>
					<bidcategory>01</bidcategory>
					<bidinventoryresearchtype>приходите, знакомиться в пятницу 13 октября с 5:00 до полуночи</bidinventoryresearchtype>
				</biddebtorinfo>
				<bidtenderinfo>
					<bidprice>10000000009</bidprice>
					<bidauctionsteppercent>5</bidauctionsteppercent>
				</bidtenderinfo>
				<biddepositinfo>
					<isdepositinpercent>No</isdepositinpercent>
					<biddeposit>100000009</biddeposit>
				</biddepositinfo>
			</bidpanel>
		</bid>
		<bid>
			<bidpanel>
				<bidinfo>
					<bidno>1</bidno>
					<bidname>Велосипед</bidname>
				</bidinfo>
				<biddebtorinfo>
					<debtorbidname>Велосипед горный
Точный адрес: Республика Башкирия, г. Уфа, Пушкина 10</debtorbidname>
					<bidregion>Удмуртия</bidregion>
					<bidcategory>01</bidcategory>
					<bidinventoryresearchtype>12 сентября 2017 года, в 11:00</bidinventoryresearchtype>
				</biddebtorinfo>
				<bidtenderinfo>
					<bidprice>150000</bidprice>
					<bidauctionsteppercent>6</bidauctionsteppercent>
				</bidtenderinfo>
				<biddepositinfo>
					<isdepositinpercent>No</isdepositinpercent>
					<biddeposit>10000</biddeposit>
				</biddepositinfo>
			</bidpanel>
		</bid>
		<bid>
			<bidpanel>
				<bidinfo>
					<bidno>2</bidno>
					<bidname>Дебиторская задолженность</bidname>
				</bidinfo>
				<biddebtorinfo>
					<debtorbidname>Дебиторская задолженность от 20 контрагентов
Точный адрес: Московская область, г. Химки, Петрова 6</debtorbidname>
					<bidregion>Удмуртия</bidregion>
					<bidcategory>01</bidcategory>
					<bidinventoryresearchtype>Обычный порядок</bidinventoryresearchtype>
				</biddebtorinfo>
				<bidtenderinfo>
					<bidprice>238476</bidprice>
					<bidauctionsteppercent>7</bidauctionsteppercent>
				</bidtenderinfo>
				<biddepositinfo>
					<isdepositinpercent>Yes</isdepositinpercent>
					<biddeposit>19</biddeposit>
				</biddepositinfo>
			</bidpanel>
		</bid>
	</bids>
	<crisicmanagerinfo>
		<crisicmanagerfullname>Парамонов Парамон Парамонович</crisicmanagerfullname>
		<crisismanagerinn>5460000016</crisismanagerinn>
		<arbitrageorganizationpanel>
			<arbitrageorganization>НП СРО АУ Левое</arbitrageorganization>
		</arbitrageorganizationpanel>
	</crisicmanagerinfo>
	<businesinfo>
		<businessname>Левый арбитражный суд Тофаларской автономной области</businessname>
		<businessno>234534345</businessno>
		<businessreason> </businessreason>
	</businesinfo>
	<requestinfo>
		<requeststartdate>01.12.2000 10:00</requeststartdate>
		<requeststopdate>01.01.2001 10:01</requeststopdate>
		<requestplace> </requestplace>
		<requestorder> </requestorder>
		<registrationorder> </registrationorder>
		<registrationdocuments> </registrationdocuments>
	</requestinfo>
	<terms>
		<purchaseauctionstartdate>03.03.2003 10:03</purchaseauctionstartdate>
	</terms>
	<resultinfo>
		<resultcriteria> </resultcriteria>
		<auctionresultdate>30.06.2017 15:00</auctionresultdate>
		<resultplace> </resultplace>
	</resultinfo>
	<contractinfo>
		<contractorder> </contractorder>
		<contractperiod> </contractperiod>
		<contractaccountinfo>
			<contractaccount>12345678912345678912</contractaccount>
			<contractfaceaccount>12345678912</contractfaceaccount>
			<contractcorraccount>12345678912345678912</contractcorraccount>
			<contractbic>123456789</contractbic>
			<contractbankname> </contractbankname>
			<contractbankaddress> </contractbankaddress>
			<contractinn>1234567891</contractinn>
			<recipientname> </recipientname>
		</contractaccountinfo>
		<contractdoc>
			<file>
				<fileid> </fileid>
				<filename> </filename>
				<signinfo> </signinfo>
				<hash> </hash>
				<sign> </sign>
			</file>
		</contractdoc>
	</contractinfo>
</purchase>
"
  ,URL= "http://utptest.sberbank-ast.ru/Bankruptcy/NBT/PurchaseCreate/11/3/10669096/"
  ,IP= "192.234.32.45"
  ,date_access= "2017-05-05"
  ,etp="Сбербанк"
;

insert into TransitBidding
set 
  token_bidding= "59557110detf96.27794286"
  ,body= "<?xml version=\"1.0\" encoding=\"utf-8\"?>
<Объявление_о_торгах xmlns=\"http://probili.ru/bidding/transit\">
	<Наименование>Торги 0</Наименование>
	<URL>a.o.ru</URL>
    <Начало_приёма_заявок>
        <дата>01.12.2001</дата>
        <время>10:00</время>
    </Начало_приёма_заявок>
	<Окончание_приёма_заявок>
		<дата>01.01.2001</дата>
		<время>10:01</время>
	</Окончание_приёма_заявок>
	<Начало_торгов>
		<дата>03.03.2003</дата>
		<время>10:03</время>
	</Начало_торгов>
	<Объявление>
		<ЕФРСБ>
			<Номер>0</Номер>
		</ЕФРСБ>
		<ФедеральноеСМИ>
			<дата>04.04.2004</дата>
			<номер_тиража>4</номер_тиража>
		</ФедеральноеСМИ>
		<МестноеСМИ>
			<Наименование>Удмуртская правда</Наименование>
			<дата>05.05.2005</дата>
			<номер_тиража>5</номер_тиража>
		</МестноеСМИ>
	</Объявление>
	<Форма_торгов>Аукцион в открытой форме</Форма_торгов>
	<ЭТП>
		<text>Межотраслевая торговая система \"Фабрикант\"</text>
		<url>www.fabrikant.ru</url>
		<operator>ООО \"Фабрикант.ру\"</operator>
	</ЭТП>
	<Лоты>
		<Лот>
			<Наименование>Здавние торгового комплекса</Наименование>
			<Начальная_цена>10000000009</Начальная_цена>
			<Размер_задатка>100000009</Размер_задатка>
            <Размер_задатка_ед>руб.</Размер_задатка_ед>
			<Шаг_аукциона>100000009</Шаг_аукциона>
            <Шаг_аукциона_ед>руб.</Шаг_аукциона_ед>
			<ОКДП>5</ОКДП>
			<Краткое_описание>Здание торговое,
10000 кв.м, 3 этажа, подземный паркинг
код 123:3456456:2344523:4539</Краткое_описание>
			<Точный_адрес>Удмуртская Республика, г. Ижевск, Песочная 13</Точный_адрес>
			<Порядок_ознакомления>приходите, знакомиться в пятницу 13 октября с 5:00 до полуночи</Порядок_ознакомления>
		</Лот>
		<Лот>
			<Наименование>Велосипед</Наименование>
			<Начальная_цена>150000</Начальная_цена>
			<Размер_задатка>10000</Размер_задатка>
            <Размер_задатка_ед>руб.</Размер_задатка_ед>
			<Шаг_аукциона>10000</Шаг_аукциона>
            <Шаг_аукциона_ед>руб.</Шаг_аукциона_ед>
			<ОКДП>7</ОКДП>
			<Краткое_описание>Велосипед горный</Краткое_описание>
			<Точный_адрес>Республика Башкирия, г. Уфа, Пушкина 10</Точный_адрес>
			<Порядок_ознакомления>12 сентября 2017 года, в 11:00</Порядок_ознакомления>
		</Лот>
		<Лот>
			<Наименование>Дебиторская задолженность</Наименование>
			<Начальная_цена>238476</Начальная_цена>
			<Размер_задатка>42378</Размер_задатка>
            <Размер_задатка_ед>руб.</Размер_задатка_ед>
			<Шаг_аукциона>100</Шаг_аукциона>
            <Шаг_аукциона_ед>руб.</Шаг_аукциона_ед>
			<ОКДП>123</ОКДП>
			<Краткое_описание>Дебиторская задолженность от 20 контрагентов</Краткое_описание>
			<Точный_адрес>Московская область, г. Химки, Петрова 6</Точный_адрес>
			<Порядок_ознакомления></Порядок_ознакомления>
		</Лот>
	</Лоты>
	<Процедура>
		<Должник>
			<Наименование>ООО Роги и ноги</Наименование>
			<ИНН>6449013711</ИНН>
			<ОГРН>1026402000657</ОГРН>
		</Должник>
		<Арбитражный_управляющий>
			<Фамилия>Парамонов</Фамилия>
			<Имя>Парамон</Имя>
			<Отчество>Парамонович</Отчество>
			<ИНН>5460000016</ИНН>
		</Арбитражный_управляющий>
		<СРО>НП СРО АУ Левое</СРО>
		<АС>Левый арбитражный суд Тофаларской автономной области</АС>
		<Номер_дела_о_банкротстве>234534345</Номер_дела_о_банкротстве>
	</Процедура>
</Объявление_о_торгах>
"
  ,URL= "http://yandex.ru"
  ,IP= "193.234.32.45"
  ,date_access= "2017-06-06"
  ,etp="Тестовая ЭТП"
;