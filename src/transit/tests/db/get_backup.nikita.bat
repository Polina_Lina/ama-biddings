set MYSQLDUMP="C:\Program Files\MySQL\MySQL Server 5.7\bin\mysqldump.exe"
set MYSQL="C:\Program Files\MySQL\MySQL Server 5.7\bin\mysql.exe"
set MYSQLUser="root"
set MYSQLPassword="root"
set MYSQLHOST="localhost"
set DBName="biddingdevel"

call %MYSQLDUMP% --host=%MYSQLHOST% --default-character-set=utf8 --user=%MYSQLUser% --password=%MYSQLPassword% --set-charset --routines --no-autocommit --extended-insert --result-file=backup.%date%.%random%.sql %DBName%