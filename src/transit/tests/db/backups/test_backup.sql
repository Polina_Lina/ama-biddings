-- MySQL dump 10.13  Distrib 5.5.23, for Win64 (x86)
--
-- Host: 192.168.0.193    Database: biddingdevel
-- ------------------------------------------------------
-- Server version	5.1.39-community

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bidding`
--

DROP TABLE IF EXISTS `bidding`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bidding` (
  `id_Bidding` int(11) NOT NULL AUTO_INCREMENT,
  `transit_token` varchar(100) DEFAULT NULL,
  `body` text,
  `tmCreated` datetime NOT NULL,
  `tmUrlSet` datetime DEFAULT NULL,
  `tmDownloaded` datetime DEFAULT NULL,
  `tmPublished` datetime DEFAULT NULL,
  `id_User` int(11) NOT NULL,
  PRIMARY KEY (`id_Bidding`),
  KEY `BiddingByUser` (`id_User`),
  CONSTRAINT `RefBiddingUser` FOREIGN KEY (`id_User`) REFERENCES `user` (`id_User`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bidding`
--

LOCK TABLES `bidding` WRITE;
/*!40000 ALTER TABLE `bidding` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `bidding` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `etp`
--

DROP TABLE IF EXISTS `etp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `etp` (
  `id_ETP` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(250) DEFAULT NULL,
  `URL` varchar(250) DEFAULT NULL,
  `LegalName` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id_ETP`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `etp`
--

LOCK TABLES `etp` WRITE;
/*!40000 ALTER TABLE `etp` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `etp` VALUES (1,'Ru-Trade24','www.ru-trade24.ru','ООО \"Ру-Трейд\"'),(2,'АИСТ','www.aistorg.ru','ООО \"Автоматизированная информационная система торгов\"'),(3,'АЙПИЭС ЭТП','www.ipsetp.ru','ООО \"АйПиЭс ЭТП\"'),(4,'Арбитат','www.arbitat.ru','ООО \"АРБИТАТ\"'),(5,'АрбиТрейд','arbitrade.ru','ООО \"Центр электронной торговли \"АрбиТрейд\"'),(6,'Аукционы Сибири','www.ausib.ru','ООО \"Аукционы Сибири\"'),(7,'Банкротство РТ','www.etp-bankrotstvo.ru','ООО \"Электронная торговая площадка\"'),(8,'Всероссийская Электронная Торговая Площадка','торговая-площадка-вэтп.рф','ООО \"ВЭТП\"'),(9,'Межотраслевая торговая система \"Фабрикант\"','www.fabrikant.ru','ООО \"Фабрикант.ру\"'),(10,'МФБ','www.etp.mse.ru','ОТКРЫТОЕ АКЦИОНЕРНОЕ ОБЩЕСТВО \"КЛИРИНГОВЫЙ ЦЕНТР МФБ\"'),(11,'Открытая торговая площадка','www.opentp.ru','ООО \"Открытая торговая площадка\"'),(12,'Сибирская электронная площадка','www.sibtorgi.ru','ООО \"СибЭПл\"'),(13,'ЭТП \"Агенда\"','www.etp-agenda.ru','ООО \"Агенда\"'),(14,'ЭТС24','www.ets24.ru','ФГУП \"Электронные торги и безопасность\"'),(15,'«Property Trade»','www.propertytrade.ru','ООО «ОТС»'),(16,'«RUSSIA OnLine»','www.rus-on.ru','ООО «РУССИА ОнЛайн»'),(17,'«Е-Тендер»','www.e-tender.su','ООО «Электронная площадка «Санкт-Петербург»'),(18,'«Новые информационные сервисы»','nistp.ru','ЗАО «Новые информационные сервисы»'),(19,'«Региональная Торговая площадка»','www.regtorg.com','ООО «Сирин»'),(20,'«Системы ЭЛектронных Торгов»','www.selt-online.ru','ООО «СЭлТ»'),(21,'«ТЕНДЕР ГАРАНТ»','www.tendergarant.com','ООО «ТЕНДЕР ГАРАНТ»'),(22,'«Электронная площадка «Вердиктъ»','www.vertrades.ru','ООО «Электронная площадка «Вердиктъ»'),(23,'«Электронная торговая площадка ELECTRO-TORGI.RU»','www.bankrupt.electro-torgi.ru','ЗАО «Вэллстон»'),(24,'B2B-Center','www.b2b-center.ru','ОАО «Центр развития экономики»'),(25,'KARTOTEKA.RU','etp.kartoteka.ru','ООО \"Коммерсантъ КАРТОТЕКА\"'),(26,'UralBidIn','www.uralbidin.ru','ООО \"УралБидИн\"'),(27,'uTender ','utender.ru/','ООО \"ЮТендер\"'),(28,'АКОСТА info','www.akosta.info','ООО «А-Коста»'),(29,'Альфалот','www.alfalot.ru','ООО \"Аукционы Федерации\"'),(30,'Аукцион-центр','www.aukcioncenter.ru','Общество с ограниченной ответственностью «ИстКонсалтингГрупп»'),(31,'Аукционы Дальнего Востока','www.torgidv.ru','ООО \"Аукционы Дальнего Востока\"'),(32,'Балтийская электронная площадка','www.bepspb.ru/','ООО \"Балтийская электронная площадка\"'),(33,'ЗАО «Сбербанк-АСТ»','utp.sberbank-ast.ru/Bankruptcy/','ЗАО «Сбербанк-АСТ»'),(34,'Межрегиональная Электронная Торговая Система','www.m-ets.ru','ООО «МЭТС»'),(35,'МЕТА-ИНВЕСТ','www.meta-invest.ru','ОАО \"ИК\"МЕТА\"'),(36,'Объединенная Торговая Площадка','utpl.ru','ЗАО \"Объединенная Торговая Площадка\"'),(37,'ООО «Специализированная организация по проведению торгов – Южная Электронная Торговая Площадка»','www.TORGIBANKROT.RU','ООО «Специализированная организация по проведению торгов – Южная Электронная Торговая Площадка»'),(38,'Российский аукционный дом','www.lot-online.ru','ОАО \"Российский аукционный дом\"'),(39,'Сибирская торговая площадка','www.sibtoptrade.ru/','Общество с ограниченной ответственностью «Сибирская торговая площадка»'),(40,'Система электронных торгов и муниципальных аукционов \"ВТБ-Центр\"','vtb-center.ru','ООО \"Модный дом\"'),(41,'ТендерСтандарт','www.tenderstandart.ru','ООО \"ТендерСтандарт\"'),(42,'Уральская электронная торговая площадка','etpu.ru/','ЗАО \"Уральская электронная торговая площадка\"'),(43,'Центр дистанционных торгов','www.cdtrf.ru','ЗАО «Центр Дистанционных Торгов»'),(44,'Электронная площадка \"Аукционный тендерный центр\"','www.atctrade.ru','Общество с ограниченной ответственностью \"Аукционный тендерный центр\"'),(45,'Электронная площадка \"Система Электронных Торгов Имуществом\" (СЭЛТИМ)','www.seltim.ru','ООО \"Сатурн\"'),(46,'Электронная площадка №1','www.etp1.ru','ООО ”Электронная площадка №1”'),(47,'Электронная площадка Группы компаний ВИТ','etp.vitnw.ru','ООО \"ВИТ\"'),(48,'Электронная площадка Центра реализации','www.centerr.ru','ООО \"Центр реализации\"'),(49,'Электронная площадка ЭСП','el-torg.com ','ООО \" Электронные системы Поволжья\"'),(50,'Электронная торговая площадка \"Евразийская торговая площадка\"','eurtp.ru/','ООО \"Евразийская торговая площадка\"'),(51,'Электронная Торговая Площадка \"ПОВОЛЖСКИЙ АУКЦИОННЫЙ ДОМ\"','www.auction63.ru','ООО \"ПОВОЛЖСКИЙ АУКЦИОННЫЙ ДОМ\"'),(52,'Электронная торговая площадка \"Профит\"','www.etp-profit.ru','Общество с ограниченной ответственностью \"Перспектива\"'),(53,'Электронная торговая площадка \"Регион\"','gloriaservice.ru','ООО\"Глория Сервис\"'),(54,'Электронный капитал','www.eksystems.ru','ЗАО «Электронный капитал»'),(55,'ЭТП \"Пром-Консалтинг\"','www.promkonsalt.ru','ООО \"ПРОМ-Консалтинг\"'),(56,'ЭТП \"Тест\"','trust.dev','ООО \"Тест\"');
/*!40000 ALTER TABLE `etp` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `tbl_migration`
--

DROP TABLE IF EXISTS `tbl_migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_migration` (
  `version` varchar(250) NOT NULL DEFAULT '',
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_migration`
--

LOCK TABLES `tbl_migration` WRITE;
/*!40000 ALTER TABLE `tbl_migration` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `tbl_migration` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `transitbidding`
--

DROP TABLE IF EXISTS `transitbidding`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transitbidding` (
  `id_TransitBidding` int(11) NOT NULL AUTO_INCREMENT,
  `token_bidding` varchar(250) DEFAULT NULL,
  `body` text,
  `URL` varchar(255) DEFAULT NULL,
  `IP` varchar(30) DEFAULT NULL,
  `date_access` datetime NOT NULL,
  `etp` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_TransitBidding`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transitbidding`
--

LOCK TABLES `transitbidding` WRITE;
/*!40000 ALTER TABLE `transitbidding` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `transitbidding` VALUES (1,'595b7110deff96.27794286','','http://utptest.sberbank-ast.ru/Bankruptcy/NBT/PurchaseCreate/11/3/10669096/','192.234.32.45','2017-01-01 00:00:00','Сбербанк'),(2,'595b7110deff96.27798286','','http://utptest.sberbank-ast.ru/Bankruptcy/NBT/PurchaseCreate/11/3/10669096/','192.234.32.45','2017-03-03 00:00:00','Сбербанк'),(3,'59557110deff96.27794286','','http://yandex.ru','193.234.32.45','2017-02-02 00:00:00','Тестовая ЭТП'),(4,'59557140deff96.27794286','','http://yandex.ru','193.234.32.45','2017-04-04 00:00:00','Тестовая ЭТП'),(5,'595b71106eff96.27794286','<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<purchase xmlns=\"http://www.norbit.ru/XMLSchema\">\n	<purchaseinfo>\n		<idefrsb>0</idefrsb>\n		<purchasename>Торги 0</purchasename>\n		<purchasetypeinfo>\n			<purchasetypename>Аукцион в открытой форме</purchasetypename>\n		</purchasetypeinfo>\n		<sitepublicdate>04.04.2004</sitepublicdate>\n		<paperpublicdate>05.05.2005</paperpublicdate>\n		<efrpublicdate>10.10.2017</efrpublicdate>\n	</purchaseinfo>\n	<debtorinfo>\n		<personphis>No</personphis>\n		<debtorname>ООО Роги и ноги</debtorname>\n		<debtorinn>6449013711</debtorinn>\n		<debtorogrn>1026402000657</debtorogrn>\n	</debtorinfo>\n	<bids>\n		<bid>\n			<bidpanel>\n				<bidinfo>\n					<bidno>0</bidno>\n					<bidname>Здание торгового комплекса</bidname>\n				</bidinfo>\n				<biddebtorinfo>\n					<debtorbidname>Здание торговое,\n10000 кв.м, 3 этажа, подземный паркинг\nкод 123:3456456:2344523:4539\nТочный адрес: Удмуртская Республика, г. Ижевск, Песочная 13</debtorbidname>\n					<bidregion>Удмуртия</bidregion>\n					<bidcategory>01</bidcategory>\n					<bidinventoryresearchtype>приходите, знакомиться в пятницу 13 октября с 5:00 до полуночи</bidinventoryresearchtype>\n				</biddebtorinfo>\n				<bidtenderinfo>\n					<bidprice>10000000009</bidprice>\n					<bidauctionsteppercent>5</bidauctionsteppercent>\n				</bidtenderinfo>\n				<biddepositinfo>\n					<isdepositinpercent>No</isdepositinpercent>\n					<biddeposit>100000009</biddeposit>\n				</biddepositinfo>\n			</bidpanel>\n		</bid>\n		<bid>\n			<bidpanel>\n				<bidinfo>\n					<bidno>1</bidno>\n					<bidname>Велосипед</bidname>\n				</bidinfo>\n				<biddebtorinfo>\n					<debtorbidname>Велосипед горный\nТочный адрес: Республика Башкирия, г. Уфа, Пушкина 10</debtorbidname>\n					<bidregion>Удмуртия</bidregion>\n					<bidcategory>01</bidcategory>\n					<bidinventoryresearchtype>12 сентября 2017 года, в 11:00</bidinventoryresearchtype>\n				</biddebtorinfo>\n				<bidtenderinfo>\n					<bidprice>150000</bidprice>\n					<bidauctionsteppercent>6</bidauctionsteppercent>\n				</bidtenderinfo>\n				<biddepositinfo>\n					<isdepositinpercent>No</isdepositinpercent>\n					<biddeposit>10000</biddeposit>\n				</biddepositinfo>\n			</bidpanel>\n		</bid>\n		<bid>\n			<bidpanel>\n				<bidinfo>\n					<bidno>2</bidno>\n					<bidname>Дебиторская задолженность</bidname>\n				</bidinfo>\n				<biddebtorinfo>\n					<debtorbidname>Дебиторская задолженность от 20 контрагентов\nТочный адрес: Московская область, г. Химки, Петрова 6</debtorbidname>\n					<bidregion>Удмуртия</bidregion>\n					<bidcategory>01</bidcategory>\n					<bidinventoryresearchtype>Обычный порядок</bidinventoryresearchtype>\n				</biddebtorinfo>\n				<bidtenderinfo>\n					<bidprice>238476</bidprice>\n					<bidauctionsteppercent>7</bidauctionsteppercent>\n				</bidtenderinfo>\n				<biddepositinfo>\n					<isdepositinpercent>Yes</isdepositinpercent>\n					<biddeposit>19</biddeposit>\n				</biddepositinfo>\n			</bidpanel>\n		</bid>\n	</bids>\n	<crisicmanagerinfo>\n		<crisicmanagerfullname>Парамонов Парамон Парамонович</crisicmanagerfullname>\n		<crisismanagerinn>5460000016</crisismanagerinn>\n		<arbitrageorganizationpanel>\n			<arbitrageorganization>НП СРО АУ Левое</arbitrageorganization>\n		</arbitrageorganizationpanel>\n	</crisicmanagerinfo>\n	<businesinfo>\n		<businessname>Левый арбитражный суд Тофаларской автономной области</businessname>\n		<businessno>234534345</businessno>\n		<businessreason> </businessreason>\n	</businesinfo>\n	<requestinfo>\n		<requeststartdate>01.12.2000 10:00</requeststartdate>\n		<requeststopdate>01.01.2001 10:01</requeststopdate>\n		<requestplace> </requestplace>\n		<requestorder> </requestorder>\n		<registrationorder> </registrationorder>\n		<registrationdocuments> </registrationdocuments>\n	</requestinfo>\n	<terms>\n		<purchaseauctionstartdate>03.03.2003 10:03</purchaseauctionstartdate>\n	</terms>\n	<resultinfo>\n		<resultcriteria> </resultcriteria>\n		<auctionresultdate>30.06.2017 15:00</auctionresultdate>\n		<resultplace> </resultplace>\n	</resultinfo>\n	<contractinfo>\n		<contractorder> </contractorder>\n		<contractperiod> </contractperiod>\n		<contractaccountinfo>\n			<contractaccount>12345678912345678912</contractaccount>\n			<contractfaceaccount>12345678912</contractfaceaccount>\n			<contractcorraccount>12345678912345678912</contractcorraccount>\n			<contractbic>123456789</contractbic>\n			<contractbankname> </contractbankname>\n			<contractbankaddress> </contractbankaddress>\n			<contractinn>1234567891</contractinn>\n			<recipientname> </recipientname>\n		</contractaccountinfo>\n		<contractdoc>\n			<file>\n				<fileid> </fileid>\n				<filename> </filename>\n				<signinfo> </signinfo>\n				<hash> </hash>\n				<sign> </sign>\n			</file>\n		</contractdoc>\n	</contractinfo>\n</purchase>\n','http://utptest.sberbank-ast.ru/Bankruptcy/NBT/PurchaseCreate/11/3/10669096/','192.234.32.45','2017-05-05 00:00:00','Сбербанк'),(6,'59557110detf96.27794286','<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<Объявление_о_торгах xmlns=\"http://probili.ru/bidding/transit\">\n	<Наименование>Торги 0</Наименование>\n	<URL>a.o.ru</URL>\n    <Начало_приёма_заявок>\n        <дата>01.12.2001</дата>\n        <время>10:00</время>\n    </Начало_приёма_заявок>\n	<Окончание_приёма_заявок>\n		<дата>01.01.2001</дата>\n		<время>10:01</время>\n	</Окончание_приёма_заявок>\n	<Начало_торгов>\n		<дата>03.03.2003</дата>\n		<время>10:03</время>\n	</Начало_торгов>\n	<Объявление>\n		<ЕФРСБ>\n			<Номер>0</Номер>\n		</ЕФРСБ>\n		<ФедеральноеСМИ>\n			<дата>04.04.2004</дата>\n			<номер_тиража>4</номер_тиража>\n		</ФедеральноеСМИ>\n		<МестноеСМИ>\n			<Наименование>Удмуртская правда</Наименование>\n			<дата>05.05.2005</дата>\n			<номер_тиража>5</номер_тиража>\n		</МестноеСМИ>\n	</Объявление>\n	<Форма_торгов>Аукцион в открытой форме</Форма_торгов>\n	<ЭТП>\n		<text>Межотраслевая торговая система \"Фабрикант\"</text>\n		<url>www.fabrikant.ru</url>\n		<operator>ООО \"Фабрикант.ру\"</operator>\n	</ЭТП>\n	<Лоты>\n		<Лот>\n			<Наименование>Здавние торгового комплекса</Наименование>\n			<Начальная_цена>10000000009</Начальная_цена>\n			<Размер_задатка>100000009</Размер_задатка>\n            <Размер_задатка_ед>руб.</Размер_задатка_ед>\n			<Шаг_аукциона>100000009</Шаг_аукциона>\n            <Шаг_аукциона_ед>руб.</Шаг_аукциона_ед>\n			<ОКДП>5</ОКДП>\n			<Краткое_описание>Здание торговое,\n10000 кв.м, 3 этажа, подземный паркинг\nкод 123:3456456:2344523:4539</Краткое_описание>\n			<Точный_адрес>Удмуртская Республика, г. Ижевск, Песочная 13</Точный_адрес>\n			<Порядок_ознакомления>приходите, знакомиться в пятницу 13 октября с 5:00 до полуночи</Порядок_ознакомления>\n		</Лот>\n		<Лот>\n			<Наименование>Велосипед</Наименование>\n			<Начальная_цена>150000</Начальная_цена>\n			<Размер_задатка>10000</Размер_задатка>\n            <Размер_задатка_ед>руб.</Размер_задатка_ед>\n			<Шаг_аукциона>10000</Шаг_аукциона>\n            <Шаг_аукциона_ед>руб.</Шаг_аукциона_ед>\n			<ОКДП>7</ОКДП>\n			<Краткое_описание>Велосипед горный</Краткое_описание>\n			<Точный_адрес>Республика Башкирия, г. Уфа, Пушкина 10</Точный_адрес>\n			<Порядок_ознакомления>12 сентября 2017 года, в 11:00</Порядок_ознакомления>\n		</Лот>\n		<Лот>\n			<Наименование>Дебиторская задолженность</Наименование>\n			<Начальная_цена>238476</Начальная_цена>\n			<Размер_задатка>42378</Размер_задатка>\n            <Размер_задатка_ед>руб.</Размер_задатка_ед>\n			<Шаг_аукциона>100</Шаг_аукциона>\n            <Шаг_аукциона_ед>руб.</Шаг_аукциона_ед>\n			<ОКДП>123</ОКДП>\n			<Краткое_описание>Дебиторская задолженность от 20 контрагентов</Краткое_описание>\n			<Точный_адрес>Московская область, г. Химки, Петрова 6</Точный_адрес>\n			<Порядок_ознакомления></Порядок_ознакомления>\n		</Лот>\n	</Лоты>\n	<Процедура>\n		<Должник>\n			<Наименование>ООО Роги и ноги</Наименование>\n			<ИНН>6449013711</ИНН>\n			<ОГРН>1026402000657</ОГРН>\n		</Должник>\n		<Арбитражный_управляющий>\n			<Фамилия>Парамонов</Фамилия>\n			<Имя>Парамон</Имя>\n			<Отчество>Парамонович</Отчество>\n			<ИНН>5460000016</ИНН>\n		</Арбитражный_управляющий>\n		<СРО>НП СРО АУ Левое</СРО>\n		<АС>Левый арбитражный суд Тофаларской автономной области</АС>\n		<Номер_дела_о_банкротстве>234534345</Номер_дела_о_банкротстве>\n	</Процедура>\n</Объявление_о_торгах>\n','http://yandex.ru','193.234.32.45','2017-06-06 00:00:00','Тестовая ЭТП');
/*!40000 ALTER TABLE `transitbidding` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id_User` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_User`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping routines for database 'biddingdevel'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-07-09 17:22:28
