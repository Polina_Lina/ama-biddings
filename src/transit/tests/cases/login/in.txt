wait_text "Параметр действия неопределён"
shot_check_png ..\..\shots\no-action.png

goto ?action=login
wait_text "Представьтесь пожалуйста"
shot_check_png ..\..\shots\login.png

goto ?action=admin
wait_text "Представьтесь пожалуйста"
shot_check_png ..\..\shots\login.png

goto ?action=biddings
wait_text "Представьтесь пожалуйста"
shot_check_png ..\..\shots\login.png

click_value "Представиться"
wait_text "Вы указали незнакомые нам данные для входа"
shot_check_png ..\..\shots\login-wrong.png

type_id id-login    admin
type_id id-password IdWShKH3n8
click_value "Представиться"
wait_text "2017-03-03"
wait_text "Просмотр 1 - 5 из 6"
shot_check_png ..\..\shots\transit-biddings.png

click_text "Выйти"
wait_text "Представьтесь пожалуйста"
shot_check_png ..\..\shots\login.png

goto ?action=biddings
wait_text "Представьтесь пожалуйста"
shot_check_png ..\..\shots\login.png

exit