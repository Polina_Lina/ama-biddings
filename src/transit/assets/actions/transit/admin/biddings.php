<?

// админка для просмотра передач на ЭТП

session_start();
if (!isset($_SESSION) || !isset($_SESSION['auth-role']) || 'Администратор'!=$_SESSION['auth-role'])
{
	header('Location: ./transit.php?action=login');
	exit;
}

require_once '../assets/helpers/log.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';

?>
<html>
<head>
	<title>Информация о переданных на ЭТП объявлениях о торгах</title>

	<meta http-equiv="X-UA-Compatible" content="IE=10" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="ru" />

	<link rel="stylesheet" type="text/css" href="css/normalize.css" />
	<link rel="stylesheet" type="text/css" href="css/main.css" />

	<script type="text/javascript" src="js/vendors/jquery/jquery.js"></script>
	<script type="text/javascript" src="js/vendors/jquery/jquery.cookie.js"></script>
	<script type="text/javascript" src="js/vendors/json2.js"></script>
	<script type="text/javascript" src="js/vendors/jszip.min.js"></script>

	<!-- вот это надо в extension ы! { -->
	<link rel="stylesheet" type="text/css" href="css/vendors/jquery/jquery-ui.css" />

	<script type="text/javascript" src="js/vendors/jquery/jquery-ui.min.js"></script>
	<script type="text/javascript" src="js/vendors/jquery/jquery.ui.datepicker-ru.js"></script>
	<script type="text/javascript" src="js/vendors/jquery/grid.locale-ru.js"></script>
	<script type="text/javascript" src="js/vendors/jquery/jquery.jqGrid.min.js"></script>
	<script type="text/javascript" src="js/vendors/jquery/jquery.inputmask.js"></script>

	<link rel="stylesheet" type="text/css" href="css/vendors/ui.jqgrid.css" />
	<link rel="stylesheet" type="text/css" href="css/vendors/select2/select2.css" />

	<script type="text/javascript" src="js/vendors/select2/select2.js"></script>
	<script type="text/javascript" src="js/vendors/select2/select2_locale_ru.js"></script>
	<!-- вот это надо в extension ы! } -->

	<script type="text/javascript">
		var form_spec = null;
		function RegisterCpwFormsExtension(extension)
		{
			if ('ama' == extension.key)
			{
				form_spec = extension.forms.transitbidding.CreateController({base_transit_url:'<?= $base_transit_url ?>'});
				var sel = 'body div.cpw-ama-transitbidding';
				form_spec.CreateNew(sel);
			}
		}
	</script>

	<script type="text/javascript" src="js/ama-bidding-transit.js"></script>
	<style>
		body.cpw-ama
		{
			width:1000px;
			margin:0 auto;
		}
		body.cpw-ama > div.cpw-ama-transitbidding
		{
			width:1000px;
			margin:0px auto;
			float:none;
			height:auto;
			display:table
		}
		body.cpw-ama > div.auth
		{
			text-align: right;
		}
		body.cpw-ama > h1
		{
			margin-top: 0px;
			margin-bottom: 0px;
		}
	</style>
</head>
<body class="cpw-ama">
	<div class="auth">
		Вы авторизованы как <?= $_SESSION['auth-role'] ?>.
		<a href="transit.php?action=logout">Выйти..</a>
	</div>
	<h1>Передаваемые через транзитный сайт торги:</h1>
	<div class="cpw-ama-transitbidding">
		Здесь должна быть отображена теблица с передаваемыми торгами..
	</div>
</body>
</html>
