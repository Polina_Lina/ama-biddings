<?php

// получение данных из таблицы transitbidding для jqGrid

session_start();
if (!isset($_SESSION) || !isset($_SESSION['auth-role']) || 'Администратор'!=$_SESSION['auth-role'])
{
	header('HTTP/1.0 403.21 Source access denied');
	exit;
}

require_once '../assets/helpers/log.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';

$sidx = $_GET['sidx']; // get index row - i.e. user click to sort
$sord = $_GET['sord']; // get the direction

if (!$sidx)
{
	$sidx = 'date_access';
	$sord  = 'desc';
}

if (isset($_GET["id_TransitBidding"]))
{
	require_once '../assets/etp/views/singleRecord.php';
}
else
{
	$filter_rule_builders= array
	(
		'etp'=>'std_filter_rule_builder'
		,'IP'=>'std_filter_rule_builder'
		,'token_bidding'=>'std_filter_rule_builder'
		,'URL'=>'std_filter_rule_builder'
		,'date_access'=>'std_filter_rule_builder_dt_compare'
	);
	$fields= "id_TransitBidding, token_bidding, IP, date_access, etp, URL";
	$from_where= "from TransitBidding WHERE 1=1 ";
	$result= execute_query_for_jqgrid_and_return_result($fields, $from_where, $filter_rule_builders , "ORDER BY $sidx $sord");

	require_once "../assets/etp/list.php";

	foreach ($result['rows'] as $b)
	{
		if (isset($etp_list[$b->etp]))
			$b->etp= $etp_list[$b->etp]->Operator;
	}

	echo json_encode($result);
}
