<?

// авторизоваться в административной части системы

if (isset($_POST) && isset($_POST['login']) && isset($_POST['password']))
{
	if ($access_login!=$_POST['login'] || $access_password!=$_POST['password'])
	{
		$extra_message= 'Вы указали незнакомые нам данные для входа';
	}
	else
	{
		session_start();
		$_SESSION['auth-role']= 'Администратор';
		header('Location: ./transit.php?action=admin');
		exit;
	}
}
?>

<html>
<head>
	<title>Вход в систему передачи данных о торгах на ЭТП</title>

	<meta http-equiv="X-UA-Compatible" content="IE=10" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="ru" />

	<script type="text/javascript" src="js/vendors/jquery/jquery.js"></script>
</head>
<style>
	body.cpw-ama.transit.login
	{
		width: 800px;
		margin:30px auto;
	}
	body.cpw-ama.transit.login form
	{
		width: 400px;
		margin:0 auto;
	}
	div.row
	{
		width: 100%;
		margin: 3px;
	}
	div.row div.title
	{
		width: 100px;
		display: inline-block;
	}
	div.row input
	{
		width: 200px;
	}
	div.row input[type="submit"]
	{
		margin-top:5px;
	}
	div.extra
	{
		color: red;
	}
</style>
<body class="cpw-ama transit login">
	<h1>Вход в систему передачи данных о торгах на ЭТП</h1>

	<form method="POST">

		<? if (isset($extra_message)) : ?>
			<div class="extra">
				<?= $extra_message ?><? if (0) { ?>что то какой то не тот пароль у вас..<? } ?>
			</div>
		<? endif; ?>

		<h2>Представьтесь пожалуйста:</h2>

		<div class="row">
			<div class="title">Логин:</div>
			<input type="text" id="id-login" name="login" />
		</div>


		<div class="row">
			<div class="title">Пароль:</div>
			<input type="password" id="id-password" name="password" />
		</div>

		<div class="row">
			<div class="title">&nbsp;</div>
			<input type="submit" value="Представиться" />
		</div>
	</form>

</body>
</html>

