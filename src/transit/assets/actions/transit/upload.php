<?

// Загрузить информацию о торгах для передачи на ЭТП на сервер и получить токен

require_once '../assets/helpers/log.php';
require_once '../assets/helpers/db.php';

require_once '../assets/helpers/texts.php';

$content= $_POST['cfb_data'];
$etp= isset($_POST['etp']) ? $_POST['etp'] : '';

if (get_magic_quotes_gpc())
	$content= stripslashes($content);

/*$content_de_xml = new SimpleXMLElement($content);
$bidding_name= (string)$content_de_xml->Наименование;*/

$token_bidding= uniqid("",true);

write_to_log("token_bidding=$token_bidding");

//TODO: Запись в таблицу access_log
$dateNow = new DateTime();
$dateNowString = $dateNow->format("Y-m-d H:i:s");

execute_query_no_result
(
	'insert into TransitBidding set token_bidding=?, body=?, IP=?, date_access=?, etp=?;'
	,array('sssss',$token_bidding,$content, $_SERVER['REMOTE_ADDR'], $dateNowString, $etp)
);

echo json_encode(array('token_bidding'=>$token_bidding));
exit;
