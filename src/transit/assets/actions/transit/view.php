<?

// посмотреть информацию о торгах связанных с токеном

require_once '../assets/helpers/log.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';

require_once '../assets/helpers/texts.php';

libxml_use_internal_errors(true);

function load_bidding_rows()
{
	global $db_charset;
	global $_GET;
	$transit_bidding_rows= false;
	if (isset($_GET['bidding_token']))
	{
		$token_bidding= $_GET['bidding_token'];
		$txt_query= 'select * from TransitBidding where token_bidding=?;';
		$transit_bidding_rows= execute_query($txt_query,array('s',$token_bidding));
	}
	else if (isset($_GET['token']))
	{
		$token_bidding= $_GET['token'];
		$txt_query= 'select * from TransitBidding where token_bidding=?;';
		$transit_bidding_rows= execute_query($txt_query,array('s',$token_bidding));
	}
	else
	{
		$id_TransitBidding= $_GET['id_TransitBidding'];
		$txt_query= 'select * from TransitBidding where id_TransitBidding=?;';
		$transit_bidding_rows= execute_query($txt_query,array('s',$id_TransitBidding));
	}

	for ($i= 0; $i<count($transit_bidding_rows); $i++)
	{
		if ((!isset($transit_bidding_rows[$i]->IP) || null==$transit_bidding_rows[$i]->IP || ''==$transit_bidding_rows[$i]->IP) && 'latin1'!=$db_charset)
			return 'xml_errors';

		$b= $transit_bidding_rows[$i]->body;
		unset($transit_bidding_rows[$i]->body);
		$transit_bidding_rows[$i]->body_text= $b;

		$fixed_body= @simplexml_load_string($b);
		$xml_errors= libxml_get_errors();
		if (0!=count($xml_errors))
		{
			libxml_clear_errors();
			return 'xml_errors';
		}
	}
	return $transit_bidding_rows;
}

$transit_bidding_rows= load_bidding_rows();
if ('xml_errors'==$transit_bidding_rows)
{
	global $db_charset;
	$db_charset= 'latin1';
	$transit_bidding_rows= load_bidding_rows();
}

$URL_to_redirect= 'redirect' != $_GET['action'] ||
	null==$transit_bidding_rows || !isset($transit_bidding_rows[0]) 
	? '' : $transit_bidding_rows[0]->URL;

?>
<html>
<head>
	<title>Транзитная страница информации о торгах</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="ru" />

	<script type="text/javascript" src="js/vendors/jquery/jquery.js"></script>

	<script type="text/javascript">

		bidding_content= <?= nice_json_encode($transit_bidding_rows) ?>;

		function RegisterCpwFormsExtension(extension)
		{
			if ('ama'==extension.key)
			{
				var sel= 'body div.cpw-ama-bidding-view';
				form_spec = extension.forms.bidding_view.CreateController();
				if (!bidding_content || null==bidding_content || 0>=bidding_content.length)
				{
					$(sel).html('Не обнаружено информации о торгах с указанным токеном..');
				}
				else
				{
					try
					{
						form_spec.SetFormContent(bidding_content[0].body_text);
					}
					catch (ex)
					{
						if (true===ex.parseException)
						{
							alert(ex.readablemsg);
						}
						else
						{
							alert(ex.toString());
						}
						return;
					}
					form_spec.Edit(sel);
				}
			}
		}

		$(function()
		{
			var sel= 'body .cpw-ama-bidding-view-text pre';
			if (!bidding_content || null==bidding_content || 0>=bidding_content.length)
			{
				$(sel).text('Не обнаружено информации о торгах в машинно-читаемом формате с указанным токеном..');
			}
			else
			{
				$(sel).text(bidding_content[0].body_text);
			}
			<? if ('redirect'==$_GET['action'] && null!=$URL_to_redirect && ''!=$URL_to_redirect) : ?>
				window.location='<?= $URL_to_redirect ?>';
			<? endif; ?>
		});
	</script>

	<!-- вот это надо в extension ы! { -->
	<link rel="stylesheet" type="text/css" href="css/vendors/jquery/jquery-ui.css" />

	<script type="text/javascript" src="<?= $base_url = $base_url=" " ? "" : $base_url."/"  ?>js/vendors/jquery/jquery-ui.min.js"></script>
	<!-- вот это надо в extension ы! } -->

	<script type="text/javascript" src="js/ama-bidding-transit.js"></script>
	<style>
	span.cpw-forms-ama-bidding-token
	{
		display: inline-block;
		width: 210px;
	}
	span.cpw-forms-ama-bidding-url
	{
		display: inline-block;
		width: 600px;
	}
	body.transit-bidding-view a
	{
		color: blue;
	}
	body.transit-bidding-view div.cpw-ama-bidding-view
	{
		display:inline-block;
		border-left: 3px solid #aaddaa;
		padding-left: 10px;
		margin-top: 5px;
		margin-bottom: 5px;
	}
	body.transit-bidding-view div.cpw-ama-bidding-view-text .without-text { display: none }
	body.transit-bidding-view div.cpw-ama-bidding-view-text .with-text { display: inline }
	body.transit-bidding-view div.cpw-ama-bidding-view-text.expanded .without-text { display: inline }
	body.transit-bidding-view div.cpw-ama-bidding-view-text.expanded .with-text { display: none }
	body.transit-bidding-view p.no-redirect {color:red;}
	</style>
</head>
<body style="margin:0 auto;width:900px;padding:5px;" class="transit-bidding-view">
	<h1>
		<? if ('redirect'!=$_GET['action']) : ?>
			Транзитная страница информации о торгах
		<? else : ?>
			Перенаправление на страницу торгов на ЭТП
		<? endif; ?>
	</h1>

	<? for ($i= 0; $i<count($transit_bidding_rows); $i++) : ?>
	<?	 $transit_bidding_row= $transit_bidding_rows[$i]; ?>
		<hr/>
		токен передаваемого объявления о торгах: 
		<b><span class="cpw-forms-ama-bidding-token">"<?= $transit_bidding_row->token_bidding ?>"</span></b>, 
		 идентификатор объявления о торгах: 
		<b>"<?= $transit_bidding_row->id_TransitBidding ?>"</b>
		<br/>
		ссылка на страницу торгов на ЭТП: 
		<span class="cpw-forms-ama-bidding-url">
			<? if (''==$transit_bidding_row->URL || null==$transit_bidding_row->URL) : ?>
				&nbsp;
			<? else : ?>
				<a href="<?= $transit_bidding_row->URL ?>"><?= $transit_bidding_row->URL ?></a>
			<? endif; ?>
		</span>
		<br/>
		<? if ('redirect'==$_GET['action'] && (null==$URL_to_redirect || ''==$URL_to_redirect)) : ?>
			<p class="no-redirect">Адрес созданной на ЭТП страницы не зарегистрирован, перенапралять некуда!</p>
		<? endif; ?>
		<div class="cpw-ama-bidding-view">
			Тут должны быть объявления о торгах..
		</div>
		<div class="cpw-ama-bidding-view-text">
			То же самое в машинно-читаемом формате (как сохранено в базе):
			<span class="with-text">
				<a href="#" onclick="$('.cpw-ama-bidding-view-text').addClass('expanded')">Развернуть</a>
			</span>
			<span class="without-text">
				<a href="#" onclick="$('.cpw-ama-bidding-view-text').removeClass('expanded')">Свернуть</a>
				<pre>Тут должны быть объявления о торгах в машинно-читаемом формате..</pre>
			</span>
		</div>
	<? endfor; ?>
	<hr/>
</body>
</html>