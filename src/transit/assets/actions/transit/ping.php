<?

// Вернуть информацию о торгах для сохранения в виде zip-файла

require_once '../assets/helpers/log.php';

require_once '../assets/helpers/texts.php';

$content= $_POST['cfb_data'];

if (get_magic_quotes_gpc())
    $content= stripslashes($content);

$content_de_xml = new SimpleXMLElement($content);
$bidding_name= (string)$content_de_xml->Наименование;

$filebase= slugify($bidding_name);
$filename= $filebase.'.cfb';

$filepath= tempnam(sys_get_temp_dir(), $filebase); //$filename;

$zip = new ZipArchive();
$zip->open($filepath,  ZipArchive::OVERWRITE);
$zip->addFromString('data.xml', $content);
$zip->addFromString('[Content_Types].xml', 
	'<?xml version="1.0" encoding="utf-8"?><Types xmlns="http://schemas.openxmlformats.org/package/2006/content-types">'.
	'<Default Extension="xml" ContentType="text/xml" /></Types>');
$zip->close();

header('Content-Type: application/zip');
header("Content-Disposition: attachment; filename=\"$filename\";" );
header("Content-Transfer-Encoding: binary");
header("Content-Length: ".filesize($filepath));
readfile($filepath);
unlink($filepath);
exit;
