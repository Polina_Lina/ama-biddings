<?php

class Sberbank_ast_codec extends Xml_codec
{
	public $schema= array(
		'tagName'=>'purchase'
		,'fields'=> array
		(
			'bids'=> array('item'=>array('tagName'=>'bid'))
		)
	);

	public function GetRootNamespaceURI() { return 'http://www.norbit.ru/XMLSchema'; }

	function Encode_purchaseinfo($data)
	{
		$purchaseinfo = array
			(
				  'idefrsb'=> $data->Объявление->ЕФРСБ->Номер
				, 'purchasename'=> $data->Наименование
				, 'purchasetypeinfo'=> array
				(
					'purchasetypename'=> $data->Форма_торгов
				)
				, 'sitepublicdate'=> $data->Объявление->ФедеральноеСМИ->дата
				, 'paperpublicdate'=> $data->Объявление->МестноеСМИ->дата
				, 'efrpublicdate'=> '10.10.2017'
			);
		return $purchaseinfo;
	}

	function Encode_debtorinfo($data)
	{
		$должник = $data->Процедура->Должник;
		$debtorinfo = array ( // Сведения о должнике, его имуществе 
			  'personphis'=> 'No'
			, 'debtorname'=> $должник->Наименование
			, 'debtorinn'=> $должник->ИНН
			, 'debtorogrn'=> $должник->ОГРН
		);
		return $debtorinfo;
	}

	function EncodeFIO($fio)
	{
		return $fio->Фамилия.' '.$fio->Имя.' '.$fio->Отчество;
	}

	function Encode_crisicmanagerinfo($data)
	{
		$АУ = $data->Процедура->Арбитражный_управляющий;
		$crisicmanagerinfo= array // Сведения об арбитражном управляющем 
		(
			  'crisicmanagerfullname'=> $this->EncodeFIO($АУ)
			, 'crisismanagerinn'=> $АУ->ИНН
			, 'arbitrageorganizationpanel'=> array('arbitrageorganization'=> $data->Процедура->СРО )
		);
		return $crisicmanagerinfo;
	}

	function Encode_businesinfo($data)
	{
		$businesinfo= array // Сведения об арбитражном суде
		(
			'businessname'=> $data->Процедура->АС
			, 'businessno'=> $data->Процедура->Номер_дела_о_банкротстве
			, 'businessreason'=> ' ' //Обязательное поле
		);
		return $businesinfo;
	}

	function Encode_Дата_Время($dt) { return $dt->дата.' '.$dt->время; }

	function Encode_requestinfo($data)
	{
		$requestinfo= array // Порядок представления заявок на участие в торгах (предложений о цене) 
			(
				  'requeststartdate'=> $this->Encode_Дата_Время($data->Начало_приёма_заявок)
				, 'requeststopdate'=> $this->Encode_Дата_Время($data->Окончание_приёма_заявок)
				, 'registrationdocuments'=> ' ' //Обязательное поле
			);
		return $requestinfo;
	}

	function Encode_terms($data)
	{
		$terms = array // Порядок проведения торгов
		(
			'purchaseauctionstartdate'=> $this->Encode_Дата_Время($data->Начало_торгов)
		);
		return $terms;
	}

	function Encode_data_without_bids($data)
	{
		$sber_data = array
		(
			  'purchaseinfo'=> $this->Encode_purchaseinfo($data)
			, 'debtorinfo'=> $this->Encode_debtorinfo($data)// Сведения о должнике, его имуществе 
			, 'bids'=> array()
			, 'crisicmanagerinfo'=> $this->Encode_crisicmanagerinfo($data) // Сведения об арбитражном управляющем 
			, 'businesinfo'=> $this->Encode_businesinfo($data) // Сведения об арбитражном суде
			, 'requestinfo'=> $this->Encode_requestinfo($data) // Порядок представления заявок на участие в торгах (предложений о цене) 
			, 'terms'=> $this->Encode_terms($data)// Порядок проведения торгов
		);
		return $sber_data;
	}

	private $debtorbidname_address_splitter = "\r\nТочный адрес: ";
	function Encode_data_bid_debtorbidname($Лот)
	{
		return $Лот->Краткое_описание.('' == $Лот->Точный_адрес ? '' : $this->debtorbidname_address_splitter.$Лот->Точный_адрес);
	}

	function Encode_data_bids($data,&$sber_data)
	{
		$bids = $sber_data['bids'];

		for ($i = 0; $i < count($data->Лоты); $i++)
		{
			$Лот = $data->Лоты[$i];

			$sber_data['bids'][]= array(
				'bidpanel'=> array
					(
						'bidinfo'=> array
							(
								  'bidno'=> $i
								, 'bidname'=> $Лот->Наименование
							)
						, 'biddebtorinfo'=> array
							(
								  'debtorbidname'=> $this->Encode_data_bid_debtorbidname($Лот)
								, 'bidregion'=> 'Удмуртия'
								, 'bidcategory'=> '01'
								, 'bidinventoryresearchtype'=> 0 == strlen($Лот->Порядок_ознакомления) ? 'Обычный порядок' : $Лот->Порядок_ознакомления
							)
						, 'bidtenderinfo'=> array
							(
								  'bidprice'=> $Лот->Начальная_цена
								, 'bidauctionsteppercent'=> $Лот->Шаг_аукциона
							)
						, 'biddepositinfo'=> array
							(
								  'isdepositinpercent'=> ('%' == $Лот->Размер_задатка_ед) ? 'Yes' : 'No'
								, 'biddeposit'=> $Лот->Размер_задатка
							)
					)
			);
		};
	}

	function Encode_data($data)
	{
		$sber_data = $this->Encode_data_without_bids($data);
		$this->Encode_data_bids($data, $sber_data);
		//$sber_data['a']= 'b';
		return $sber_data;
	}

	function Encode($data)
	{
		$sber_data= $this->Encode_data($data);
		$dom= $this->Encode_to_dom($sber_data);

		$xml_string= $dom->saveXML();

		$xml_string = preg_replace('/(?:^|\G)  /um', "\t", $xml_string);
		//$xml_string = str_replace(">\n",">\r\n",$xml_string);
		$xml_string = str_replace("\n","\r\n",$xml_string);

		//return $xml_string;

		libxml_use_internal_errors(true);

		$dom_to_read_to= new DOMDocument;
		$dom_to_read_to->loadXml($xml_string);

		if (@$dom_to_read_to->schemaValidate('../assets/actions/to-etp/sberbank-ast/sber-PurchaseCreate.xsd'))
		{
			return $xml_string;
		}
		else
		{
			throw new XmlErrorException('can not write to sberbank xml');
		}
		
	}

	public function Decode($txt)
	{
		return parent::Decode($txt);
	}
}

?>