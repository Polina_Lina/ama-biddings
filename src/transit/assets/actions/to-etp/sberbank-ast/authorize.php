<?

require_once '../assets/helpers/log.php';
require_once '../assets/helpers/db.php';
write_to_log('sberbank-ast/authorize..');

$xml = execute_query("select body from TransitBidding where token_bidding=?;", array('s', $_GET['bidding_token']));

?>
<html>
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=10" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="language" content="ru" />
		<title>Передача информации о торгах на ЭТП Сбербанк-АСТ</title>
<style>
body
{
	margin: 0;
	padding: 0;
	font-family: Arial;
}
div.header
{
	background-color: #f5f6f6;
	height: 95px;
	width: 100%;
	border-bottom: 1px solid #cccccc;
}
div.footer
{
	background-color: #f5f6f6;
	height: 95px;
	width: 100%;
	border-top: 1px solid #cccccc;
}
div.mcolumn
{
	width: 800px;
	margin: 0 auto;
}
div.form form
{
	margin-top: 40px;
	margin-bottom: 40px;
}
div.form div.row
{
	width: 100%;
	margin: 8px;
}
div.form div.row div.title
{
	width: 200px;
	height: 30px;
	display: inline-block;
	margin-left: 100px;
	vertical-align: top;
}
div.form div.row div.value
{
	height: 30px;
	width: 382px;
	display: inline-block;
}
div.form div.row input
{
	width: 382px;
}
div.form input[type="submit"]
{
	float: right;
}
div.header img
{
	margin-top: 20px;
	display: inline-block;
	border: 0;
}
div.header div.title
{
	font-size: 20px;
	margin-top: 26px;
	margin-left: 10px;
	padding-top: 10px;
	padding-bottom: 10px;
	padding-left: 10px;
	vertical-align: top;
	display: inline-block;
	border-left: 1px solid #acacac;
}
div.header div.arrow
{
	font-size: 20px;
	margin-top: 26px;
	padding-top: 10px;
	padding-bottom: 10px;
	margin-right: 8px;
	vertical-align: top;
	display: inline-block;
	color: black;
	font-weight:bold;
}
p.lefttab {
    margin-left:30px;
}
div.requaredFields {
    margin-left:110px; 
    margin-bottom:20px;
}
div.requaredFields div {
    width:100%; 
    font-size:12px;
}
.red {
    color:red;
}
p.titleErrors {
    display:none;    
}
</style>

<script type="text/javascript" src="js/vendors/jquery/jquery.js"></script>
	</head>
	<body>

		<div class="header">
			<div class="mcolumn">
				<a href="http://russianit.ru/software/bunkrupt/"
					><img alt="Помощник арбитражного управляющего" src="img/ama/ico_ama.png" style="width:60px;"
				/></a>
				<div class="arrow">&rArr;</div>
				<img alt="Сбербанк-АСТ" src="img/sberbank/logo.png" />
				<div class="title">Создание черновика объявления о торгах</div>
			</div>
		</div>

		<div class="form mcolumn">
			<?
				$test_mode= (isset($_GET['test-mode']) && 'on'==$_GET['test-mode']);
				$action= 'to-etp.php?etp=sberbank-ast&bidding_token='.$_GET['bidding_token'];
				if ($test_mode)
					$action.= '&test-mode=on';
			?>
			<form method="POST" action="<?= $action ?>">
				<div class="requaredFields">
					<div class="title">
						<p>
							<b>ВНИМАНИЕ!</b> 
							Поля, которые 
							<u>обязательны для заполнения</u> на Сбербанк-АСТ, 
							<u>но
							отсутствуют</u> в ПАУ,<br/>
							будут заполнены неактуальными значениями, чтобы выполнить требования по формату..
						</p>
						<p>
							Поэтому следующие поля <b><u>обязательно</u></b> необходимо будет отредактировать вручную:
							<ol>
								<li>Дата публикации сообщения о проведении торгов в Едином федеральном реестре сведений о банкротстве</li>
								<li>Перечень предоставляемых участниками документов, требования к их оформлению</li>
								<li>Регион выставляемого на торги имущества</li>
								<li>Классификация выставляемого на торги имущества</li>
							</ol>
						</p>
						<p style="font-weight:bold;" class="titleErrors">В данных присутствуют ошибки:</p>  
						<p id="checkPersent" class="lefttab red"></p>      
					</div>
				</div>
				<div class="row">
					<div class="title" style="width:100%;">
						Для создания черновика объявления о торгах укажите параметры доступа:
					</div>
				</div>

				<? if (!$test_mode) :?>
					<div class="row"><div class="title"></div></div>
				<? else :?>
					<div class="row">
						<div class="title" style="width:500px;color:red;">
							ВНИМАНИЕ! Используется тестовая площадка Сбербанка!
						</div>
					</div>
					<div class="row">
						<div class="title" style="width:550px;color:red;margin-left:120px;">
							Созданный черновик нельзя будет использовать для проведения торгов
						</div>
					</div>
					<div class="row">
						<div class="title" style="width:400px;color:red;margin-left:340px;">
							в соответствии с Законом о несостоятельности!
						</div>
					</div>
				<? endif; ?>

				<div class="row">
					<div class="title">Имя пользователя</div>
					<div class="value"><input type="value" name="login" /></div>
				</div>
				<div class="row">
					<div class="title">Пароль</div>
					<div class="value"><input type="password" name="password" /></div>
				</div>
				<div class="row">
					<div class="title"></div>
					<div class="value">
						<a href="http://www.sberbank-ast.ru/PasswordRemind.aspx" target="_blank">
							Забыли пароль?
						</a>
					</div>
				</div>
				<div class="row"><div class="title"></div></div>
				<div class="row">
					<div class="title">
						<a href="<?= $base_transit_url ?>?action=view&bidding_token=<?= $_GET['bidding_token'] ?>" target="_blank" id="open-bidding-draft">
							информация в черновик
						</a>
					</div>
					<div class="value">
						<input type="submit" value="Создать черновик объявления о торгах" />
						<? if ($test_mode) :?>
							<input type="submit" value="Создать черновик объявления о торгах без перехода" name="step-by-step" />
						<? endif; ?>
					</div>
				</div>
			</form>

			<form method="post" action="transit.php?action=ping" style="display:none;" id="downloadXml">
				<input type="hidden" name="cfb_data" value="<? echo htmlspecialchars($xml[0]->body) ?>" />
				<input type="submit" value="Скачать XML" />
			</form>
		</div>

		<div class="footer">
		</div>
		<script type="text/javascript">
			if (sessionStorage.getItem('errors') != null)
			{
				$("p.titleErrors").css("display", "block");
				$("#checkPersent").html(sessionStorage.getItem('errors'));
			}

			$("input.download").click(function () {
				$("form#downloadXml").submit();
			})
		</script>
	</body>
</html>