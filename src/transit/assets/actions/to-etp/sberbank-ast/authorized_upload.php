<?

require_once '../assets/helpers/log.php';
write_to_log('sberbank-ast/authorized_upload..');

require_once '../assets/etp/upload.php';
require_once '../assets/helpers/db.php';

$progress_log= array();
$progress_tab= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
function Progress($progress_txt)
{
	global $progress_log;
	$progress_log[]= array('time'=>date('m/d/Y h:i:s a', time()),'txt'=>$progress_txt);
	write_to_log(str_replace('&nbsp;',"\t",$progress_txt));
}

function ProgressHideOnTest($progress_txt)
{
	global $progress_log;
	$progress_log[]= array('time'=>date('m/d/Y h:i:s a', time()),'txt'=>$progress_txt,'hide-on-test'=>true);
	write_to_log(str_replace('&nbsp;',"\t",$progress_txt));
}

function ProgressAstWebServiceResponse($web_service_responce)
{
	global $progress_tab;
	Progress($progress_tab.'cтатус='.$web_service_responce->Status.', описание='.$web_service_responce->Description);
}

function ProgressArguments($login,$password,$token)
{
	global $progress_tab;
	ProgressHideOnTest($progress_tab.'bidding_token='.$token);
	Progress($progress_tab.'login='.$login);
	Progress($progress_tab.'password='.str_repeat("*", strlen($password)));
}

function UploadBiddingToService($login,$password,$bidding,$sberbank_ast_service,$auth)
{
	global $sberbank_ast_url;

	Progress('Создаём черновик объявления о торгах  на сервисе Сбербанк-АСТ..');
	$register_doc_args= array(
		  'uniqueToken'       =>$auth->UniqueToken
		, 'TSCode'            =>'Bankruptcy'
		, 'actionCode'        =>'PurchaseCreate'
		, 'purchaseTypeId'    =>'11' // Открытый аукцион с открытыми ценами
		, 'buFace'            =>'3'  // Тип организации текущего пользователя
		, 'docId'             =>0    // Регистрация черновика
		, 'xmlData'           =>$bidding->body
		, 'sign'              =>null
		, 'isSignedDoc'       =>false
		, 'ignoreWarning'     =>false
		, 'serviceSetPriceId' =>0
	);
	$register_doc_responce= $sberbank_ast_service->RegisterDocument($register_doc_args);

	if ($register_doc_responce->RegisterDocumentResult->Status != 0)
	{
		Progress('..Недопустимые данные..'); 
	}
	else
	{
		Progress('..Закончили создание черновика объявления о торгах  на сервисе Сбербанк-АСТ..');   

		$register_doc_result= $register_doc_responce->RegisterDocumentResult;

		ProgressAstWebServiceResponse($register_doc_result);
		$reg= simplexml_load_string($register_doc_result->Response);
		$DocID= $reg->Message->DocID;

		$bidding_url= $sberbank_ast_url.'/'
			.$register_doc_args['TSCode'].'/NBT/'
			.$register_doc_args['actionCode'].'/'
			.$register_doc_args['purchaseTypeId'].'/'
			.$register_doc_args['buFace'].'/'
			.$DocID.'/';

		Progress('Ссылка на страницу торгов:');
		ProgressHideOnTest($bidding_url);

		Progress('Записываем ссылку на страницу торгов в транзитную базу данных..');
		execute_query_no_result('update TransitBidding set URL=? where token_bidding=?;',
			array('ss',$bidding_url,$bidding->transit_token));
		Progress('..Закончили запись ссылки на страницу торгов в транзитную базу данных.');

		return $bidding_url;
	}
	return null;
}


function UploadBidding($login,$password,$bidding)
{
	global $sberbank_ast_service_url;
	global $sberbank_ast_service_partner_key;
	global $progress_tab;

	Progress('Создаём соединение с сервисом Сбербанк-АСТ..');
	$url= $sberbank_ast_service_url.'?wsdl';
	Progress($url);
	$sberbank_ast_service= new SoapClient($url, array('trace' => 1, 'cache_wsdl' => WSDL_CACHE_NONE));
	Progress('..Закончили создание соединения с сервисом Сбербанк-АСТ.');

	if (null==$sberbank_ast_service)
	{
		Progress('Не удалось создать соединение с сервисом Сбербанк-АСТ !');
	}
	else
	{
		Progress('Авторизуемся на сервисе Сбербанк-АСТ..');
		Progress("  key=$sberbank_ast_service_partner_key");
		Progress("  login=$login");
		$asterisk_password= str_repeat('*',strlen($password));
		Progress("  password=$asterisk_password");
		$md5_password= md5($password);
		Progress("  md5(password)=$md5_password");
		//Progress($progress_tab.'sberbank_ast_service_partner_key='.$sberbank_ast_service_partner_key);
		$auth_responce= $sberbank_ast_service->Authorization(array(
			  'key'     =>$sberbank_ast_service_partner_key
			, 'login'   =>$login
			, 'password'=>$password
		));
		Progress('..Закончили авторизацию на сервисе Сбербанк-АСТ.');

		$auth_result= $auth_responce->AuthorizationResult;
		ProgressAstWebServiceResponse($auth_result);

		if (0!=$auth_result->Status)
		{
			Progress('Не удалось авторизоваться сервисом Сбербанк-АСТ!');
		}
		else
		{
			$auth= simplexml_load_string($auth_result->Response);
			Progress('Успешно авторизовались с параметрами:');
			Progress('Организация: '.$auth->buFullName.' (ИНН:'.$auth->INN.', КПП:'.$auth->KPP.', GUID:'.$auth->buID.')');
			Progress('Пользователь: '.$auth->PersonFullName);
			//Progress('Токен:'.$auth->UniqueToken);
			Progress('Ключ идентификации: '.$auth->Identifkey);

			return UploadBiddingToService($login,$password,$bidding,$sberbank_ast_service,$auth);
		}
	}
	return null;
}

function Upload()
{
	$login=     $_POST['login'];
	$password=  $_POST['password'];
	$token=     $_GET['bidding_token'];

	ProgressArguments($login,$password,$token);

	Progress('Загружаем данные о торгах из транзитной таблицы..');
	$bidding= SafeDownloadBidding($token);

	if (false==strpos($bidding->body,'</purchase>'))
	{
		require_once('../assets/helpers/codec.xml.php');
		require_once('../assets/actions/to-etp/sberbank-ast/sberbank-ast-codec.php');
		$xml_codec= new Xml_codec();
		$data= $xml_codec->Decode($bidding->body);

		if (isset($data->Лоты) && isset($data->Лоты->Лот) && !is_array($data->Лоты->Лот))
			$data->Лоты= array($data->Лоты->Лот);

		$codec= new Sberbank_ast_codec();
		$bidding->body= $codec->Encode($data);
	}

	Progress('..Закончили загрузку данных о торгах из транзитной таблицы.');

	if (isset($bidding->error) && true==$bidding->error)
	{
		Progress('Загрузка завершилась ошибкой!');
	}
	else
	{
		return UploadBidding($login,$password,$bidding);
	}
	return null;
}

Progress('Получили задание на передачу торгов');

try
{
	$bidding_url= Upload();
}
catch (XmlErrorException $exception)
{
	write_to_log($exception->getMessage());
	write_to_log($exception->errors);
	Progress($exception->getMessage());
	foreach ($exception->errors as $error)
	{
		Progress(str_replace("\n","<br/>",print_r($error,true)));
	}
	Progress('Передача торгов на ЭТП завершилась неудачей!');
}
catch (Exception $exception)
{
	$message= 'Unhandled exception occurred: ' . get_class($exception) . ' - ' . $exception->getMessage();
	write_to_log($message);
	Progress($message);
	Progress('Передача торгов на ЭТП завершилась неудачей!');
}
Progress('Закончили');

?>
<html>
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=10" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="language" content="ru" />
		<title>Передача информации о торгах на ЭТП Сбербанк-АСТ</title>
	</head>
	<body style="width: 800px; margin: 0 auto;">

	<h2 style="margin-top: 20px;">Передача данных о торгах на ЭТП Сбербанк-АСТ
		</h2>

		<style>
			td { padding-bottom: 3px; }
			td.cpw-forms-log-time
			{
				color: silver;
				min-width: 180px;
				vertical-align: top;
			}
		</style>

		<table style="margin-left: 30px;">
			<? $reversed_progress_log= $progress_log; /*array_reverse($progress_log);*/ ?>
			<? if (isset($bidding_url) && null!=$bidding_url) : ?>
				<tr>
					<td class="cpw-forms-log-time"></td>
					<td>
						<? if (!isset($_POST['step-by-step'])) : ?>
							Переходим на страницу торгов..
						<? else : ?>
							<span>Далее можно перейти на страницу торгов.</span>
							<button onclick="window.location='<?= $bidding_url ?>'">
								Перейти на страницу торгов
							</button>
						<? endif; ?>
					</td>
				</tr>
			<? endif; ?>
			<? foreach ($reversed_progress_log as $log_record) : ?>
				<tr <?= !isset($log_record['hide-on-test']) ? '' : ' class="cpw-forms-hide-on-test"' ?> >
					<td class="cpw-forms-log-time"><?= $log_record['time'] ?></td>
					<td><?= $log_record['txt'] ?></td>
				</tr>
			<? endforeach; ?>
		</table>

		<? if (!isset($_POST['step-by-step']) && isset($bidding_url) && null!=$bidding_url) : ?>
			<script type="text/javascript">window.location = '<?= $bidding_url ?>';</script>
		<? endif; ?>

	</body>
</html>