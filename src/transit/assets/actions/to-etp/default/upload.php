<?

require_once '../assets/helpers/log.php';
write_to_log('default upload..');

global $etp;
global $id_etp;

?>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="language" content="ru" />
		<title>Передача информации о торгах на ЭТП</title>
	</head>
	<body style="margin:0 auto;width:650px;padding-top:20px;">

		<h2>
			<a href="http://russianit.ru/software/bunkrupt/"
				><img alt="Помощник арбитражного управляющего" src="img/ama/ico_ama.png" style="width:60px;vertical-align:middle;"
			/></a>
			Работа с передачей информации о торгах на ЭТП
		</h2>

		<? if (!isset($id_etp) || null==$id_etp) : ?>
			<p style="text-align:center;font-size:large;color:red;">
				На какую ЭТП передавать данные, не указано!
			</p>
		<? elseif (!isset($etp) || null==$etp) : ?>
			<p style="text-align:center;font-size:large;color:red;">
				ЭТП с идентификатором 
				"<b style="font-size:large;"><?= $id_etp ?></b>" 
				неизвестна в системе!
			</p>
		<? else : ?>
			<p>
				К сожалению, техническая реализация автоматизированной передачи информации о торгах
			</p>
			<p style="text-align:center;font-size:large;">
				на ЭТП
				<b><?= $etp->Name ?></b>
			</p>
			<p style="text-align:right;">
				пока не завершена.
			</p>

			<p style="font-size:large;">
				Воспользуйтесь, пожалуйста, для размещения объявления о торгах 
			</p>
			<p style="font-size:large;text-align:right">
				сайтом самой ЭТП (<a href="http://<?= $etp->URL ?>"><?= $etp->URL ?></a>).
			</p>

			<p style="text-align:center;">
				<img alt="Under construction" src="img/ama/under-construction.jpg" style="margin:0 auto;"/>
			</p>
		<? endif; ?>

	</body>
</html>
<?