<?php

global $etp_list;

$etp_list= array();

$e= (object)array
(
	 'id'=>       'sberbank-ast'
	,'URL'=>      'utp.sberbank-ast.ru/Bankruptcy/'
	,'Name'=>     'ЗАО «Сбербанк-АСТ»'
	,'Operator'=> 'ЗАО «Сбербанк-АСТ»'
	,'Testing'=>  true
	,'Release'=>  true
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'fabrikant'
	,'URL'=>      'www.fabrikant.ru'
	,'Name'=>     'Межотраслевая торговая система "Фабрикант"'
	,'Operator'=> 'ООО "Фабрикант.ру"'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'lot-online'
	,'URL'=>      'www.lot-online.ru'
	,'Name'=>     'Российский аукционный дом'
	,'Operator'=> 'ОАО "Российский аукционный дом"'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'm-ets'
	,'URL'=>      'www.m-ets.ru'
	,'Name'=>     'Межрегиональная Электронная Торговая Система'
	,'Operator'=> 'ООО «МЭТС»'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'centerr'
	,'URL'=>      'www.centerr.ru'
	,'Name'=>     'Электронная площадка Центра реализации'
	,'Operator'=> 'ООО "Центр реализации"'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'ausib'
	,'URL'=>      'www.ausib.ru'
	,'Name'=>     'Аукционы Сибири'
	,'Operator'=> 'ООО "Аукционы Сибири"'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'atctrade'
	,'URL'=>      'www.atctrade.ru'
	,'Name'=>     'Электронная площадка "Аукционный тендерный центр"'
	,'Operator'=> 'Общество с ограниченной ответственностью "Аукционный тендерный центр"'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'seltim'
	,'URL'=>      'www.seltim.ru'
	,'Name'=>     'Электронная площадка "Система Электронных Торгов Имуществом" (СЭЛТИМ)'
	,'Operator'=> 'ООО "Сатурн"'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'utpl'
	,'URL'=>      'utpl.ru'
	,'Name'=>     'Объединенная Торговая Площадка'
	,'Operator'=> 'ЗАО "Объединенная Торговая Площадка"'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'utender'
	,'URL'=>      'utender.ru/'
	,'Name'=>     'uTender '
	,'Operator'=> 'ООО "ЮТендер"'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'meta-invest'
	,'URL'=>      'www.meta-invest.ru'
	,'Name'=>     'МЕТА-ИНВЕСТ'
	,'Operator'=> 'ОАО "ИК"МЕТА"'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'arbitat'
	,'URL'=>      'www.arbitat.ru'
	,'Name'=>     'Арбитат'
	,'Operator'=> 'ООО "АРБИТАТ"'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'cdtrf'
	,'URL'=>      'www.cdtrf.ru'
	,'Name'=>     'Центр дистанционных торгов'
	,'Operator'=> 'ЗАО «Центр Дистанционных Торгов»'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'ru-trade24'
	,'URL'=>      'www.ru-trade24.ru'
	,'Name'=>     'Ru-Trade24'
	,'Operator'=> 'ООО "Ру-Трейд"'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'aistorg'
	,'URL'=>      'www.aistorg.ru'
	,'Name'=>     'АИСТ'
	,'Operator'=> 'ООО "Автоматизированная информационная система торгов"'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'ipsetp'
	,'URL'=>      'www.ipsetp.ru'
	,'Name'=>     'АЙПИЭС ЭТП'
	,'Operator'=> 'ООО "АйПиЭс ЭТП"'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'arbitrade'
	,'URL'=>      'arbitrade.ru'
	,'Name'=>     'АрбиТрейд'
	,'Operator'=> 'ООО "Центр электронной торговли "АрбиТрейд"'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'etp-bankrotstvo'
	,'URL'=>      'www.etp-bankrotstvo.ru'
	,'Name'=>     'Банкротство РТ'
	,'Operator'=> 'ООО "Электронная торговая площадка"'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'торговая-площадка-вэтп'
	,'URL'=>      'торговая-площадка-вэтп.рф'
	,'Name'=>     'Всероссийская Электронная Торговая Площадка'
	,'Operator'=> 'ООО "ВЭТП"'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'mse'
	,'URL'=>      'www.etp.mse.ru'
	,'Name'=>     'МФБ'
	,'Operator'=> 'ОТКРЫТОЕ АКЦИОНЕРНОЕ ОБЩЕСТВО "КЛИРИНГОВЫЙ ЦЕНТР МФБ"'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'opentp'
	,'URL'=>      'www.opentp.ru'
	,'Name'=>     'Открытая торговая площадка'
	,'Operator'=> 'ООО "Открытая торговая площадка"'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'sibtorgi'
	,'URL'=>      'www.sibtorgi.ru'
	,'Name'=>     'Сибирская электронная площадка'
	,'Operator'=> 'ООО "СибЭПл"'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'etp-agenda'
	,'URL'=>      'www.etp-agenda.ru'
	,'Name'=>     'ЭТП "Агенда"'
	,'Operator'=> 'ООО "Агенда"'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'ets24'
	,'URL'=>      'www.ets24.ru'
	,'Name'=>     'ЭТС24'
	,'Operator'=> 'ФГУП "Электронные торги и безопасность"'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'propertytrade'
	,'URL'=>      'www.propertytrade.ru'
	,'Name'=>     '«Property Trade»'
	,'Operator'=> 'ООО «ОТС»'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'rus-on'
	,'URL'=>      'www.rus-on.ru'
	,'Name'=>     '«RUSSIA OnLine»'
	,'Operator'=> 'ООО «РУССИА ОнЛайн»'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'e-tender'
	,'URL'=>      'www.e-tender.su'
	,'Name'=>     '«Е-Тендер»'
	,'Operator'=> 'ООО «Электронная площадка «Санкт-Петербург»'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'nistp'
	,'URL'=>      'nistp.ru'
	,'Name'=>     '«Новые информационные сервисы»'
	,'Operator'=> 'ЗАО «Новые информационные сервисы»'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'regtorg'
	,'URL'=>      'www.regtorg.com'
	,'Name'=>     '«Региональная Торговая площадка»'
	,'Operator'=> 'ООО «Сирин»'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'selt-online'
	,'URL'=>      'www.selt-online.ru'
	,'Name'=>     '«Системы ЭЛектронных Торгов»'
	,'Operator'=> 'ООО «СЭлТ»'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'tendergarant'
	,'URL'=>      'www.tendergarant.com'
	,'Name'=>     '«ТЕНДЕР ГАРАНТ»'
	,'Operator'=> 'ООО «ТЕНДЕР ГАРАНТ»'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'vertrades'
	,'URL'=>      'www.vertrades.ru'
	,'Name'=>     '«Электронная площадка «Вердиктъ»'
	,'Operator'=> 'ООО «Электронная площадка «Вердиктъ»'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'electro-torgi'
	,'URL'=>      'www.bankrupt.electro-torgi.ru'
	,'Name'=>     '«Электронная торговая площадка ELECTRO-TORGI.RU»'
	,'Operator'=> 'ЗАО «Вэллстон»'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'b2b-center'
	,'URL'=>      'www.b2b-center.ru'
	,'Name'=>     'B2B-Center'
	,'Operator'=> 'ОАО «Центр развития экономики»'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'kartoteka'
	,'URL'=>      'etp.kartoteka.ru'
	,'Name'=>     'KARTOTEKA.RU'
	,'Operator'=> 'ООО "Коммерсантъ КАРТОТЕКА"'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'uralbidin'
	,'URL'=>      'www.uralbidin.ru'
	,'Name'=>     'UralBidIn'
	,'Operator'=> 'ООО "УралБидИн"'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'akosta'
	,'URL'=>      'www.akosta.info'
	,'Name'=>     'АКОСТА info'
	,'Operator'=> 'ООО «А-Коста»'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'alfalot'
	,'URL'=>      'www.alfalot.ru'
	,'Name'=>     'Альфалот'
	,'Operator'=> 'ООО "Аукционы Федерации"'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'aukcioncenter'
	,'URL'=>      'www.aukcioncenter.ru'
	,'Name'=>     'Аукцион-центр'
	,'Operator'=> 'Общество с ограниченной ответственностью «ИстКонсалтингГрупп»'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'torgidv'
	,'URL'=>      'www.torgidv.ru'
	,'Name'=>     'Аукционы Дальнего Востока'
	,'Operator'=> 'ООО "Аукционы Дальнего Востока"'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'bepspb'
	,'URL'=>      'www.bepspb.ru/'
	,'Name'=>     'Балтийская электронная площадка'
	,'Operator'=> 'ООО "Балтийская электронная площадка"'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'TORGIBANKROT'
	,'URL'=>      'www.TORGIBANKROT.RU'
	,'Name'=>     'ООО «Специализированная организация по проведению торгов – Южная Электронная Торговая Площадка»'
	,'Operator'=> 'ООО «Специализированная организация по проведению торгов – Южная Электронная Торговая Площадка»'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'sibtoptrade'
	,'URL'=>      'www.sibtoptrade.ru/'
	,'Name'=>     'Сибирская торговая площадка'
	,'Operator'=> 'Общество с ограниченной ответственностью «Сибирская торговая площадка»'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'vtb-center'
	,'URL'=>      'vtb-center.ru'
	,'Name'=>     'Система электронных торгов и муниципальных аукционов "ВТБ-Центр"'
	,'Operator'=> 'ООО "Модный дом"'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'tenderstandart'
	,'URL'=>      'www.tenderstandart.ru'
	,'Name'=>     'ТендерСтандарт'
	,'Operator'=> 'ООО "ТендерСтандарт"'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'etpu'
	,'URL'=>      'etpu.ru/'
	,'Name'=>     'Уральская электронная торговая площадка'
	,'Operator'=> 'ЗАО "Уральская электронная торговая площадка"'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'etp1'
	,'URL'=>      'www.etp1.ru'
	,'Name'=>     'Электронная площадка №1'
	,'Operator'=> 'ООО ”Электронная площадка №1”'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'vitnw'
	,'URL'=>      'etp.vitnw.ru'
	,'Name'=>     'Электронная площадка Группы компаний ВИТ'
	,'Operator'=> 'ООО "ВИТ"'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'el-torg'
	,'URL'=>      'el-torg.com '
	,'Name'=>     'Электронная площадка ЭСП'
	,'Operator'=> 'ООО " Электронные системы Поволжья"'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'eurtp'
	,'URL'=>      'eurtp.ru/'
	,'Name'=>     'Электронная торговая площадка "Евразийская торговая площадка"'
	,'Operator'=> 'ООО "Евразийская торговая площадка"'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'auction63'
	,'URL'=>      'www.auction63.ru'
	,'Name'=>     'Электронная Торговая Площадка "ПОВОЛЖСКИЙ АУКЦИОННЫЙ ДОМ"'
	,'Operator'=> 'ООО "ПОВОЛЖСКИЙ АУКЦИОННЫЙ ДОМ"'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'etp-profit'
	,'URL'=>      'www.etp-profit.ru'
	,'Name'=>     'Электронная торговая площадка "Профит"'
	,'Operator'=> 'Общество с ограниченной ответственностью "Перспектива"'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'gloriaservice'
	,'URL'=>      'gloriaservice.ru'
	,'Name'=>     'Электронная торговая площадка "Регион"'
	,'Operator'=> 'ООО"Глория Сервис"'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'eksystems'
	,'URL'=>      'www.eksystems.ru'
	,'Name'=>     'Электронный капитал'
	,'Operator'=> 'ЗАО «Электронный капитал»'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'promkonsalt'
	,'URL'=>      'www.promkonsalt.ru'
	,'Name'=>     'ЭТП "Пром-Консалтинг"'
	,'Operator'=> 'ООО "ПРОМ-Консалтинг"'
);
$etp_list[$e->id]= $e;

$e= (object)array
(
	 'id'=>       'trust'
	,'URL'=>      'bankrot.me/bidding-transit/test-etp.php'
	,'Name'=>     'ЭТП "Тест"'
	,'Operator'=> 'ООО "ТЕСТ"'
);
$etp_list[$e->id]= $e;
