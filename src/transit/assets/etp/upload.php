<?php

require_once '../assets/helpers/log.php';
require_once '../assets/helpers/db.php';

function SafeGetUserId($name)
{
	$txt_query= 'select * from User where Name=?;';
	$users= execute_query($txt_query,array('s',$name));
	if (0!=count($users))
	{
		return $users[0]->id_User;
	}
	else
	{
		return execute_query_get_last_insert_id('insert into User set Name=?;',array('s',$name));
	}
}

function LoadBiddingsByToken($token)
{
	$txt_query= 'select b.*, u.Name UserName from Bidding b inner join User u on b.id_User=u.id_User where transit_token=?;';
	return execute_query($txt_query,array('s',$token));
}

function LoadBidding($id_Bidding)
{
	$txt_query= 'select b.*, u.Name UserName from Bidding b inner join User u on b.id_User=u.id_User where id_Bidding=?;';
	return execute_query($txt_query,array('s',$id_Bidding));
}

function SafeCreateBiddingForToken($token)
{
	write_to_log("SafeCreateBiddingForToken { token=$token");
	$biddings= LoadBiddingsByToken($token);
	write_to_log(print_r($biddings,true));

	if (0!=count($biddings))
	{
		write_to_log('0!=count($biddings)');
		$bidding= $biddings[0];
		$bidding->existed= true;
	}
	else
	{
		write_to_log('0==count($biddings)');
		$id_User= SafeGetUserId($_SESSION['user_name']);
		write_to_log("id_User=$id_User");
		$id_Bidding= execute_query_get_last_insert_id('insert into Bidding set transit_token=?, id_User=?, tmCreated=now();',array('ss',$token,$id_User));
		$bidding= (object)array();
		$bidding->id_Bidding= $id_Bidding;
		$bidding->UserName= $_SESSION['user_name'];
		$bidding->transit_token= $token;
	}
	write_to_log('SafeCreateBiddingForToken }');
	return $bidding;
}

function SafeSetUrlForTransitBidding($token,$id_Bidding)
{
	global $base_transit_url;
	global $base_testetp_url;

	$etp_url= $base_testetp_url.'?action=bidding&id_Bidding='.$id_Bidding;

	$ch = curl_init();
	$url= $base_transit_url.'?action=set-url&bidding_token='.$token.'&etp-url='.urlencode($etp_url);
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch, CURLOPT_MAXREDIRS, 3);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	$responce= curl_exec($ch);
	$errno= curl_errno($ch);
	if (0!=$errno)
	{
		write_to_log('errno='.curl_errno($ch));
		write_to_log('error='.curl_error($ch));
		return false;
	}
	curl_close($ch);
	return 'ok'==json_decode($responce)->result;
}

function SafeDownloadBidding($token)
{
	global $base_transit_url;
	$ch = curl_init();
	$url= $base_transit_url.'?action=download&bidding_token='.$token;
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch, CURLOPT_MAXREDIRS, 3);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	$downloaded_xml_txt= curl_exec($ch);
	$errno= curl_errno($ch);
	if (0!=$errno)
	{
		write_to_log('errno='.curl_errno($ch));
		write_to_log('error='.curl_error($ch));
	}
	curl_close($ch);
	$bidding= (object)array();
	$bidding->transit_token= $token;
	if (0!=$errno)
	{
		$bidding->error= true;
	}
	else
	{
		$bidding->body= $downloaded_xml_txt;
	}
	return $bidding;
}

function SafeDownloadAndStoreBidding($token,$id_Bidding)
{
	$bidding= SafeDownloadBidding($token);
	$bidding->id_Bidding= $id_Bidding;
	if (!isset($bidding->error) || true!=$bidding->error)
		execute_query_no_result('update Bidding set body=?, tmDownloaded=now() where id_Bidding=?;',array('ss',$bidding->body,$id_Bidding));
	return $bidding;
}


