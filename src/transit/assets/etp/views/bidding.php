<?php

require_once '../assets/helpers/json.php';

?>

Торги <b>ID<?= $bidding->id_Bidding ?></b>
зарегистрированные на площадке 
<? if (isset($bidding->tmCreated)) : ?>
	<b><span class="cpw-forms-ama-bidding-time-create"><?= $bidding->tmCreated ?></span></b> 
<? endif; ?>
<? if (isset($bidding->UserName )) : ?>
	пользователем <b><?= $bidding->UserName ?></b>
<? endif; ?>
c транзитным токеном <b><span class="cpw-forms-ama-bidding-token"><?= $bidding->transit_token ?></span></b>.

<? 
	$parsed_bidding_body= !isset($bidding->body) ? null : simplexml_load_string($bidding->body);
	if (false===$parsed_bidding_body)
	{
		?><br/><br/><?
		write_to_log('некорректное тело торгов:');
		write_to_log($bidding->body);
		throw new Exception("Некорректный формат информации о торгах!");
	}
?>

<script type="text/javascript">
	bidding_content= <?= nice_json_encode($parsed_bidding_body) ?>;
	if (null!=bidding_content && false!=bidding_content)
	{
		bidding_content.Лоты= !bidding_content.Лоты ? [] : !bidding_content.Лоты.Лот.length ? [bidding_content.Лоты.Лот] : bidding_content.Лоты.Лот;
		for (var i= 0; i < bidding_content.Лоты.length; i++)
		{
			var Лот= bidding_content.Лоты[i];
			if ('object' == typeof Лот.Порядок_ознакомления)
				Лот.Порядок_ознакомления= '';
		}
	}

	function RegisterCpwFormsExtension(extension)
	{
		if ('ama'==extension.key)
		{
			var sel= 'body div.cpw-ama-bidding-view';
			form_spec = extension.forms.bidding_view.CreateController();
			if (!bidding_content || null==bidding_content || 0>=bidding_content.length)
			{
				$(sel).html('Не получается отобразить информацию о торгах с указанным токеном..');
			}
			else
			{
				form_spec.SetFormContent(bidding_content);
				form_spec.Edit(sel);
			}
		}
	}
</script>
<script type="text/javascript" src="js/ama-bidding-transit.js"></script>
<div class="cpw-ama-bidding-view">
	Тут должна быть информация из объявления о торгах..
</div>