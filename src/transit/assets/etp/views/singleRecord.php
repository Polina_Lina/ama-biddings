<?php 
$singleRecord = execute_query("SELECT id_TransitBidding, token_bidding, body, URL, IP, date_access, etp FROM transitbidding WHERE id_TransitBidding = ?", array('s', $_GET["id_TransitBidding"]));

$token_bidding= $singleRecord[0]->token_bidding;

$cfb_content= execute_query('select * from TransitBidding where token_bidding=?;',array('s',$token_bidding));

for ($i= 0; $i<count($cfb_content); $i++)
{
    $b= $cfb_content[$i]->body;
    write_to_log($b);
    write_to_log(simplexml_load_string($b));

    $cfb_content[$i]->body= simplexml_load_string($b);
}
?>

<div class="singleRecord" style="width:70%; margin:20px 0 0 20px;">
<p><b>Id</b>: <? echo $singleRecord[0]->id_TransitBidding;  ?> </p>
<p><b>IP адрес</b>: <? echo $singleRecord[0]->IP; ?> </p>
<p><b>Токен</b>: <? echo $singleRecord[0]->token_bidding; ?> </p>
<p><b>Адрес URL</b>: <a href="<? echo $singleRecord[0]->URL; ?>" target="_blank"><? echo $singleRecord[0]->URL; ?></a></p>
<p><b>Дата обращения</b>: <? echo $singleRecord[0]->date_access; ?> </p>
<p><b>ЭТП назначения</b>: <? echo $singleRecord[0]->etp; ?> </p>
<p style="margin-bottom:0px;"><b>Содержимое</b>:</p><div class="cpw-ama-bidding-view"></div>
<p><a href="javascript:return false;" class="showXml"><b>Содержимое Xml</b></a></p>
<div style="display:none;" class="divXml"><? echo htmlspecialchars($singleRecord[0]->body); ?></div>
</div>

<script type="text/javascript" src="js/ama-bidding-transit.js"></script>
<script type="text/javascript" src="js/vendors/jquery/jquery.js"></script>
<script type="text/javascript" src="<?= $base_url = $base_url=" " ? "" : $base_url."/"  ?>js/vendors/jquery/jquery-ui.min.js"></script>
<script type="text/javascript">	        
    bidding_content= <?= nice_json_encode($cfb_content) ?>;    

    var sberbank_format = false;

    for (var i= 0; i < bidding_content.length; i++)
    {		    
        if (bidding_content[i].etp == "ЗАО «Сбербанк-АСТ»")  {
            bidding_content[i].body.bids = bidding_content[i].body.bids.bid;	
            sberbank_format = true;
        }
        else {
            bidding_content[i].body.Лоты= bidding_content[i].body.Лоты.Лот;
        }		    		                
    }

    function RegisterCpwFormsExtension(extension)
    {
        if ('ama'==extension.key)
        {
            var sel= 'body div.cpw-ama-bidding-view';
			    
            //Проверка на формат xml
            if (sberbank_format) {
                form_spec = extension.forms.bidding_view_sberbank.CreateController();
            }
            else {
                form_spec = extension.forms.bidding_view.CreateController();
            }
				
            if (!bidding_content || null==bidding_content || 0>=bidding_content.length)
            {
                $(sel).html('Не обнаружено информации о торгах с указанным токеном..');
            }
            else
            {				    
                form_spec.SetFormContent(bidding_content[0].body);
                form_spec.Edit(sel);
            }
        }
    }    
    
    $(".showXml").click(function() {
        $(".divXml").toggle("fast");
    })
</script>