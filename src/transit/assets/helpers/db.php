<?

global $db_host;
global $db_user;
global $db_password;
global $db_name;

function mysqli_stmt_get_result_without_mysqlnd( $Statement )
{
	$RESULT = array();
	$Statement->store_result();
	for ( $i = 0; $i < $Statement->num_rows; $i++ )
	{
		$Metadata = $Statement->result_metadata();
		$PARAMS = array();
		while ( $Field = $Metadata->fetch_field() )
		{
			$PARAMS[] = &$RESULT[ $i ][ $Field->name ];
		}
		call_user_func_array( array( $Statement, 'bind_result' ), $PARAMS );
		$Statement->fetch();
		$RESULT[$i]= (object)$RESULT[$i];
	}
	return $RESULT;
}

function execute_query_internal($txt_query,$parameters,$return_result)
{
	global $db_host;
	global $db_user;
	global $db_password;
	global $db_name;
	global $db_charset;

	$db_link = mysqli_connect($db_host, $db_user, $db_password, $db_name);

	if (!$db_link)
		die('Ошибка соединения: ' . mysql_error());

	if (!mysqli_set_charset($db_link, $db_charset))
		die('Ошибка соединения при установке кодировки: ' . mysql_error());

	$stmt= mysqli_stmt_init($db_link);

	$query_result= null;
	if (!mysqli_stmt_prepare($stmt, $txt_query))
	{
		write_to_log('mysqli_error: '.mysqli_error($db_link));
		write_to_log('can not prepare query: '.$txt_query);
		throw new Exception('can not prepare query!');
	}
	else
	{
		$parameters_len= count($parameters);
		if ($parameters_len>1)
		{
			$blobs= array();
			$types= $parameters[0];
			for ($i= 0; $i<$parameters_len-1; $i++)
			{
				if ('b'==substr($types,$i,1))
				{
					$blobs[]= $parameters[$i+1];
					$parameters[$i+1]= null;
				}
			}
			switch (count($parameters))
			{
				case 0: break;
				case 1: break;
				case 2: mysqli_stmt_bind_param($stmt, $parameters[0], $parameters[1]); break;
				case 3: mysqli_stmt_bind_param($stmt, $parameters[0], $parameters[1], $parameters[2]); break;
				case 4: mysqli_stmt_bind_param($stmt, $parameters[0], $parameters[1], $parameters[2], $parameters[3]); break;
				case 5: mysqli_stmt_bind_param($stmt, $parameters[0], $parameters[1], $parameters[2], $parameters[3], $parameters[4]); break;
				case 6: mysqli_stmt_bind_param($stmt, $parameters[0], $parameters[1], $parameters[2], $parameters[3], $parameters[4], $parameters[5]); break;
				case 7: mysqli_stmt_bind_param($stmt, $parameters[0], $parameters[1], $parameters[2], $parameters[3], $parameters[4], $parameters[5], $parameters[6]); break;
				case 8: mysqli_stmt_bind_param($stmt, $parameters[0], $parameters[1], $parameters[2], $parameters[3], $parameters[4], $parameters[5], $parameters[6], $parameters[7]); break;
				case 9: mysqli_stmt_bind_param($stmt, $parameters[0], $parameters[1], $parameters[2], $parameters[3], $parameters[4], $parameters[5], $parameters[6], $parameters[7], $parameters[8]); break;
				case 10: mysqli_stmt_bind_param($stmt, $parameters[0], $parameters[1], $parameters[2], $parameters[3], $parameters[4], $parameters[5], $parameters[6], $parameters[7], $parameters[8], $parameters[9]); break;
				case 11: mysqli_stmt_bind_param($stmt, $parameters[0], $parameters[1], $parameters[2], $parameters[3], $parameters[4], $parameters[5], $parameters[6], $parameters[7], $parameters[8], $parameters[9], $parameters[10]); break;
				case 12: mysqli_stmt_bind_param($stmt, $parameters[0], $parameters[1], $parameters[2], $parameters[3], $parameters[4], $parameters[5], $parameters[6], $parameters[7], $parameters[8], $parameters[9], $parameters[10], $parameters[11]); break;
				case 13: mysqli_stmt_bind_param($stmt, $parameters[0], $parameters[1], $parameters[2], $parameters[3], $parameters[4], $parameters[5], $parameters[6], $parameters[7], $parameters[8], $parameters[9], $parameters[10], $parameters[11], $parameters[12]); break;
				case 14: mysqli_stmt_bind_param($stmt, $parameters[0], $parameters[1], $parameters[2], $parameters[3], $parameters[4], $parameters[5], $parameters[6], $parameters[7], $parameters[8], $parameters[9], $parameters[10], $parameters[11], $parameters[12], $parameters[13]); break;
			}
			$i= 0;
			foreach ($blobs as $blob)
			{
				mysqli_stmt_send_long_data ($stmt,$i,$blob);
				$i++;
			}
		}
		if (!mysqli_stmt_execute($stmt))
		{
			write_to_log('mysqli_error: '.mysqli_error($db_link));
			write_to_log('can not execute query: '.$txt_query);
			throw new Exception('can not execute query!');
		}
		if (true===$return_result)
		{
			$query_result= array();
			$result = mysqli_stmt_get_result_without_mysqlnd($stmt);
			while ($row = array_shift($result))
			{
				$query_result[]= $row;
			}
		}
		else if (false==$return_result)
		{
		}
		else if (3==$return_result)
		{
			$query_result= mysqli_insert_id($db_link);
		}
		else if (isset($return_result))
		{
			write_to_log($return_result);
		}
		else
		{
			write_to_log('!isset(return_result)');
		}
	}
	mysqli_close($db_link);

	if (false!=$return_result)
		return $query_result;
}

function execute_query($txt_query,$parameters)
{
	return execute_query_internal($txt_query,$parameters,true);
}

function execute_query_get_last_insert_id($txt_query,$parameters)
{
	return execute_query_internal($txt_query,$parameters,3);
}

function execute_query_no_result($txt_query,$parameters)
{
	execute_query_internal($txt_query,$parameters,false);
}

function std_filter_rule_builder_dt_compare($rule)
{
	$field= $rule->field;
	$txt= trim($rule->data);
	$txt_len= strlen($txt);
	if (2>$txt_len)
	{
		return "";
	}
	else
	{
		$c= substr($txt,0,1);
		switch ($c)
		{
			case '>':
			case '<':
				$tail= trim(substr($txt,1,$txt_len-1));
				$res= " and $field $c '$tail' ";
				return $res;
			default: 
				return " and $field = '$txt' ";
		}
	}
	
}

function std_filter_rule_builder($rule)
{
	$field= $rule->field;
	$where= " and $field";
	switch ($rule->op) // В будущем будет больше вариантов для всех вохможных условий jqGrid
	{
		case 'eq': $where .= " = '".$rule->data."'"; break;
		case 'ne': $where .= " != '".$rule->data."'"; break;
		case 'bw': $where .= " LIKE '".$rule->data."%'"; break;
		case 'bn': $where .= " NOT LIKE '".$rule->data."%'"; break;
		case 'ew': $where .= " LIKE '%".$rule->data."'"; break;
		case 'en': $where .= " NOT LIKE '%".$rule->data."'"; break;
		case 'cn': $where .= " LIKE '%".$rule->data."%'"; break;
		case 'nc': $where .= " NOT LIKE '%".$rule->data."%'"; break;
		case 'nu': $where .= " IS NULL"; break;
		case 'nn': $where .= " IS NOT NULL"; break;
		case 'in': $where .= " IN ('".str_replace(",", "','", $rule->data)."')"; break;
		case 'ni': $where .= " NOT IN ('".str_replace(",", "','", $rule->data)."')"; break;
	}
	return $where;
}

function execute_query_for_jqgrid_and_return_result($fields,$from_where,$filter_rule_builders,$order='')
{
	$where= '';
	if (isset($_GET['filters']))
	{
		$filters = $_GET['filters'];
		$filters= str_replace("\\\"", "\"", $filters);
		$filters = json_decode($filters);
		if (isset($filters->rules))
		{
			foreach ($filters->rules as $index => $rule)
			{
				$field= $rule->field;
				if (is_callable($filter_rule_builders[$field]))
				{
					$func= $filter_rule_builders[$field];
					$where.= $func($rule);
				}
			}
		}
		
	}
	$page= 0;
	$limit_position= 0;
	$limit_size= 10;
	if (isset($_GET['rows']))
	{
		$limit_size= $_GET['rows'];
		if (isset($_GET['page']))
		{
			$page= $_GET['page'];
			$limit_position= ($page-1)*$limit_size;
		}
	}

	$txt_query= "select $fields $from_where $where $order limit $limit_position, $limit_size;";
	$rows= execute_query($txt_query,array());

	$txt_query= "select count(*) count $from_where $where";
	$rows_count= execute_query($txt_query,array());
	$rows_count= $rows_count[0]->count;

	return array(
		'page'=>$page,
		'total'=>ceil($rows_count/$limit_size),
		'records'=>$rows_count,
		'rows'=>$rows
	);
}

function execute_query_for_jqgrid($fields,$from_where,$filter_rule_builders,$order='')
{
	$result= execute_query_for_jqgrid_and_return_result($fields,$from_where,$filter_rule_builders,$order);
	echo json_encode($result);
}
