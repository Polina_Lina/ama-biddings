<?

require_once '../assets/config.php';

mb_internal_encoding("utf-8");
mb_http_output( "UTF-8" );
mb_http_input( "UTF-8" );

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');

global $trace_methods;
if ($trace_methods)
	require_once '../assets/helpers/log.php';
?>

<html>
<head>
	<title>Тестовая страница для отработки интеграции ПАУ и ЭТП</title>
	<meta http-equiv="X-UA-Compatible" content="IE=10" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="ru" />
	<script type="text/javascript">
		var base_transit_url = '<?= $base_transit_apps_url ?>';
		function global_external_AMA_SaveBiddings(){}
		window.cpw_bidding_demo_print = true;
		function RegisterCpwFormsExtension(extension)
		{
			if ('ama'==extension.key)
			{
				var sel= 'body div.cpw-ama-biddings';
				form_spec = extension.forms['demo-biddings'].CreateController({ base_transit_url: base_transit_url });
				form_spec.CreateNew(sel);
			}
		}
	</script>

	<link rel="stylesheet" type="text/css" href="css/normalize.css" />
	<link rel="stylesheet" type="text/css" href="css/main.css" />

	<script type="text/javascript" src="js/vendors/jquery/jquery.js"></script>
	<script type="text/javascript" src="js/vendors/jquery/jquery.cookie.js"></script>
	<script type="text/javascript" src="js/vendors/json2.js"></script>
	<script type="text/javascript" src="js/vendors/jszip.min.js"></script>

	<!-- вот это надо в extension ы! { -->
	<link rel="stylesheet" type="text/css" href="css/vendors/jquery/jquery-ui.css" />

	<script type="text/javascript" src="js/vendors/jquery/jquery-ui.min.js"></script>
	<script type="text/javascript" src="js/vendors/jquery/jquery.ui.datepicker-ru.js"></script>
	<script type="text/javascript" src="js/vendors/jquery/grid.locale-ru.js"></script>
	<script type="text/javascript" src="js/vendors/jquery/jquery.jqGrid.min.js"></script>
	<script type="text/javascript" src="js/vendors/jquery/jquery.inputmask.js"></script>

	<link rel="stylesheet" type="text/css" href="css/vendors/ui.jqgrid.css" />
	<link rel="stylesheet" type="text/css" href="css/vendors/select2/select2.css" />

	<script type="text/javascript" src="js/vendors/select2/select2.js"></script>
	<script type="text/javascript" src="js/vendors/select2/select2_locale_ru.js"></script>
	<!-- вот это надо в extension ы! } -->

	<script type="text/javascript" src="js/ama-biddings-demo.js"></script>
	<style>
	body.cpw-ama.demo div.ama-biddings-biddings a.ama-icon
	{
		background-color: transparent;
		vertical-align: middle;
		cursor: pointer;
		background: url(img/ama/icons_sprite.png) no-repeat;
	}
	body.cpw-ama.demo div.ama-biddings-biddings a.ama-icon-remove       { background-position: -28px -733px; }
	body.cpw-ama.demo div.ama-biddings-biddings a.ama-icon-remove:hover { background-position: -28px -660px; }
	body.cpw-ama.demo div.ama-biddings-biddings a.ama-icon-edit         { background-position: -28px -1080px; }
	body.cpw-ama.demo div.ama-biddings-biddings a.ama-icon-edit:hover   { background-position: -28px -1008px; }
	body.cpw-ama.demo div.ama-biddings-biddings a.ama-icon-plus         { background-position: -19px -306px; }
	body.cpw-ama.demo div.ama-biddings-biddings a.ama-icon-plus:hover   { background-position: -22px -237px; }
	body.cpw-ama.demo div.ama-auction-lots a.ama-icon
	{
		cursor: pointer;
		background: url(img/ama/icons_sprite.png) no-repeat;
	}
	body.cpw-ama.demo div.ama-auction-lots a.ama-icon-remove       { background-position: -28px -733px; }
	body.cpw-ama.demo div.ama-auction-lots a.ama-icon-remove:hover { background-position: -28px -660px; }
	body.cpw-ama.demo div.ama-auction-lots a.ama-icon-edit         { background-position: -28px -1080px; }
	body.cpw-ama.demo div.ama-auction-lots a.ama-icon-edit:hover   { background-position: -28px -1008px;}
	body.cpw-ama.demo div.ama-auction-lots a.ama-icon-drag         { background-position: -27px -2849px;}
	body.cpw-ama.demo div.ama-auction-lots a.ama-icon-drag:hover   { background-position: -27px -2772px;}
	body.cpw-ama.demo div.ama-auction-lots a.ama-icon-plus         { background-position: -19px -306px; }
	body.cpw-ama.demo div.ama-auction-lots a.ama-icon-plus:hover   { background-position: -22px -237px; }
	body.cpw-ama.demo div.full{height:25px;}
	body.cpw-ama.demo td.cmd.column1
	{
		padding-left:0;
		padding-right:0;
	}
	</style>
</head>
<body style="width:1000px;margin: 0 auto;" class="cpw-ama demo">
	<h1>Тестовая страница для отработки интеграции ПАУ и ЭТП</h1>

	<div class="cpw-ama-biddings">
		Тут должна быть информация о торгах..
	</div>
</body>
</html>