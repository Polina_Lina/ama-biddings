<?

require_once '../assets/config.php';

mb_internal_encoding("utf-8");
mb_http_output( "UTF-8" );
mb_http_input( "UTF-8" );

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

global $trace_methods;
if ($trace_methods)
	require_once '../assets/helpers/log.php';
if ($trace_methods)
	write_to_log('---------to-etp---------------------');

require_once '../assets/etp/list.php';

$test_mode= (isset($_GET['test-mode']) && 'on'==$_GET['test-mode']);

///////////////////////////////////////////////////////////////////////////////
if (!isset($_GET['etp']))
{
	require_once '../assets/actions/to-etp/default/upload.php';
	if ($trace_methods)
		write_to_log('   undefined etp!');
}
else
{
	global $id_etp;
	$id_etp= $_GET['etp'];
	if (!isset($etp_list[$id_etp]))
	{
		require_once '../assets/actions/to-etp/default/upload.php';
		if ($trace_methods)
			write_to_log('   unknown etp '.$id_etp);
	}
	else
	{
		if ($trace_methods)
			write_to_log('   etp '.$id_etp);
		try
		{
			global $etp;
			$etp= $etp_list[$id_etp];
			if ( $test_mode && (!isset($etp->Testing) || false===$etp->Testing) ||
				!$test_mode && (!isset($etp->Release) || false===$etp->Release))
			{
				require_once '../assets/actions/to-etp/default/upload.php';
			}
			else
			{
				require_once '../assets/actions/to-etp/'.$id_etp.'/upload.php';
			}
		}
		catch (Exception $exception)
		{
			require_once '../assets/helpers/log.php';
			write_to_log('Unhandled exception occurred: ' . get_class($exception) . ' - ' . $exception->getMessage());
			throw new Exception("can not upload to etp with id ".$id_etp);
		}
	}
}
