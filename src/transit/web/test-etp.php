<?

require_once '../assets/config.php';

mb_internal_encoding("utf-8");
mb_http_output( "UTF-8" );
mb_http_input( "UTF-8" );

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');

global $trace_methods;
if ($trace_methods)
	require_once '../assets/helpers/log.php';
if ($trace_methods)
	write_to_log('---------test-etp---------------------');

///////////////////////////////////////////////////////////////////////////////
session_start();
if (isset($_POST['user_name']))
{
	$_SESSION['user_name']= $_POST['user_name'];
}
else if (isset($_GET['start']) || isset($_GET['action']) && 'logout'==$_GET['action'])
{
	$_SESSION= array();
}

?>
<html>
	<head>
		<title>Тестовая Электронная Тоговая Площадка для торгов имуществом банкротов</title>
		<meta http-equiv="X-UA-Compatible" content="IE=10" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="language" content="ru" />
		<script type="text/javascript" src="js/vendors/jquery/jquery.js"></script>
		<!-- вот это надо в extension ы! { -->
		<link rel="stylesheet" type="text/css" href="css/vendors/jquery/jquery-ui.css" />
		<script type="text/javascript" src="<?= $base_url = $base_url=" " ? "" : $base_url."/"  ?>js/vendors/jquery/jquery-ui.min.js"></script>
		<!-- вот это надо в extension ы! } -->
	</head>
	<style>
	span.cpw-forms-ama-bidding-token
	{
		display: inline-block;
		min-width: 185px;
	}
	</style>
	<body style="width:1000px;margin: 0 auto;padding:5px;">
		<h1 style="text-align:center">
			<span>Тестовая Электронная Тоговая Площадка</span><br/>
			для торгов имуществом банкротов
		</h1>

		<p style="text-align:right;font-size:small;font-style:italic">
			Для отработки взаимодействия с программой 
			<a href="http://www.russianit.ru/software/bunkrupt/">
				Помощник Арбитражного Управляющего
			</a>
		</p>
<?

if (!isset($_SESSION['user_name']))
{
?>
	<p style="font-weight:bold;">
		Мы НЕ работаем с незнакомыми нам (т.е. неаутентифицированными) пользователями!
	</p>

	<form method="POST" action="http://<?= $_SERVER['HTTP_HOST'] ?><?= $_SERVER['REQUEST_URI'] ?>">
		Для продолжения работы, пожалуйста, назовите своё имя:
		<input type="text" name="user_name">
		<input type="submit">
	</form>
<?
}
else
{
	?>
		<p style="text-align:right;font-size:small;font-style:italic">
			Вы аутентифицированы как <b>"<?= $_SESSION['user_name'] ?>"</b>,
			чтобы выйти из системы, нажмите 
			<a href="?action=logout">здесь</a>
			.
		</p>
	<?
	if (isset($_GET['action']))
		write_to_log('  action='.$_GET['action']);
	if (isset($_GET['action']) && 'upload_bidding'==$_GET['action'])
	{
		?>
			<p>
				<b><?= $_SESSION['user_name'] ?></b>!
			</p>
			<p>
				От вашего имени к нам обратились с запросом на загрузку для вас черновика объявления о торгах<br/>
				 из программы "Помощник Арбитражного Управляющего"
				(токен передаваемого объявления: <b>"<span class="cpw-forms-ama-bidding-token"><?= $_GET['bidding_token'] ?></span>"</b>).
			</p>
			<p>
				<span>Вы действительно хотите загрузить черновик</span> 
				<a href="<?= $base_transit_url ?>?action=view&bidding_token=<?= $_GET['bidding_token'] ?>" target="_blank">
					этого объявления
				</a>?
			</p>
			<p>
				<div style="float:right;">
					<button
						onclick="window.location='<?= $base_testetp_url ?>?action=do_create_bidding_with_token&bidding_token=<?= $_GET['bidding_token'] ?>'"
					>Да, загрузите пожалуйста этот черновик объявления о торгах для меня</button>
					<br/><br/>
					<button
						onclick="window.location='<?= $base_testetp_url ?>?action=do_create_bidding_with_token&step-by-step=on&bidding_token=<?= $_GET['bidding_token'] ?>'"
					>Да, загрузите пожалуйста этот черновик объявления о торгах для меня пошагово</button>
				</div>
			</p>
		<?
	}
	else if (isset($_GET['action']) && 'do_create_bidding_with_token'==$_GET['action'])
	{
		?>
			<p>
				Получена команда на загрузку объявления о торгах с транзитным токеном 
				"<span class="cpw-forms-ama-bidding-token"><?= $_GET['bidding_token'] ?></span>"..
			</p>
			<p>Выделяем регистрационный номер..</p>
		<?
		require_once '../assets/etp/upload.php';
		$bidding= SafeCreateBiddingForToken($_GET['bidding_token']);
		write_to_log(print_r($bidding,true));

		if (!isset($bidding->existed))
		{
			?>
				<p>Для торгов с транзитным токеном "<span class="cpw-forms-ama-bidding-token"><?= $_GET['bidding_token'] ?></span>" выделен регистрационный номер..</p>
			<?
		}
		else
		{
			?>
				<p>Торги с транзитным токеном "<span class="cpw-forms-ama-bidding-token"><?= $_GET['bidding_token'] ?></span>" уже зарегистрированы в системе..</p>
			<?
		}
		require_once '../assets/etp/views/bidding.php';
		if (!isset($bidding->tmCreated) || isset($bidding->tmDownloaded) || null==$bidding->tmDownloaded)
		{
			?>
				<p>Далее, нужно запросить информацию для объявления с транзитного сайта..</p>
				<p>
					<div style="float:right;">
						<button id="auto_press"
							onclick="window.location='<?= $base_testetp_url ?>?action=do_download_bidding&bidding_token=<?= $_GET['bidding_token'] ?>&id_Bidding=<?= $bidding->id_Bidding ?>'">
							Продолжить процесс далее
						</button>
						<br/><br/>
						<button
							onclick="window.location='<?= $base_testetp_url ?>?action=do_download_bidding&step-by-step=on&bidding_token=<?= $_GET['bidding_token'] ?>&id_Bidding=<?= $bidding->id_Bidding ?>'">
							Продолжить процесс далее пошагово
						</button>
						<? if (!isset($_GET['step-by-step']) || 'on'!=$_GET['step-by-step']) : ?>
							<script type="text/javascript"> $(function(){ $('button#auto_press').click(); }); </script>
						<? endif; ?>
					</div>
				</p>
			<?
		}
	}
	else if (isset($_GET['action']) && 'do_download_bidding'==$_GET['action'])
	{
		?>
			<p>
				Запрашиваем информацию для торгов <b>ID<?= $_GET['id_Bidding'] ?></b>
				по транзитному токену "<span class="cpw-forms-ama-bidding-token"><?= $_GET['bidding_token'] ?></span>"
				с транзитного сайта и срхраняем его..
			</p>
		<?
		require_once '../assets/etp/upload.php';
		$bidding= SafeDownloadAndStoreBidding($_GET['bidding_token'],$_GET['id_Bidding']);
		if (isset($bidding->error) && true==$bidding->error)
		{
			?><p>Загрузка и сохранение информации с транзитного сайта завершилась ошибкой..</p><?
		}
		else
		{
			?><p>Получено и сохранено:</p><?
		}
		require_once '../assets/etp/views/bidding.php';
		if (!isset($bidding->tmUrlSet) || null==$bidding->tmUrlSet)
		{
			?>
				<p>Далее, нужно передать информацию о созданной странице на транзитный сайт..</p>
				<p>
					<div style="float:right;">
						<button id="auto_press"
							onclick="window.location='<?= $base_testetp_url ?>?action=do_link_bidding&bidding_token=<?= $_GET['bidding_token'] ?>&id_Bidding=<?= $bidding->id_Bidding ?>'">
							Продолжить процесс далее
						</button>
						<br/><br/>
						<button
							onclick="window.location='<?= $base_testetp_url ?>?action=do_link_bidding&step-by-step=on&bidding_token=<?= $_GET['bidding_token'] ?>&id_Bidding=<?= $bidding->id_Bidding ?>'">
							Продолжить процесс далее пошагово
						</button>
						<? if (!isset($_GET['step-by-step']) || 'on'!=$_GET['step-by-step']) : ?>
							<script type="text/javascript"> $(function(){ $('button#auto_press').click(); }); </script>
						<? endif; ?>
					</div>
				</p>
			<?
		}
	}
	else if (isset($_GET['action']) && 'do_link_bidding'==$_GET['action'])
	{
		require_once '../assets/etp/upload.php';
		?>
			<p>
				Налаживаем связь объявления о торгах <b>ID<?= $_GET['id_Bidding'] ?></b>
				и транзитным токеном "<span class="cpw-forms-ama-bidding-token"><?= $_GET['bidding_token'] ?></span>"
				с транзитным сайтом..
			</p>
		<?
		$biddings= LoadBidding($_GET['id_Bidding']);
		$bidding= $biddings[0];
		if (!SafeSetUrlForTransitBidding($bidding->transit_token,$bidding->id_Bidding))
		{
			?>
				<p>
					Попытка наладить связь с транзитным сайтом закончилась неудачей..
				</p>
				<p>
					Возможно вам необходимо будет самостоятельно указать в свойствах торгов ссылку на страницу..
				</p>
			<?
		}
		else
		{
			?>
				<p>Успешно наладили связь, переходим непосредственно к торгам..</p>
				<p>
					<button id="auto_press"
						onclick="window.location='<?= $base_testetp_url ?>?action=bidding&id_Bidding=<?= $bidding->id_Bidding ?>'">
						Переход на страницу торгов
					</button>
					<? if (!isset($_GET['step-by-step']) || 'on'!=$_GET['step-by-step']) : ?>
						<script type="text/javascript"> $(function(){ $('button#auto_press').click(); }); </script>
					<? endif; ?>
				</p>
			<?
		}
		require_once '../assets/etp/views/bidding.php';
	}
	else if (isset($_GET['action']) && 'bidding'==$_GET['action'])
	{
		require_once '../assets/etp/upload.php';
		$biddings= LoadBidding($_GET['id_Bidding']);
		$bidding= $biddings[0];
		?><span>Объявление.</span><br/><?
		require_once '../assets/etp/views/bidding.php';
	}
	else
	{
		?>
			<p>
				В этом месте по идее должны быть:
				<ul>
					<li>Ваш профиль</li>
					<li>Список всех объявленных торгов</li>
					<li>Список организуемых вами торгов</li>
					<li>Список торгов, на участие в которых вы сделали заявку</li>
				</ul>
			</p>
		<?
	}
}
?>
	</body>
</html>
<?