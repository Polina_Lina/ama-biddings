<?
require_once '../assets/config.php';

mb_internal_encoding("utf-8");
mb_http_output( "UTF-8" );
mb_http_input( "UTF-8" );

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

global $trace_methods;
if ($trace_methods)
	require_once '../assets/helpers/log.php';

$possible_file_actions= array
(
	// открыто всегда:
	 'ping'=>            'transit/ping'             // Вернуть информацию о торгах для сохранения в виде файла
	,'upload'=>          'transit/upload'           // Загрузить информацию о торгах для передачи на ЭТП на сервер и получить токен

	// открыто 30 минут после загрузки, потом только админу:
	,'view'=>            'transit/view'             // посмотреть информацию о торгах связанных с токеном
	,'download'=>        'transit/download'         // Выгрузить информацию о торгах по токену
	,'set-url'=>         'transit/seturl'           // Ассоциировать токен торгов с URL на ЭТП

	// открыто всегда:
	,'redirect'=>        'transit/view'             // Перейти на страницу торгов на ЭТП

	,'login' =>          'transit/admin/login'      // авторизоваться в административной части системы
	,'logout' =>         'transit/admin/logout'     // выйти из административной части системы

	// только админу:
	,'biddings-g' =>     'transit/admin/biddings-g' // получение данных из таблицы transitbidding для jqGrid
	,'biddings'=>        'transit/admin/biddings'   // админка для просмотра передач на ЭТП
	,'admin'=>           'transit/admin/biddings'   // админка для просмотра передач на ЭТП
);

///////////////////////////////////////////////////////////////////////////////
if (!isset($_GET['action']))
{
?>
<html>
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=10" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="language" content="ru" />
		<title>Работа с передачей информации о торгах на ЭТП</title>
	</head>
	<body style="width:800px;margin:60px auto;">
		<h1>Работа с передачей информации о торгах на ЭТП</h1>
		<p style="color:red;text-align:center;">
			Параметр действия неопределён
		<p>
	</body>
</html>
<?
	if ($trace_methods)
		write_to_log('   undefined action!');
}
else
{
	$action= $_GET['action'];

	global $trace_methods;

	if ($trace_methods)
		write_to_log('------'.$action.'--------------------------');

	if (!isset($possible_file_actions[$action]))
	{
		if ($trace_methods)
			write_to_log('   unknown action!');
	}
	else
	{
		$subpath= $possible_file_actions[$action];
		if (''==$subpath)
			$subpath= $action;
		try
		{
			require_once '../assets/actions/'.$subpath.'.php';
		}
		catch (Exception $exception)
		{
			require_once '../assets/helpers/log.php';
			write_to_log('Unhandled exception occurred: ' . get_class($exception) . ' - ' . $exception->getMessage());
			throw new Exception("can not execute action ".$action);
		}
	}
}
