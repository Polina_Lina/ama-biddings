call :build-ama-bidding-transit
call :build-ama-biddings-demo
call :build-ama-biddings
call :build-ama-prices
exit

rem -----------------------------------------------------------
:build-ama-bidding-transit
del              ..\transit\web\js\ama-bidding-transit.js
del                          built\ama-bidding-transit.js
node optimizers\r.js -o conf\build-ama-bidding-transit.js
copy                         built\ama-bidding-transit.js ..\transit\web\js\
exit /B

rem -----------------------------------------------------------
:build-ama-biddings-demo
del              ..\transit\web\js\ama-biddings-demo.js
del                          built\ama-biddings-demo.js
node optimizers\r.js -o conf\build-ama-biddings-demo.js
copy                         built\ama-biddings-demo.js ..\transit\web\js\
exit /B

rem -----------------------------------------------------------
:build-ama-biddings
del                      ..\ama\js\ama-biddings.js
del                          built\ama-biddings.js
node optimizers\r.js -o conf\build-ama-biddings.js
copy                         built\ama-biddings.js ..\ama\js\
exit /B

rem -----------------------------------------------------------
:build-ama-prices
del                   ..\prices\js\ama-prices.js
del                          built\ama-prices.js
node optimizers\r.js -o conf\build-ama-prices.js
copy                         built\ama-prices.js ..\prices\js\
exit /B
