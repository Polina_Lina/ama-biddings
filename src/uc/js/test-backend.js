var ajax_GetBiddings= function(options)
{
	var res =
	{
		"page": "1",
		"total": 1,
		"records": 1,
		"rows":
			[
				{
					  "id_TransitBidding": 1
					, "token_bidding": "595a6486a98810.71013287"
					, "IP": "255.255.255.255"
					, "date_access": "2017-07-03 18:36:38"
					, "etp": "\u042d\u0422\u041f \"\u0422\u0435\u0441\u0442\""
					, "URL": "http:\/\/utptest.sberbank-ast.ru\/Bankruptcy\/NBT\/PurchaseCreate\/11\/3\/10668855\/"
				}
			]
	}
	return res;
}

var ajax_function = function (options)
{
	if (0 == options.url.indexOf('/ama-biddings-transit/transit.php?action=biddings-g'))
	{
		return ajax_GetBiddings(options);
	}
	return null;
}

function global_external_AMA_SaveBiddings() { }

$(function ()
{
	$.ajaxTransport
	(
		'+*',
		function (options, originalOptions, jqXHR)
		{
			var external_ajax_transport =
			{
				send: function (headers, completeCallback)
				{
					var res = ajax_function(options);
					completeCallback(200, 'success', { text: JSON.stringify(res) });
				}
				, abort: function ()
				{
				}
			}
			return external_ajax_transport;
		}
	);
});