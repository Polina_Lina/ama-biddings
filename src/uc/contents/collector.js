define(function ()
{
	return function (dependencies, form_specs)
	{
		var res = {}
		for (var i = 0; i < dependencies.length; i++)
		{
			var module_name = dependencies[i];
			res[module_name] = form_specs[i];
		}
		return res;
	}
});
