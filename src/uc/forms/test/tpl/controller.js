define([
	  'forms/base/controller'
	, 'tpl!forms/test/tpl/view.html'
],
function (BaseFormController, index_tpl)
{
	return function ()
	{
		var edit_id_spec = '#test-test-form-edit';
		var res = BaseFormController();

		var RenderForm = function (id_form_div, value)
		{
			var form_div = $('#' + id_form_div);
			var content = index_tpl(value);
			form_div.append(content);
			$(edit_id_spec).focus();
		}

		res.CreateNew = function (id_form_div) { RenderForm(id_form_div, ""); };

		res.Edit = function (id_form_div) { RenderForm(id_form_div, this.form_content); };

		res.GetFormContent = function () { return $(edit_id_spec).val(); };

		res.SetFormContent = function (form_content)
		{
			var isnum = /^\d+$/.test(form_content);
			if (!isnum)
			{
				return "Документ должен содержать только цифры!";
			}
			else
			{
				this.form_content = form_content;
				return null;
			}
		};

		res.Validate = function ()
		{
			var form_content = $(edit_id_spec).val();
			var isnum = /^\d+$/.test(form_content);
			return isnum ? null : "Документ должен содержать только цифры!";
		};

		return res;
	}
});
