define(['forms/ama/biddings/view/c_bidding_view'],
function (CreateController)
{
	var form_spec =
	{
		  CreateController: CreateController
		, key: 'bidding_view'
		, Title: 'Объявление о торгах'
		, FileFormat: 
			{
				  FileExtension: 'cfb'
				, Description: 'CFB. Объявление о торгах'
			}
	};
	return form_spec;
});
