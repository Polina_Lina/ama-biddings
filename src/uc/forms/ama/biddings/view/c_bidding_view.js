define([
	  'forms/base/c_binded'
	, 'tpl!forms/ama/biddings/view/v_bidding_view.html'
	, 'forms/ama/biddings/main/x_bidding_sber_PurchaseCreate'
	, 'forms/ama/biddings/base/cfb/v1/codec.cfb'
],
function (c_binded, tpl, x_bidding_sber_PurchaseCreate, codec_cfb)
{
	return function()
	{
		var controller = c_binded(tpl);

		var codec = {
			Decode: function(data)
			{
				if ('string' != typeof data)
				{
					if (null==data || !data.purchaseinfo)
					{
						return data;
					}
					else
					{
						return x_bidding_sber_PurchaseCreate().Decode_data(data);
					}
				}
				else if (-1 != data.indexOf('<purchaseinfo'))
				{
					var res = x_bidding_sber_PurchaseCreate().Decode(data);
					return res;
				}
				else if (-1 != data.indexOf('<Объявление_о_торгах'))
				{
					var res = codec_cfb().Decode(data);
					return res;
				}
				else
				{
					return data;
				}
			}
		};

		controller.UseCodec(codec);

		return controller;
	}
});
