define([
	  'forms/base/c_binded'
	, 'tpl!forms/ama/biddings/lots/e_lots.html'
	, 'forms/base/b_collection'
	, 'forms/base/h_msgbox'
	, 'forms/ama/biddings/lot/c_lot'
	, 'forms/base/h_validation_msg'
],
function (c_binded, tpl, b_collection, h_msgbox, c_lot, h_validation_msg)
{
	return function()
	{
		var BuildClassesForItem= function(lots,i)
		{
			var lot = lots[i];
			var res = '';
			if (!lot.Начальная_цена || '' == lot.Начальная_цена || null == lot.Начальная_цена)
				res += ' empty-price-start';
			if (!lot.Размер_задатка || '' == lot.Размер_задатка || null == lot.Размер_задатка)
				res += ' empty-price-0';
			if (!lot.Шаг_аукциона || '' == lot.Шаг_аукциона || null == lot.Шаг_аукциона)
				res += ' empty-price-step';
			if (1 == lots.length)
				res += ' single-lot';
			if ( 1 < lots.length )
				res += ' not-single-lot';
			return res;
		}

		var controller = c_binded(tpl, {CreateBinding: b_collection, BuildClassesForItem: BuildClassesForItem});

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			var self = this;
			$( sel + ' a.ama-icon.ama-icon-plus' ).click( function( e ) { e.preventDefault(); self.OnAdd(); } );

			var sOnEdit = function (e) { e.preventDefault(); self.OnEdit(e); };
			var sOnDelete = function (e) { e.preventDefault(); self.OnDelete(e); };
			var ReClick = function (item, func)
			{
				item.off('click', func);
				item.click(func);
			}
			this.binding.on_after_update_dom = function ()
			{
				ReClick($(sel + ' td.position-number'), sOnEdit);
				ReClick($(sel + ' td.content'), sOnEdit);
				ReClick($(sel + ' a.ama-icon.ama-icon-edit'), sOnEdit);
				ReClick($(sel + ' a.ama-icon.ama-icon-remove'), sOnDelete);
			};
			this.binding.on_after_update_dom();
		}

		controller.OnAdd= function()
		{
			var obj = this.binding;
			var editor = c_lot();

			var SaveButtonName = 'Сохранить параметры лота';

			editor.SetFormContent({});

			var self = this;
			h_msgbox.ShowModal
				({
					controller: editor
					, title: 'Добавление выставляемого на торги лота'
					, width: editor.size.width
					, height: editor.size.height
					, resizable: false
					, id_div: 'cpw-ama-bidding-modal-lot'
					, buttons: [SaveButtonName, 'Отмена']
					, onclose: function (bname, dlg)
					{
						if (SaveButtonName == bname)
						{
							h_validation_msg.IfOkWithValidateResult(editor.Validate(), function ()
							{
								var lot = editor.GetFormContent();
								if (!obj.model || null == obj.model)
								{
									obj.model = [lot];
								}
								else
								{
									obj.model.push(lot);
								}
								if (self.IncrementVersion)
									self.IncrementVersion();
								obj.update_dom();
								$(dlg).dialog('close');
							});
							return false;
						}
					}
				});
		}

		controller.OnEdit = function (e)
		{
			var obj = this.binding;
			var i_item = obj.find_binding_for_item($(e.target)).i_item;
			var lot = obj.model[i_item];

			var editor = c_lot();

			var SaveButtonName = 'Сохранить параметры лота';

			editor.SetFormContent(lot);

			var self = this;
			h_msgbox.ShowModal
				({
					controller: editor
					, title: 'Редактирование лота № ' + (parseInt(i_item) + 1) + ' "' + lot.Наименование + '"'
					, width: editor.size.width
					, height: editor.size.height
					, resizable: false
					, id_div: 'cpw-ama-bidding-modal-lot'
					, buttons: [SaveButtonName, 'Отмена']
					, onclose: function (bname, dlg)
					{
						if (SaveButtonName == bname)
						{
							h_validation_msg.IfOkWithValidateResult(editor.Validate(), function ()
							{
								editor.GetFormContent();
								if (self.IncrementVersion)
									self.IncrementVersion();
								obj.update_dom();
								$(dlg).dialog("close");
							});
							return false;
						}
					}
				});
		}

		controller.OnDelete = function (e)
		{
			var obj = this.binding;
			var i_item = obj.find_binding_for_item($(e.target)).i_item;
			var lot = obj.model[i_item];
			var delete_item = parseInt(i_item) + 1;
			var self = this;
			var OkButtonName = 'Да, удалить лот';
			h_msgbox.ShowModal
				({
					html: 'Вы действительно хотите удалить лот № ' + delete_item + ' "' + lot.Наименование + '"?'
					, title: 'Подтверждение удаления лота'
					, width: 500, height: 200
					, buttons: [OkButtonName, 'Отмена']
					, onclose: function (bname, dlg)
					{
						if (OkButtonName == bname)
						{
							obj.model.splice(i_item, 1);
							if (self.IncrementVersion)
								self.IncrementVersion();
							obj.update_dom();
						}
					}
				});
		}

		return controller;
	}
});
