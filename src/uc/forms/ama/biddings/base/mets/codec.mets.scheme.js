define(function ()
{
	return {
		tagName: 'Информация_о_торгах_имуществом_в_деле_о_банкротстве_для_объявления_на_ЭТП_mets'
		, fields: 
		{
			Лоты: {
				type: 'array', item: {
					tagName: 'Лот'
					, fields: {
						Приложения: { type: 'array', item: { tagName: 'Приложение' } }
						, КлассификацияЕФРСБ: { type: 'array', item: { tagName: 'Класс' } }
						, Интервалы_снижения_цены: { type: 'array', item: { tagName: 'Интервал_цены' } }
					}
				}
			}
			, Приложения: { type: 'array', item: { tagName: 'Приложение' } }
		}
	};
});
