define([
	  'forms/base/codec/codec.xml'
	, 'forms/ama/biddings/base/mets/codec.mets.scheme'
],
function (BaseCodec, scheme)
{
	return function ()
	{
		var codec = BaseCodec();
		codec.schema = scheme;
		codec.GetRootNamespaceURI = function () { return "http://probili.ru/bidding/transit"; }
		return codec;
	}
});
