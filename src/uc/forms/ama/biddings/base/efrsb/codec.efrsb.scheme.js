define(function ()
{
	var MessageInfo_Auction_scheme= { fields: {
					LotTable: { type: 'array', item: {
						tagName: 'AuctionLot'
						, fields: {
							ClassifierCollection: { type: 'array', item: {
								tagName: 'AuctionLotClassifier'
							} }
						}
					} }
				} };
	return {
		tagName: 'MessageData'
		, fields: {
			MessageInfo: { fields: {
				Auction: MessageInfo_Auction_scheme
				,ChangeAuction: MessageInfo_Auction_scheme
			} }
			, MessageURLList: { type: 'array', item: {
				tagName: 'MessageURL'
			} }
			, FileInfoList: { type: 'array', item: {
				tagName: 'FileInfo'
			} }
		}
	};
});
