define([
	  'forms/base/codec/codec.xml'
	, 'forms/ama/biddings/base/efrsb/codec.efrsb.scheme'
],
function (BaseCodec, scheme)
{
	return function ()
	{
		var codec = BaseCodec();
		codec.schema = scheme;

		var base_Decode= codec.Decode;
		codec.Decode= function(d)
		{
			var res= base_Decode.call(this,d);
			if (res.MessageURLList)
				delete res.MessageURLList;
			if (res.CaseNumber)
				res.CaseNumber= res.CaseNumber.replace(/^\s+|\s+$/g, '');
			if (res.PublishDate)
			{
				var pos= res.PublishDate.indexOf('T');
				if (-1!=pos)
					res.PublishDate= res.PublishDate.substring(0,pos);
			}
			return res;
		}

		return codec;
	}
});
