﻿define([
	'forms/base/h_constraints'
],
function (h_constraints)
{
	return function ()
	{
		var constraints =
		[
			  h_constraints.for_Field('Наименование', h_constraints.NotEmpty())
		];
		return constraints;
	}
});
