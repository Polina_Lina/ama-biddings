define([
	  'forms/base/c_binded'
	, 'tpl!forms/ama/biddings/lot/e_lot.html'
	, 'forms/ama/biddings/lot/r_lot'
	, 'forms/ama/biddings/kmobjects/c_kmobjects'
	, 'forms/base/h_msgbox'
	, 'forms/ama/biddings/kmobjects/h_kmobjects'
	, 'forms/base/codec/codec.copy'
],
function (c_binded, tpl, r_lot, c_kmobjects, h_msgbox, h_kmobjects, codec_copy)
{
	return function()
	{
		var controller = c_binded(tpl, {
			constraints: r_lot()
			, field_spec:
			{
				Объект_конкурсной_массы: function (lot)
				{
					return {
						text: h_kmobjects.BuildTextForLink
						,controller: function(item, onAfterSave)
						{
							return {
								ShowModal: function (e)
								{
									var editor = c_kmobjects();
									var ccopy = codec_copy();
									editor.SetFormContent(!lot || !lot.Объект_конкурсной_массы ? [] :  ccopy.Copy(lot.Объект_конкурсной_массы));
									var self = this;
									var SaveButtonName = 'Сохранить объекты конкурсной массы для лота';
									h_msgbox.ShowModal
										({
											controller: editor
											, title: 'Объекты конкурсной массы выставляемые на торги единым лотом'
											, width: 680
											, height: 420
											, resizable: false
											, id_div: 'cpw-ama-bidding-modal-kmobjects'
											, buttons: [SaveButtonName, 'Отмена']
											, onclose: function( bname, dlg )
											{
												$( '#select2-drop' ).css( "display", "none" );
												if (SaveButtonName == bname)
												{
													var kmobjects = editor.GetFormContent();
													var old_km_objects_lot = h_kmobjects.BuildLot(lot.Объект_конкурсной_массы);
													var new_km_objects_lot = h_kmobjects.BuildLot(kmobjects);
													lot.Объект_конкурсной_массы = kmobjects;

													var set_field_value= function(field_name, field_value)
													{
														$('*[model_field_name="' + field_name + '"]').val(field_value);
														$('*[model_field_name="' + field_name + '"]').change();
													}

													for (var name in new_km_objects_lot)
													{
														var cur_value = $('*[model_field_name="' + name + '"]').val();
														if ((!old_km_objects_lot || null == old_km_objects_lot) && '' == cur_value)
														{
															set_field_value(name, new_km_objects_lot[name]);
														}
														else
														{
															if (old_km_objects_lot && cur_value == old_km_objects_lot[name])
															{
																set_field_value(name, new_km_objects_lot[name]);
															}
														}
													}
													onAfterSave();
												}
											}
										});
								}
							};
						}
					};
				}
			}
		});

		controller.size = { width: 710, height: 520 };

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			$(sel + ' input[model_field_name="Наименование"]').focus();

			var self = this;
			$(sel + ' a.related-objects').click(function (e) { e.preventDefault(); self.OnRelations() });

			if (!this.model || null == this.model)
				this.model = {}
			if (!this.model.Наименование || null == this.model.Наименование || '' == this.model.Наименование)
				$(sel + ' a[model_field_name="Объект_конкурсной_массы"]').click();
		}

		return controller;
	}
});
