define([
	  'tpl!forms/ama/biddings/zob110/01body/v_zob110_body.html'
	, 'tpl!forms/ama/biddings/zob110/01body/v_zob110_body_help.html'
	, 'forms/ama/biddings/zob110/01body/c_zob110_body'
	, 'forms/base/h_msgbox'
],
function (v, v_help, c, h_msgbox)
{
	var nav= 'состав, порядок ознакомления';
	var h_spec = {
		tag:'01body'
		,nav:nav
		,title:'Сведения о лотах, их составе, характеристиках, описание, порядок ознакомления'
		, html: v
		, help: v_help
		, edit: function(cfb, on_change)
		{
			var controller= c();
			controller.SetFormContent({ Общий_порядок_ознакомления_с_имуществом: cfb.Общий_порядок_ознакомления_с_имуществом});
			var btnOk= 'Сохранить раздел "' + nav + '"';
			h_msgbox.ShowModal({
				title:'Редактирование раздела сообщения о продаже "' + nav + '"'
				,width:700
				,controller:controller
				,buttons:[btnOk,'Отмена']
				, onclose: function (btn)
				{
					if (btnOk==btn)
					{
						var res= controller.GetFormContent();
						cfb.Общий_порядок_ознакомления_с_имуществом= res.Общий_порядок_ознакомления_с_имуществом;
						on_change(cfb);
					}
				}
			})
		}
	};
	return h_spec;
});
