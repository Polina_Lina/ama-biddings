define([
	  'forms/base/c_binded'
	, 'tpl!forms/ama/biddings/zob110/01body/e_zob110_body.html'
],
function (c_binded, tpl)
{
	return function()
	{
		var controller= c_binded(tpl);

		return controller;
	}
});
