define([
	  'forms/base/c_binded'
	, 'tpl!forms/ama/biddings/zob110/06schp/e_zob110_schp.html'
],
function (c_binded, tpl)
{
	return function()
	{
		var controller= c_binded(tpl);

		return controller;
	}
});
