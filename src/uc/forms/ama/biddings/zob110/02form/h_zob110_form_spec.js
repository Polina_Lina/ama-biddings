define([
	  'tpl!forms/ama/biddings/zob110/02form/v_zob110_form.html'
	, 'tpl!forms/ama/biddings/zob110/02form/v_zob110_form_help.html'
	, 'forms/ama/biddings/zob110/02form/c_zob110_form'
	, 'forms/base/h_msgbox'
],
function (v, v_help, c, h_msgbox)
{
	var nav= 'форма торгов и предложений';
	var h_spec = {
		tag:'02form'
		, nav:nav
		, title:'Сведения о форме проведения торгов и форме представления предложений о цене'
		, html:v
		, help: v_help
		, edit: function(cfb, on_change)
		{
			var controller= c();
			controller.SetFormContent({
				Форма_проведения_торгов: cfb.Форма_проведения_торгов
				,Форма_представления_предложений_цены: cfb.Форма_представления_предложений_цены
			});
			var btnOk= 'Сохранить раздел "' + nav + '"';
			h_msgbox.ShowModal({
				title:'Редактирование раздела сообщения о продаже "' + nav + '"'
				,width:700
				,controller:controller
				,buttons:[btnOk,'Отмена']
				, onclose: function (btn)
				{
					if (btnOk==btn)
					{
						var res= controller.GetFormContent();
						cfb.Форма_проведения_торгов= res.Форма_проведения_торгов;
						cfb.Форма_представления_предложений_цены= res.Форма_представления_предложений_цены;
						on_change(cfb);
					}
				}
			})
		}
	};
	return h_spec;
});
