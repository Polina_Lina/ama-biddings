define([
	  'forms/base/c_binded'
	, 'tpl!forms/ama/biddings/zob110/02form/e_zob110_form.html'
],
function (c_binded, tpl)
{
	return function()
	{
		var controller= c_binded(tpl);

		return controller;
	}
});
