define([
	  'forms/base/c_binded'
	, 'tpl!forms/ama/biddings/zob110/03reqc/e_zob110_reqc.html'
],
function (c_binded, tpl)
{
	return function()
	{
		var controller= c_binded(tpl);

		return controller;
	}
});
