define([
	  'tpl!forms/ama/biddings/zob110/03reqc/v_zob110_reqc.html'
	, 'tpl!forms/ama/biddings/zob110/03reqc/v_zob110_reqc_help.html'
	, 'forms/ama/biddings/zob110/03reqc/c_zob110_reqc'
	, 'forms/base/h_msgbox'
],
function (v, v_help, c, h_msgbox)
{
	var nav= 'требования к участникам';
	var h_spec = {
		tag:'03reqc'
		,nav:nav
		,title:'Требования к участникам закрытых торгов'
		, html: v
		, help: v_help
		, conditional: true
		, edit: function(cfb, on_change)
		{
			var controller= c();
			controller.SetFormContent({ 
				Требования_к_участникам_закрытых_торгов: !cfb ? '' : cfb.Требования_к_участникам_закрытых_торгов
			});
			var btnOk= 'Сохранить раздел "' + nav + '"';
			h_msgbox.ShowModal({
				title:'Редактирование раздела сообщения о продаже "' + nav + '"'
				,width:700
				,controller:controller
				,buttons:[btnOk,'Отмена']
				, onclose: function (btn)
				{
					if (btnOk==btn)
					{
						var res= controller.GetFormContent();
						cfb.Требования_к_участникам_закрытых_торгов= res.Требования_к_участникам_закрытых_торгов;
						on_change(cfb);
					}
				}
			})
		}
	};
	return h_spec;
});
