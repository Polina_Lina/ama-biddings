define([
	  'tpl!forms/ama/biddings/zob110/05scha/v_zob110_scha.html'
	, 'tpl!forms/ama/biddings/zob110/05scha/v_zob110_scha_help.html'
	, 'forms/ama/biddings/zob110/05scha/c_zob110_scha'
	, 'forms/base/h_msgbox'
],
function (v, v_help, c, h_msgbox)
{
	var nav= 'представления заявок';
	var h_spec = {
		tag:'05scha'
		,nav:nav
		,title:'Порядок, место, срок и время представления заявок на участие в торгах'
		, html: v
		, help: v_help
		, edit: function(cfb, on_change)
		{
			var controller= c();
			var r= (!cfb 
				|| !cfb.Регламенты 
				|| !cfb.Регламенты.Регламент_проведения_торгов 
				|| !cfb.Регламенты.Регламент_проведения_торгов.Представление_заявок_об_участии)
				? {}
				: cfb.Регламенты.Регламент_проведения_торгов.Представление_заявок_об_участии;
			controller.SetFormContent({ 
				Порядок: r.Порядок, Место: r.Место, Срок_и_время: r.Срок_и_время
			});
			var btnOk= 'Сохранить раздел "' + nav + '"';
			h_msgbox.ShowModal({
				title:'Редактирование раздела сообщения о продаже "' + nav + '"'
				,width:700
				,controller:controller
				,buttons:[btnOk,'Отмена']
				, onclose: function (btn)
				{
					if (btnOk==btn)
					{
						var res= controller.GetFormContent();
						var Регламенты= cfb.Регламенты;
						if (!Регламенты)
							Регламенты= cfb.Регламенты = {};
						var Регламент_проведения_торгов= Регламенты.Регламент_проведения_торгов;
						if (!Регламент_проведения_торгов)
							Регламент_проведения_торгов = Регламенты.Регламент_проведения_торгов = {};
						Регламент_проведения_торгов.Представление_заявок_об_участии = {
							Порядок: res.Порядок
							,Место: res.Место
							,Срок_и_время:{
								начало: res.Срок_и_время.начало
								,окончание: res.Срок_и_время.окончание
								,текстом: res.Срок_и_время.текстом
							}
						}
						on_change(cfb);
					}
				}
			})
		}
	};
	return h_spec;
});
