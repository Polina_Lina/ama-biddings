define([
	  'forms/base/c_binded'
	, 'tpl!forms/ama/biddings/zob110/05scha/e_zob110_scha.html'
],
function (c_binded, tpl)
{
	return function()
	{
		var controller= c_binded(tpl);

		return controller;
	}
});
