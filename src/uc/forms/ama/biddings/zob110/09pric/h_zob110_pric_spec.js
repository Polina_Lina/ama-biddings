define([
	  'tpl!forms/ama/biddings/zob110/09pric/v_zob110_pric.html'
	, 'tpl!forms/ama/biddings/zob110/09pric/v_zob110_pric_help.html'
	, 'forms/ama/biddings/zob110/09pric/c_zob110_pric'
	, 'forms/base/h_msgbox'
],
function (v, v_help, c, h_msgbox)
{
	var nav= 'начальная цена';
	var h_spec = {
		tag:'09pric'
		,nav:nav
		,title:'Начальная цена продажи'
		, html: v
		, help: v_help
		,readonly:true
		, edit: function(cfb, on_change)
		{
			var controller= c();
			controller.SetFormContent({});
			h_msgbox.ShowModal({
				title:'Редактирование раздела сообщения о продаже "' + nav + '"'
				,width:700
				,controller:controller
				,buttons:['Закрыть']
			})
		}
	};
	return h_spec;
});
