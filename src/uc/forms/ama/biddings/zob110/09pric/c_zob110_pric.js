define([
	  'forms/base/c_binded'
	, 'tpl!forms/ama/biddings/zob110/09pric/e_zob110_pric.html'
],
function (c_binded, tpl)
{
	return function()
	{
		var controller= c_binded(tpl);

		return controller;
	}
});
