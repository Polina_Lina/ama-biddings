define([
	  'tpl!forms/ama/biddings/zob110/12rres/v_zob110_rres.html'
	, 'tpl!forms/ama/biddings/zob110/12rres/v_zob110_rres_help.html'
	, 'forms/ama/biddings/zob110/12rres/c_zob110_rres'
	, 'forms/base/h_msgbox'
],
function (v, v_help, c, h_msgbox)
{
	var nav= 'подведения результатов';
	var h_spec = {
		tag:'12rres'
		,nav:nav
		,title:'Дата, время и место подведения результатов торгов'
		, html: v
		, help: v_help
		, edit: function(cfb, on_change)
		{
			var controller= c();
			var r= (!cfb 
				|| !cfb.Регламенты 
				|| !cfb.Регламенты.Регламент_завершения_торгов 
				|| !cfb.Регламенты.Регламент_завершения_торгов.Выявление_победителей)
				? {}
				: cfb.Регламенты.Регламент_завершения_торгов.Выявление_победителей;
			controller.SetFormContent({ 
				Порядок: r.Порядок, Критерии: r.Критерии
			});
			var btnOk= 'Сохранить раздел "' + nav + '"';
			h_msgbox.ShowModal({
				title:'Редактирование раздела сообщения о продаже "' + nav + '"'
				,width:700
				,controller:controller
				,buttons:[btnOk,'Отмена']
				, onclose: function (btn)
				{
					if (btnOk==btn)
					{
						var res= controller.GetFormContent();
						var Регламенты= cfb.Регламенты;
						if (!Регламенты)
							Регламенты= cfb.Регламенты = {};
						var Регламент_завершения_торгов= Регламенты.Регламент_завершения_торгов;
						if (!Регламент_завершения_торгов)
							Регламент_завершения_торгов = Регламенты.Регламент_завершения_торгов= {};
						console.log(res);
						Регламент_завершения_торгов.Выявление_победителей = {
							Порядок: res.Порядок, Критерии: res.Критерии
						}
						on_change(cfb);
					}
				}
			})
		}
	};
	return h_spec;
});
