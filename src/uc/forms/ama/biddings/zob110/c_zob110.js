define([
	  'forms/base/c_binded'
	, 'tpl!forms/ama/biddings/zob110/e_zob110.html'
	, 'forms/base/h_msgbox'
	, 'tpl!forms/ama/biddings/zob110/v_zob110_help.html'

	, 'forms/ama/biddings/zob110/01body/h_zob110_body_spec'
	, 'forms/ama/biddings/zob110/02form/h_zob110_form_spec'
	, 'forms/ama/biddings/zob110/03reqc/h_zob110_reqc_spec'
	, 'forms/ama/biddings/zob110/04conc/h_zob110_conc_spec'
	, 'forms/ama/biddings/zob110/05scha/h_zob110_scha_spec'
	, 'forms/ama/biddings/zob110/06schp/h_zob110_schp_spec'
	, 'forms/ama/biddings/zob110/07docs/h_zob110_docs_spec'

	, 'forms/ama/biddings/zob110/09pric/h_zob110_pric_spec'
	, 'forms/ama/biddings/zob110/10step/h_zob110_step_spec'
	, 'forms/ama/biddings/zob110/11rwin/h_zob110_rwin_spec'
	, 'forms/ama/biddings/zob110/12rres/h_zob110_rres_spec'
	, 'forms/ama/biddings/zob110/13cntr/h_zob110_cntr_spec'

	, 'forms/ama/biddings/zob110/15orgr/h_zob110_orgr_spec'
],
function (c_binded, tpl, h_msgbox, v_zob110_help
	, h_zob110_body_spec, h_zob110_form_spec, h_zob110_reqc_spec, h_zob110_conc_spec, h_zob110_scha_spec, h_zob110_schp_spec, h_zob110_docs_spec

	, h_zob110_pric_spec, h_zob110_step_spec, h_zob110_rwin_spec, h_zob110_rres_spec, h_zob110_cntr_spec

	, h_zob110_orgr_spec)
{
	return function()
	{
		var sections= [
			h_zob110_body_spec
			,h_zob110_form_spec
			,h_zob110_reqc_spec
			,h_zob110_conc_spec
			,h_zob110_scha_spec
			,h_zob110_schp_spec
			,h_zob110_docs_spec

			,{
				tag:'08advc'
				,nav:'задатки'
				,title:'Размер задатка, сроки и порядок внесения задатка, реквизиты счетов, на которые вносится задаток'
			}

			,h_zob110_pric_spec
			,h_zob110_step_spec
			,h_zob110_rwin_spec
			,h_zob110_rres_spec
			,h_zob110_cntr_spec

			,{
				tag:'14pays'
				,nav:'платежи'
				,title:'Сроки платежей, реквизиты счетов, на которые вносятся платежи'
			}
			,h_zob110_orgr_spec
		];

		var sections_by_tag= null;
		var get_section_by_tag= function(tag)
		{
			if (null==sections_by_tag)
			{
				var h= {};
				for (var i= 0; i< sections.length; i++)
				{
					var s= sections[i];
					h[s.tag]= s;
				}
				sections_by_tag= h;
			}
			return sections_by_tag[tag];
		}

		var options = { sections: sections };

		var controller= c_binded(tpl, options);

		var base_Render= controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this,sel);

			var self= this;
			$(sel + ' div.nav li').mouseover(function (e) { self.OnNavLiMouseOver(e);});
			$(sel + ' div.nav li').mouseleave(function (e) { self.OnNavLiMouseLeave(e);});

			$(sel + ' div.btn.help').click(function (e) { self.OnSectionHelp(e);});
			$(sel + ' div.par').click(function (e) { self.OnSection(e);});
		}

		controller.OnNavLiMouseOver= function(e)
		{
			var $t= $(e.target);
			var href= $t.attr('href')
			if (!href)
				href= $t.find('a').attr('href');
			href= href.substring(1);

			var sel= this.binding.form_div_selector;

			$(sel + ' div.par').css('border-color','white');
			$(sel + ' div.par[data-link="' + href + '"]').css('border-color','blue');
		}

		controller.OnNavLiMouseLeave= function(e)
		{
			var $t= $(e.target);
			var href= $t.attr('href')
			if (!href)
				href= $t.find('a').attr('href');
			href= href.substring(1);

			var sel= this.binding.form_div_selector;

			$(sel + ' div.par').css('border-color','white');
		}

		controller.OnSectionHelp= function(e)
		{
			var $t= $(e.target);
			var $par= $t.parents('div.par');
			var tag= $par.attr('data-link');
			var section= get_section_by_tag(tag);
			if (section)
			{
				h_msgbox.ShowModal({ 
					title: 'Разъяснение по заполнению информации о продаже'
					, width: 800, html: v_zob110_help(section)
				});
			}
		}

		controller.OnSection= function(e)
		{
			var $t= $(e.target);
			if (!$t.hasClass('help'))
			{
				var $par= $t.parents('div.par');
				var tag= $par.attr('data-link');
				var section= get_section_by_tag(tag);
				if (section)
				{
					if (!section.edit)
					{
						console.log('skipped edit ' + section.tag);
					}
					else
					{
						var self= this;
						if (!self.model)
							self.model = {};
						var on_change_model= function()
						{
							console.log('on_change_model');
							console.log(self.model);
							$text= $par.find('div.text');
							$text.html(section.html(self.model));
						}
						section.edit(this.model, on_change_model);
					}
				}
			}
		}

		return controller;
	}
});
