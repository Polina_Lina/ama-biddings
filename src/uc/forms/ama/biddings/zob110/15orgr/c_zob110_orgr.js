define([
	  'forms/base/c_binded'
	, 'tpl!forms/ama/biddings/zob110/15orgr/e_zob110_orgr.html'
],
function (c_binded, tpl)
{
	return function()
	{
		var controller= c_binded(tpl);

		return controller;
	}
});
