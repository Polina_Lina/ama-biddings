define([
	  'tpl!forms/ama/biddings/zob110/15orgr/v_zob110_orgr.html'
	, 'tpl!forms/ama/biddings/zob110/15orgr/v_zob110_orgr_help.html'
	, 'forms/ama/biddings/zob110/15orgr/c_zob110_orgr'
	, 'forms/base/h_msgbox'
],
function (v, v_help, c, h_msgbox)
{
	var nav= 'организатор';
	var h_spec = {
		tag:'15orgr'
		,nav:nav
		,title:'Сведения об организаторе торгов, его почтовый адрес, адрес электронной почты, номер контактного телефона'
		, html: v
		, help: v_help
		, edit: function(cfb, on_change)
		{
			var controller= c();
			var r= (!cfb 
				|| !cfb.Организатор_торгов 
				|| !cfb.Организатор_торгов.Информация_в_объявление_для_участников_торгов)
				? {}
				: cfb.Организатор_торгов.Информация_в_объявление_для_участников_торгов;
			controller.SetFormContent({ 
				адрес_электронной_почты: r.адрес_электронной_почты
				, номер_контактного_телефона: r.номер_контактного_телефона
				, почтовый_адрес: r.почтовый_адрес
				, дополнительные_сведения: r.дополнительные_сведения
			});
			var btnOk= 'Сохранить раздел "' + nav + '"';
			h_msgbox.ShowModal({
				title:'Редактирование раздела сообщения о продаже "' + nav + '"'
				,width:700
				,controller:controller
				,buttons:[btnOk,'Отмена']
				, onclose: function (btn)
				{
					if (btnOk==btn)
					{
						var res= controller.GetFormContent();
						var Организатор_торгов= cfb.Организатор_торгов;
						if (!Организатор_торгов)
							Организатор_торгов= cfb.Организатор_торгов = {};
						Организатор_торгов.Информация_в_объявление_для_участников_торгов = {
							адрес_электронной_почты: res.адрес_электронной_почты
							,номер_контактного_телефона: res.номер_контактного_телефона
							,почтовый_адрес: res.почтовый_адрес
							,дополнительные_сведения: res.дополнительные_сведения
						}
						on_change(cfb);
					}
				}
			})
		}
	};
	return h_spec;
});
