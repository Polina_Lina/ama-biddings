define([
	  'forms/base/c_binded'
	, 'tpl!forms/ama/biddings/zob110/11rwin/e_zob110_rwin.html'
],
function (c_binded, tpl)
{
	return function()
	{
		var controller= c_binded(tpl);

		return controller;
	}
});
