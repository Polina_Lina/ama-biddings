define([
	  'tpl!forms/ama/biddings/zob110/11rwin/v_zob110_rwin.html'
	, 'tpl!forms/ama/biddings/zob110/11rwin/v_zob110_rwin_help.html'
	, 'forms/ama/biddings/zob110/11rwin/c_zob110_rwin'
	, 'forms/base/h_msgbox'
],
function (v, v_help, c, h_msgbox)
{
	var nav= 'выявление победителя';
	var h_spec = {
		tag:'11rwin'
		,nav:nav
		,title:'Порядок и критерии выявления победителя торгов'
		, html: v
		, help: v_help
		, edit: function(cfb, on_change)
		{
			var controller= c();
			var r= (!cfb 
				|| !cfb.Регламенты 
				|| !cfb.Регламенты.Регламент_завершения_торгов 
				|| !cfb.Регламенты.Регламент_завершения_торгов.Выявление_победителей)
				? {}
				: cfb.Регламенты.Регламент_завершения_торгов.Выявление_победителей;
			controller.SetFormContent({ 
				Порядок: r.Порядок, Критерии: r.Критерии
			});
			var btnOk= 'Сохранить раздел "' + nav + '"';
			h_msgbox.ShowModal({
				title:'Редактирование раздела сообщения о продаже "' + nav + '"'
				,width:700
				,controller:controller
				,buttons:[btnOk,'Отмена']
				, onclose: function (btn)
				{
					if (btnOk==btn)
					{
						var res= controller.GetFormContent();
						var Регламенты= cfb.Регламенты;
						if (!Регламенты)
							Регламенты= cfb.Регламенты = {};
						var Регламент_завершения_торгов= Регламенты.Регламент_завершения_торгов;
						if (!Регламент_завершения_торгов)
							Регламент_завершения_торгов = Регламенты.Регламент_завершения_торгов= {};
						console.log(res);
						Регламент_завершения_торгов.Выявление_победителей = {
							Порядок: res.Порядок, Критерии: res.Критерии
						}
						on_change(cfb);
					}
				}
			})
		}
	};
	return h_spec;
});
