define([
	  'forms/base/c_binded'
	, 'tpl!forms/ama/biddings/zob110/07docs/e_zob110_docs.html'
],
function (c_binded, tpl)
{
	return function()
	{
		var controller= c_binded(tpl);

		return controller;
	}
});
