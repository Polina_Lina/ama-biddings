define([
	  'tpl!forms/ama/biddings/zob110/13cntr/v_zob110_cntr.html'
	, 'tpl!forms/ama/biddings/zob110/13cntr/v_zob110_cntr_help.html'
	, 'forms/ama/biddings/zob110/13cntr/c_zob110_cntr'
	, 'forms/base/h_msgbox'
],
function (v, v_help, c, h_msgbox)
{
	var nav= 'заключения договора';
	var h_spec = {
		tag:'13cntr'
		,nav:nav
		,title:'Порядок и срок заключения договора купли-продажи'
		, html: v
		, help: v_help
		, edit: function(cfb, on_change)
		{
			var controller= c();
			var r= (!cfb 
				|| !cfb.Регламенты 
				|| !cfb.Регламенты.Регламент_купли_продажи 
				|| !cfb.Регламенты.Регламент_купли_продажи.Заключение_договора)
				? {}
				: cfb.Регламенты.Регламент_купли_продажи.Заключение_договора;
			controller.SetFormContent({ 
				Порядок: r.Порядок, Срок: r.Срок, Порядок_и_срок_общим_текстом: r.Порядок_и_срок_общим_текстом
				, Общим_текстом: (!r.Порядок && !r.Срок)
			});
			var btnOk= 'Сохранить раздел "' + nav + '"';
			h_msgbox.ShowModal({
				title:'Редактирование раздела сообщения о продаже "' + nav + '"'
				,width:700
				,controller:controller
				,buttons:[btnOk,'Отмена']
				, onclose: function (btn)
				{
					if (btnOk==btn)
					{
						var res= controller.GetFormContent();
						var Регламенты= cfb.Регламенты;
						if (!Регламенты)
							Регламенты= cfb.Регламенты = {};
						var Регламент_купли_продажи= Регламенты.Регламент_купли_продажи;
						if (!Регламент_купли_продажи)
							Регламент_купли_продажи = Регламенты.Регламент_купли_продажи = {};
						Регламент_купли_продажи.Заключение_договора =
							res.Общим_текстом 
							? { Порядок_и_срок_общим_текстом: res.Порядок_и_срок_общим_текстом }
							: { Порядок: res.Порядок, Срок: res.Срок };
						on_change(cfb);
					}
				}
			})
		}
	};
	return h_spec;
});
