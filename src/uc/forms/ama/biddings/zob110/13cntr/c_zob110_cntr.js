define([
	  'forms/base/c_binded'
	, 'tpl!forms/ama/biddings/zob110/13cntr/e_zob110_cntr.html'
],
function (c_binded, tpl)
{
	return function()
	{
		var controller= c_binded(tpl);

		var base_Render= controller.Render;
		controller.Render= function(sel)
		{
			if (!this.model)
				this.model = {Общим_текстом:true};
			base_Render.call(this,sel);

			var self= this;
			$(sel + ' input').change(function () { self.OnChangeVariant(sel); })
		}

		controller.OnChangeVariant= function(sel)
		{
			var checked= 'checked'==$(sel + ' input').attr('checked');
			var variant= checked ? 'common' : 'separate';
			$(sel + ' > div').attr('data-variant',variant);
		}

		return controller;
	}
});
