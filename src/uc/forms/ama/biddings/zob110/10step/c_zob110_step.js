define([
	  'forms/base/c_binded'
	, 'tpl!forms/ama/biddings/zob110/10step/e_zob110_step.html'
],
function (c_binded, tpl)
{
	return function()
	{
		var controller= c_binded(tpl);

		return controller;
	}
});
