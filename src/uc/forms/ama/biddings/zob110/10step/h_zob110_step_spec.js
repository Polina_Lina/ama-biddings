define([
	  'tpl!forms/ama/biddings/zob110/10step/v_zob110_step.html'
	, 'tpl!forms/ama/biddings/zob110/10step/v_zob110_step_help.html'
	, 'forms/ama/biddings/zob110/10step/c_zob110_step'
	, 'forms/base/h_msgbox'
],
function (v, v_help, c, h_msgbox)
{
	var nav= 'шаг аукциона';
	var h_spec = {
		tag:'10step'
		,nav:nav
		,title:'Величина повышения начальной цены продажи предприятия ("шаг аукциона")'
		, html: v
		, help: v_help
		,readonly:true
		, edit: function(cfb, on_change)
		{
			var controller= c();
			controller.SetFormContent({});
			h_msgbox.ShowModal({
				title:'Редактирование раздела сообщения о продаже "' + nav + '"'
				,width:700
				,controller:controller
				,buttons:['Закрыть']
			})
		}
	};
	return h_spec;
});
