define([
	  'tpl!forms/ama/biddings/zob110/04conc/v_zob110_conc.html'
	, 'tpl!forms/ama/biddings/zob110/04conc/v_zob110_conc_help.html'
	, 'forms/ama/biddings/zob110/04conc/c_zob110_conc'
	, 'forms/base/h_msgbox'
],
function (v, v_help, c, h_msgbox)
{
	var nav= 'условия конкурса';
	var h_spec = {
		tag:'04conc'
		,nav:nav
		,title:'Условия конкурса'
		, html: v
		, help: v_help
		, conditional: true
		, edit: function(cfb, on_change)
		{
			var controller= c();
			controller.SetFormContent({ 
				Условия_конкурса: !cfb ? '' : cfb.Условия_конкурса
			});
			var btnOk= 'Сохранить раздел "' + nav + '"';
			h_msgbox.ShowModal({
				title:'Редактирование раздела сообщения о продаже "' + nav + '"'
				,width:700
				,controller:controller
				,buttons:[btnOk,'Отмена']
				, onclose: function (btn)
				{
					if (btnOk==btn)
					{
						var res= controller.GetFormContent();
						cfb.Условия_конкурса= res.Условия_конкурса;
						on_change(cfb);
					}
				}
			})
		}
	};
	return h_spec;
});
