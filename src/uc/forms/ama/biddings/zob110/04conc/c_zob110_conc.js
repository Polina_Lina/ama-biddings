define([
	  'forms/base/c_binded'
	, 'tpl!forms/ama/biddings/zob110/04conc/e_zob110_conc.html'
],
function (c_binded, tpl)
{
	return function()
	{
		var controller= c_binded(tpl);

		return controller;
	}
});
