﻿include ..\..\..\..\..\..\wbt.lib.txt quiet
execute_javascript_stored_lines add_wbt_std_functions

wait_text "Добавить"

js wbt_FocusSelect2('.select-wrapper');

shot_check_png ..\..\shots\02sav.png

js $("[collection-index='0'] a.do_delete").click();

shot_check_png ..\..\shots\03sav.png

wait_click_text "Сохранить содержимое формы"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\03sav.json.result.txt

exit