﻿define([
	'forms/base/h_file'
],
function (h_file)
{
	var res= {
		BuildLot: function(kmobjects)
		{
			var res = null;
			if (kmobjects && null != kmobjects && 0 != kmobjects.length)
			{
				if (1 == kmobjects.length)
				{
					var kmobject = kmobjects[0];
					res = {
						Наименование: kmobject.СокращённоеНазвание
						, Начальная_цена: kmobject.ОценочнаяСтоимость
						, Краткое_описание: kmobject.СокращённоеНазвание
					};
					if (kmobject.Информация_об_объекте)
						res.Точный_адрес = kmobject.Информация_об_объекте.Адрес;
				}
				else
				{
					var title = kmobjects.length + h_file.GetNumbering(kmobjects.length, ' объект', ' объекта', ' объектов');
					var price = 0;
					var description = '';
					var group = null;
					for (var i= 0; i<kmobjects.length; i++)
					{
						var kmobject = kmobjects[i];
						price += (!kmobject.ОценочнаяСтоимость || null == kmobject.ОценочнаяСтоимость) ? 0 : kmobject.ОценочнаяСтоимость;
						description += (!kmobject.СокращённоеНазвание || null == kmobject.СокращённоеНазвание) ? '' : (kmobject.СокращённоеНазвание + ';\r\n');
						var new_group = ' группы "' + kmobject.Группа + '\"';
						group = (null == group || group == new_group) ? new_group : ' конкурсной массы';
					}
					res = {
						Наименование: title + group
						, Начальная_цена: price
						, Краткое_описание: description
					};
				}
			}
			if (res && null != res && res.Начальная_цена)
			{
				//res.Размер_задатка = Math.floor(res.Начальная_цена * 0.1);
				//res.Шаг_аукциона = Math.floor(res.Начальная_цена * 0.1);
				res.Размер_задатка = 10;
				res.Шаг_аукциона = 10;
			}
			return res;
		}
		, BuildTextForLink: function (kmobjects)
		{
			return (!kmobjects || null == kmobjects || 0 == kmobjects.length)
			 ? 'Кликните, чтобы выбрать соответствующие объекты конкурсной массы'
			 : kmobjects.length + h_file.GetNumbering(kmobjects.length,
				' объект конкурсной массы, входящий в лот',
				' объекта конкурсной массы, входящих в лот',
				' объектов конкурсной массы, входящих в лот');
		}
	};
	return res;
});
