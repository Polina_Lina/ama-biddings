﻿define([
	  'forms/base/h_file'
	, 'forms/base/guid'
],
function (h_file, guid)
{
	var res =
	{
		BuildBiddingName: function(bidding)
		{
			if (!bidding || !bidding.Лоты || null == bidding.Лоты || 0 == bidding.Лоты.length)
			{
				return 'Торги';
			}
			else if (1 == bidding.Лоты.length)
			{
				return bidding.Лоты[0].Наименование;
			}
			else
			{
				var txtres = '';
				txtres += bidding.Лоты.length;
				txtres += ' ';
				txtres += h_file.GetNumbering(bidding.Лоты.length,'лот','лота','лотов');
				txtres += ' начиная с ';
				txtres += bidding.Лоты[0].Наименование;
				return txtres;
			}
		}

		,BuildBiddingByLot: function(lot)
		{
			var bidding = {
				Лоты: [lot],
				bidding_id: guid.newGuid(),
				isActive: false,
				URL: "",
				Начало_приёма_заявок: { дата: "", время: "" },
				Окончание_приёма_заявок: { дата: "", время: "" },
				Начало_торгов: { дата: "", время: "" },
				Форма_торгов: "Аукцион в открытой форме",
				ЭТП: {
					id: 0,
					text: "",
					url: "",
					operator: "",
					uploadurl: "",
					format: ""
				},
				Объявление: {
					ЕФРСБ: { Номер: "" },
					ФедеральноеСМИ: { дата: "", номер_тиража: "" },
					МестноеСМИ: { Наименование: "", дата: "", номер_тиража: "" }
				}
			};
			bidding.Наименование = this.BuildBiddingName(bidding);
			return bidding;
		}

		,AddLotToBiddingOnCreate: function(bidding,lot)
		{
			var old_std_name = this.BuildBiddingName(bidding);
			bidding.Лоты.push(lot);
			if (bidding.Наименование == old_std_name)
				bidding.Наименование = this.BuildBiddingName(bidding);
		}
	};
	return res;
});
