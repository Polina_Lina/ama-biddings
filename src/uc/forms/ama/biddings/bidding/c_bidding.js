define([
	  'forms/base/c_binded'
	, 'tpl!forms/ama/biddings/bidding/e_bidding.html'
	, 'forms/ama/biddings/base/etp/etp'
	, 'forms/ama/biddings/bidding/r_bidding'
],
function (c_binded, tpl, etps, r_bidding)
{
	return function()
	{
		var options =
		{
			constraints: r_bidding()
			,field_spec:
			{
				'ЭТП': function ()
				{
					var spec =
					{
						placeholder: 'Выберите электронную торговую площадку'
						, allowClear: true
						, query: function (query)
						{
							var term = query.term.toUpperCase();
							var res = [];
							for (var i = 0; i < etps.length; i++)
							{
								var item = etps[i];
								var item_text = item.text.toUpperCase();
								if (-1 != item_text.indexOf(term))
									res.push(item);
							}
							query.callback({ results: res });
						}
						, escapeMarkup: function (m) { return m; }
						, dropdownCssClass: "bigdrop" // apply css that makes the dropdown taller
					};
					return spec;
				}
			}
		};
		return c_binded(tpl, options);
	}
});
