define([
	  'forms/ama/biddings/biddings/c_biddings'
	, 'txt!forms/ama/biddings/biddings/tests/contents/biddings1.json.txt'
],
function (c_biddings, biddings_content)
{
	return function (options)
	{
		var controller = c_biddings(options);

		controller.CreateNew = function (sel)
		{
			this.SetFormContent(biddings_content);
			this.Edit(sel);
		}

		return controller;
	}
});
