define(['forms/ama/biddings/transit/demo-ama/c_demo-ama'],
function (CreateController)
{
	var form_spec =
	{
		  CreateController: CreateController
		, key: 'demo-biddings'
		, Title: 'Торги имуществом банкрота'
		, FileFormat: 
			{
				  FileExtension: 'cfb'
				, Description: 'CFB. Торги имуществом банкрота'
			}
	};
	return form_spec;
});
