define([
	  'forms/base/controller'
	, 'tpl!forms/ama/biddings/transit/biddings/e_transit-biddings.html'
],
function (c_binded, tpl)
{
	return function (options)
	{
		var controller = c_binded(tpl);

		controller.base_transit_url = options && options.base_transit_url
			? options.base_transit_url
			: '/ama-biddings-transit/transit.php';

		var colModel =
		[
			  { label: 'Id', name: 'id_TransitBidding', key: true, hidden: true }
			, { label: 'Дата', name: 'date_access', width: 100, searchoptions: { sopt: ['cn'] } }
			, { label: 'на ЭТП ', name: 'etp', searchoptions: { sopt: ['cn']} }
			, { label: 'с IP', name: 'IP', width: 100, searchoptions: { sopt: ['cn']} }
			, { label: 'Токен', name: 'token_bidding', width: 110, searchoptions: { sopt: ['cn'] } }
			, { label: 'Адрес URL', name: 'URL', searchoptions: { sopt: ['cn'] } }
		];

		controller.Render = function (sel)
		{
			this.controller_selector = sel;
			var self = this;
			$(sel).html(tpl());
			var grid = $(sel + ' table#cpw-rit-bidding-grid1');
			grid.jqGrid
			({
				  datatype: "json"
				, url: self.base_transit_url + '?action=biddings-g'
				, colModel: colModel
				, gridview: true
				, loadtext: 'Загрузка...'
				, rownumbers: false
				, rowNum: 5
				, rowList: [5, 10, 15, 50, 100]
				, pager: '#gridPager'
				, viewrecords: true
				, width: '947'
				, height: 'auto'
				, loadError: function Error(xhr, st, err) { alert('Сервер вернул вместо содержимого для таблицы, код ошибки..'); }
				, ondblClickRow: function () { self.OnOpenDisplayBlank(grid); }
			});

			grid.jqGrid('filterToolbar', { stringResult: true, searchOnEnter: false });
			$(sel + ' button.open').click(function () { self.OnOpenDisplayBlank(grid); });
		}

		controller.OnOpenDisplayBlank = function (grid)
		{
			var id = grid.jqGrid('getGridParam', 'selrow');
			window.open(this.base_transit_url + "?action=view&id_TransitBidding=" + id, "_blank");
		}

		controller.CreateNew = function (sel) { this.Render(sel); }

		controller.Edit = function (sel) { this.Render(sel); }

		controller.SetFormContent = function (content) { }

		controller.GetFormContent = function () { return null; }

		controller.Validate = function () { return null; }

		return controller;
    }
})