define(['forms/ama/biddings/transit/biddings/c_transit-biddings'],
function (CreateController) {
    var form_spec =
	{
	    CreateController: CreateController
		, key: 'transitbidding'
		, Title: 'Информация о передачах на ЭТП'
		, FileFormat:
			{
			    FileExtension: 'cfb'
				, Description: 'CFB. Объявление о торгах'
			}
	};
    return form_spec;
});
