define(['forms/ama/biddings/biddings/c_biddings'],
function (CreateController)
{
	var form_spec =
	{
		  CreateController: CreateController
		, key: 'biddings'
		, Title: 'Торги имуществом банкрота'
		, FileFormat: 
			{
				  FileExtension: 'cfb'
				, Description: 'CFB. Торги имуществом банкрота'
			}
	};
	return form_spec;
});
