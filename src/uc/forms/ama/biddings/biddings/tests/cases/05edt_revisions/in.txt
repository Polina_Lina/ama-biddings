﻿include ..\..\..\..\..\..\wbt.lib.txt quiet
include ..\..\..\..\kmobjects\tests\cases\in.lib.txt quiet
include ..\..\..\..\bidding\tests\cases\in.lib.txt quiet
include ..\..\..\..\lot\tests\cases\in.lib.txt quiet

execute_javascript_stored_lines add_wbt_std_functions
je Math.random= function () {return 0.23; }
je $('div.cpw-forms-store-status').css('color','gray').css('background-color','gray');

wait_text "Торги:"
shot_check_png ..\..\shots\biddings1.png

######################### start revision:
jr app.current_form_spec.c_store_status.revision;
######################### revision after change current bidding:
je $('select.selector').val(1).change();
shot_check_png ..\..\shots\01edt-1.png
jr app.current_form_spec.c_store_status.revision;
######################### revision after add bidding:
je $('a.add').click();
play_stored_lines km_objects_add_debt
click_text "Сохранить объекты конкурсной массы для лота"
wait_text "1 объект конкурсной массы"
click_text "Сохранить и к торгам"
jr app.current_form_spec.c_store_status.revision;
play_stored_lines bidding_fields_2
click_text "Сохранить параметры торгов"
jr app.current_form_spec.c_store_status.revision;
######################### revision after delete bidding:
je $('a.delete').click();
wait_click_text "Да, удалить данные о торгах"
jr app.current_form_spec.c_store_status.revision;
######################### revision after change bidding head:
je $('a.edit').click();
wait_click_text "Сохранить параметры торгов"
jr app.current_form_spec.c_store_status.revision;
######################### revision after change bidding lot:
wait_click_text "238476"
wait_click_text "Сохранить параметры лота"
jr app.current_form_spec.c_store_status.revision;
######################### revision after delete bidding lot:
je $('.collection-item[collection-index="0"] a.ama-icon-remove').click();
click_text "Да, удалить лот"
jr app.current_form_spec.c_store_status.revision;
######################### revision after add bidding lot:
click_text "Добавить"
je $("#cpw-ama-bidding-modal-kmobjects").dialog('close');
play_stored_lines lot_fields_1
click_text "Сохранить параметры лота"
jr app.current_form_spec.c_store_status.revision;

wait_click_text "Сохранить содержимое формы"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\05sav_revisions.json.result.txt

exit