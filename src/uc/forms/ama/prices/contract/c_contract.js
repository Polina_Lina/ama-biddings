define([
	  'forms/base/c_binded'
	, 'tpl!forms/ama/prices/contract/e_contract.html'
	, 'forms/ama/prices/base/h_input_increment'
],
function (c_binded, tpl, h_input_increment)
{
	return function()
	{
		var controller = c_binded(tpl);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			h_input_increment.process_keys_to_increment(sel + ' input[type="text"]');
		}

		return controller;
	}
});
