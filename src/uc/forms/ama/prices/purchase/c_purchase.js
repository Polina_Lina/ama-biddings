define([
	  'forms/base/c_binded'
	, 'tpl!forms/ama/prices/purchase/e_purchase.html'
	, 'forms/ama/prices/calculation/h_prices'
	, 'forms/ama/prices/base/h_input_increment'
],
function (c_binded, tpl, h_prices, h_input_increment)
{
	return function()
	{
		var controller = c_binded(tpl);

		var base_Render= controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			var self = this;
			$(sel + ' select').change(function () { self.OnChangeType(); });

			h_input_increment.process_keys_to_increment(sel + ' input[type="text"]');
		}

		controller.OnChangeType= function()
		{
			var sel = this.binding.form_div_selector;
			$(sel + ' select').val();

			var Редакция = $(sel + ' select').val();
			var База = h_prices.Для_редакции[Редакция].База;

			$(sel + ' input[model_field_name="Количество.АУ"]').val(База.Количество.АУ);
			$(sel + ' input[model_field_name="Количество.Рабочих_мест"]').val(База.Количество.Мест);
		}

		return controller;
	}
});
