﻿define(['forms/base/h_times'], function (h_times)
{
	var calc_step = function (int_value, sign)
	{
		return (1 == sign)
		? (
			int_value < 10 ? 1 :
			int_value < 100 ? 10 :
			int_value < 1000 ? 100 :
			int_value < 10000 ? 1000 :
			int_value < 100000 ? 10000 : 100000
		) : (
			int_value <= 10 ? 1 :
			int_value <= 100 ? 10 :
			int_value <= 1000 ? 100 :
			int_value <= 10000 ? 1000 :
			int_value <= 100000 ? 10000 : 100000
		);
	};
	var Format_date = function (dt)
	{
		var d = dt.getDate() + '';
		if (1 == d.length)
			d = '0' + d;
		var m = (1 + dt.getMonth()) + '';
		if (1 == m.length)
			m = '0' + m;
		return d + '.' + m + '.' + dt.getFullYear();
	};
	return {
		process_keys_to_increment: function(sel)
		{
			$(sel).keydown(function (e)
			{
				var sign = 0;
				switch (e.keyCode)
				{
					case 38: // вверх
						sign= 1;
						break;
					case 40: // вниз
						sign= -1;
						break;
				}
				if (null!=sign)
				{
					var item = $(e.target);
					var value = item.val();
					if ('' != value)
					{
						if ('date' != item.attr('renderas'))
						{
							var int_value = parseInt(value);
							var step = calc_step(int_value, sign);
							var new_value = int_value + sign * step;
							if (0 <= new_value && new_value < 1000000)
								item.val(new_value).change();
						}
						else
						{
							var dt = h_times.ParseRussianDate(value);
							var new_dt = dt;
							if (e.ctrlKey)
							{
								new_dt.setMonth(new_dt.getMonth() + sign);
							}
							else
							{
								new_dt = new Date(new_dt.setTime(new_dt.getTime() + sign * 86400000));
							}
							item.val(Format_date(new_dt)).change();
						}
					}
				}
			});
		}
	};
});