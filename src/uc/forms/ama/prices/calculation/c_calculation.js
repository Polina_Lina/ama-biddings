define([
	  'forms/base/c_binded'
	, 'tpl!forms/ama/prices/calculation/e_calculation.html'
	, 'forms/ama/prices/contract/c_contract'
	, 'forms/ama/prices/purchase/c_purchase'
	, 'forms/ama/prices/additional/c_additional'
	, 'tpl!forms/ama/prices/calculation/v_calculation.html'
	, 'forms/ama/prices/calculation/h_calculation'
	, 'forms/ama/prices/calculation/h_to_test'
	, 'tpl!forms/ama/prices/calculation/v_test_samples.html'
	, 'tpl!forms/ama/prices/calculation/v_comments.html'
	, 'forms/base/codec/codec.copy'
	, 'forms/base/h_number'
	, 'tpl!forms/ama/prices/calculation/v_ama_prices.html'
	, 'tpl!forms/ama/prices/tabs/v_legenda.html'
	, 'tpl!forms/ama/prices/tabs/v_rules.html'
	, 'forms/ama/prices/calculation/h_prices'
	, 'forms/base/h_times'
],
function (c_binded, tpl, с_contract, c_purchase, c_additional, v_calculation, h_calculation, h_to_test, v_test_samples, v_comments, ccopy, h_number, v_ama_prices, v_legenda, v_rules, h_prices, h_times)
{
	return function()
	{
		var options = {
			h_to_test: h_to_test
			, field_spec:
			{
				  'Договор.Будет': { controller: с_contract }
				, 'Договор.Был': { controller: с_contract }

				, 'Хочу.Купить': { controller: c_purchase }
				, 'Предлагается.Купить': { controller: c_purchase }

				, 'Хочу.Докупить': { controller: c_additional }
				, 'Предлагается.Докупить': { controller: c_additional }
			}
		};
		var controller = c_binded(tpl, options);

		var base_Render= controller.Render;
		controller.Render= function(sel)
		{
			this.model = {
				Дата_рассчёта: '13.09.2017'
				, Релиз: {
					Предпоследний: {
						Дата: '01.04.2017'
					}
				}
				, Тип: 'Покупка'
				, Хочу: {
					Купить: {
						Редакция: "про"
						, Техподдержка: true
						, Количество: {
							АУ: 1
							, Рабочих_мест: 3
						}
					}
					, Докупить: {
						Техподдержка: true
						, Разовое_обновление: true
						, Количество: {
							АУ: 0
							, Рабочих_мест: 1
						}
					}
				}
				, Договор: {
					Был: {
						Количество: {
							АУ: "1"
							, Рабочих_мест: "3"
							, Предприятий: ""
						}
						, Стоимость_компонентов: "28890"
						, Техподдержка_до: "13.10.2017"
						, Редакция: "про"
					}
				}
			};
			base_Render.call(this, sel);

			var self = this;
			$(sel + ' input[model_field_name="Тип"]').click(function (e) { self.OnChangeType(); });

			$(sel + ' button.calculate').click(function () { self.Calculate(); });
			self.Calculate();

			$(sel + ' div[model_field_name="Хочу.Купить"] input').change(function () { self.OnSomethingChanged(); });
			$(sel + ' div[model_field_name="Хочу.Купить"] select').change(function () { self.OnSomethingChanged(); });
			$(sel + ' div[model_field_name="Хочу.Докупить"] input').change(function () { self.OnSomethingChanged(); });
			$(sel + ' div[model_field_name="Договор.Был"] input').change(function () { self.OnSomethingChanged(); });
			$(sel + ' div[model_field_name="Договор.Был"] select').change(function () { self.OnSomethingChanged(); });

			$(sel + ' select.test-variant').change(function () { self.OnSelectTestVariant(); });
		}

		controller.OnSelectTestVariant= function()
		{
			var sel = this.binding.form_div_selector;
			var controls = this.binding.controls;
			var selected_test_name = $(sel + ' select.test-variant').val();
			if ('' != selected_test_name)
			{
				var selected_test = h_to_test[selected_test_name];

				$(sel + ' input[model_field_name="Дата_рассчёта"]').val(selected_test.Дата_рассчёта);
				$(sel + ' input[model_field_name="Релиз.Предпоследний.Дата"]').val(selected_test.Релиз.Предпоследний.Дата);

				if (selected_test.Хочу.Купить)
				{
					var form_sel = sel + ' div[model_field_name="Хочу.Купить"]';
					$(form_sel).html('');
					var form = controls['Хочу.Купить'].controller;
					form.SetFormContent(selected_test.Хочу.Купить);
					form.Render(form_sel);
				}

				this.Calculate();
			}
		}

		controller.OnChangeType= function()
		{
			var sel = this.binding.form_div_selector;
			var Тип = $(sel + ' input[model_field_name="Тип"]:checked').val();
			$(sel + ' div.cpw-ama-prices-calculator').attr('variant', Тип);
			this.OnSomethingChanged();
		}

		controller.OnSomethingChanged= function()
		{
			var sel = this.binding.form_div_selector;
			if ($(sel + ' input.auto-recalc').attr('checked'))
				this.Calculate();
		}

		controller.Calculate= function()
		{
			var sel = this.binding.form_div_selector;
			var controls = this.binding.controls;

			var Тип = $(sel + ' input[model_field_name="Тип"]:checked').val();
			var дата_рассчёта = $(sel + ' input[model_field_name="Дата_рассчёта"]').val();
			if ('Покупка' == Тип)
			{
				var покупка = controls['Хочу.Купить'].controller.GetFormContent();
				var обсчёт = h_calculation.Обсчитать.покупку(дата_рассчёта, покупка);
			}
			else
			{
				var докупка = controls['Хочу.Докупить'].controller.GetFormContent();
				var договор = controls['Договор.Был'].controller.GetFormContent();
				var дата_предпоследнего_релиза = $(sel + ' input[model_field_name="Релиз.Предпоследний.Дата"]').val();
				var обсчёт = h_calculation.Обсчитать.докупку(дата_рассчёта, докупка, договор, { Предпоследний: { Дата: дата_предпоследнего_релиза } });
			}

			var Рассчёт = ccopy().Copy(обсчёт.Рассчёт);
			if (Рассчёт)
				Рассчёт.h_number = h_number;
			$(sel + ' div.calculation').html(v_calculation(Рассчёт));
			$(sel + ' div.comments').html(v_comments(обсчёт.Пояснения));

			var form_sel = sel + ' div[model_field_name="Договор.Будет"]';
			$(form_sel).html('');
			var form = controls['Договор.Будет'].controller;
			form.SetFormContent(обсчёт.Договор);
			form.Render(form_sel);

			if ('Покупка' == Тип)
			{
				var form_sel = sel + ' div[model_field_name="Предлагается.Купить"]';
				$(form_sel).html('');
				var form = controls['Предлагается.Купить'].controller;
				form.SetFormContent(обсчёт.Покупка);
				form.Render(form_sel);
			}
			else
			{
				var form_sel = sel + ' div[model_field_name="Предлагается.Докупить"]';
				$(form_sel).html('');
				var form = controls['Предлагается.Докупить'].controller;
				form.SetFormContent(обсчёт.Докупка);
				form.Render(form_sel);
			}

			$(sel + ' div[model_field_name="Предлагается.Купить"] input').attr('disabled', 'disabled');
			$(sel + ' div[model_field_name="Предлагается.Купить"] select').attr('disabled', 'disabled');
			$(sel + ' div[model_field_name="Предлагается.Докупить"] input').attr('disabled', 'disabled');
			$(sel + ' div[model_field_name="Предлагается.Докупить"] select').attr('disabled', 'disabled');
			$(sel + ' div[model_field_name="Договор.Будет"] input').attr('disabled', 'disabled');
			$(sel + ' div[model_field_name="Договор.Будет"] select').attr('disabled', 'disabled');
		}

		controller.BuildHtmlReport = function (earg)
		{
			if ('ama-prices' == earg)
			{
				return v_ama_prices({
					samples: v_test_samples({ to_test_cases: h_to_test, h_calculation: h_calculation, h_number: h_number })
					, legenda: v_legenda()
					, rules: v_rules({ h_prices: h_prices, h_number: h_number })
					, h_times: h_times
				});
			}
			else
			{
				var примеры = [];
				var res = { параметры: h_prices, примеры: примеры };

				var Дата_рассчёта = "13.09.2017";
				var Релиз = { Предпоследний: { Дата: "01.04.2017" } };
				for (var name in h_to_test)
				{
					var варианты = [];
					var пример = { Название: name, варианты: варианты };
					var to_test_group = h_to_test[name];
					for (var i = 0; i < to_test_group.length; i++)
					{
						var вариант = { Дата_рассчёта: Дата_рассчёта, Релиз: Релиз };
						var to_test = to_test_group[i];
						if (to_test.Договор)
							вариант.Было = { Договор: to_test.Договор };
						вариант.Хочу = (to_test.Купить) ? { Купить: to_test.Купить } : { Докупить: to_test.Доеупить };
						var обсчитано = to_test.Купить
							? h_calculation.Обсчитать.покупку(Дата_рассчёта, to_test.Купить)
							: h_calculation.Обсчитать.докупку(Дата_рассчёта, to_test.Докупить, to_test.Договор, Релиз);
						if (обсчитано.Покупка || обсчитано.Докупка)
							вариант.Предлагается = обсчитано.Покупка ? { Покупка: обсчитано.Покупка } : { Докупка: обсчитано.Докупка };
						if (обсчитано.Договор)
							вариант.Будет = обсчитано.Договор;
						if (обсчитано.Рассчёт)
							вариант.Рассчёт = обсчитано.Рассчёт;
						if (обсчитано.Пояснения)
							вариант.Пояснения = обсчитано.Пояснения;
						варианты.push(вариант);
					}
					примеры.push(пример);
				}

				return JSON.stringify(res,null,'\t').replace(/\n/g,'\r\n');
			}
		}

		return controller;
	}
});
