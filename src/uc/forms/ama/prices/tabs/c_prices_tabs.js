define([
	  'forms/base/c_binded'
	, 'tpl!forms/ama/prices/tabs/e_prices_tabs.html'
	, 'tpl!forms/ama/prices/calculation/v_test_samples.html'
	, 'forms/ama/prices/calculation/h_calculation'
	, 'forms/ama/prices/calculation/h_to_test'
	, 'tpl!forms/ama/prices/tabs/v_legenda.html'
	, 'tpl!forms/ama/prices/tabs/v_rules.html'
	, 'forms/ama/prices/calculation/h_prices'
	, 'forms/ama/prices/calculation/c_calculation'
	, 'forms/base/h_number'
	, 'tpl!forms/ama/prices/calculation/v_ama_prices.html'
	, 'forms/base/h_times'
],
function (c_binded, tpl, v_test_samples, h_calculation, h_to_test, v_legenda, v_rules, h_prices, c_calculation, h_number, v_ama_prices, h_times)
{
	return function ()
	{
		var options = {
			field_spec:
			{
				Калькулятор: { controller: c_calculation }
			}
		};
		var controller = c_binded(tpl, options);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			var self = this;
			$(sel + ' div.examples').html(v_test_samples({ to_test_cases: h_to_test, h_calculation: h_calculation, h_number: h_number }));
			$(sel + ' div.legenda').html(v_legenda());
			$(sel + ' div.rules').html(v_rules({ h_prices: h_prices, h_number: h_number }));
			$(sel + ' button.print').click(function (e) { e.preventDefault(); self.OnPrint();});
		}

		controller.OnPrint= function()
		{
			var newWin = window.open('', 'ama-prices', 'width=1000,height=700,resizable=yes,scrollbars=yes,menubar=yes,toolbar=yes');
			newWin.document.open();
			newWin.document.writeln(v_ama_prices({
				samples: v_test_samples({ to_test_cases: h_to_test, h_calculation: h_calculation, h_number: h_number })
				, legenda: v_legenda()
				, rules: v_rules({ h_prices: h_prices, h_number: h_number })
				, h_times: h_times
			}));
			newWin.document.close();
		}

		return controller;
	}
});