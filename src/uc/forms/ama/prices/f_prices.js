define(['forms/ama/prices/tabs/c_prices_tabs'],
function (CreateController)
{
	var form_spec =
	{
		  CreateController: CreateController
		, key: 'prices'
		, Title: 'Калькулятор цен'
	};
	return form_spec;
});
