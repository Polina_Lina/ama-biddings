define
(
	[
		  'contents/collector'
		, 'txt!forms/ama/biddings/lot/tests/contents/01sav.json.etalon.txt'

		, 'txt!forms/ama/biddings/lots/tests/contents/lots1.json.txt'
		, 'txt!forms/ama/biddings/lots/tests/contents/lots3.json.txt'
		, 'txt!forms/ama/biddings/lots/tests/contents/02sav.json.etalon.txt'

		, 'txt!forms/ama/biddings/bidding/tests/contents/01sav.json.etalon.txt'
		, 'txt!forms/ama/biddings/main/tests/contents/bidding1.json.txt'
		, 'txt!forms/ama/biddings/main/tests/contents/bidding_to_upload.json.txt'
		, 'txt!forms/ama/biddings/main/tests/contents/to_sberbank.json.txt'
		, 'txt!forms/ama/biddings/main/tests/contents/to_sberbank_sber_PurchaseCreate.etalon.xml'
		, 'txt!forms/ama/biddings/bidding/tests/contents/bidding1.xml'

		, 'txt!forms/ama/biddings/biddings/tests/contents/biddings1.json.txt'
		, 'txt!forms/ama/biddings/biddings/tests/contents/biddings2.json.txt'
		, 'txt!forms/ama/biddings/biddings/tests/contents/01sav-1.json.etalon.txt'

		, 'txt!forms/ama/biddings/kmobjects/tests/contents/full.json.etalon.txt'
		, 'txt!forms/ama/biddings/kmobjects/tests/contents/02sav.json.etalon.txt'
	],
	function (collect)
	{
		return collect([
		  'lot_01sav'

		, 'lots1'
		, 'lots3'
		, 'lots_02sav'

		, 'bidding_01sav'
		, 'bidding1'
		, 'bidding_to_upload'
		, 'bidding_to_sberbank'
		, 'bidding_to_sberbank_xml'
		, 'bidding1_xml'

		, 'biddings1'
		, 'biddings2'
		, 'biddings1-1'

		, 'kmobjects-full'
		, 'kmobjects-02sav'
		], Array.prototype.slice.call(arguments, 1));
	}
);