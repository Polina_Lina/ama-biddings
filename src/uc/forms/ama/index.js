define
(
	[
		  'forms/collector'
		, 'forms/ama/biddings/lot/c_lot'
		, 'forms/ama/biddings/lots/c_lots'
		, 'forms/ama/biddings/bidding/c_bidding'
		, 'forms/ama/biddings/main/c_bidding_main'
		, 'forms/ama/biddings/biddings/c_biddings'
		, 'forms/ama/biddings/kmobjects/c_kmobjects'
		, 'forms/ama/biddings/view/c_bidding_view'
		, 'forms/ama/biddings/transit/demo-ama/c_demo-ama'
		, 'forms/ama/biddings/transit/biddings/c_transit-biddings'

		, 'forms/ama/prices/contract/c_contract'
		, 'forms/ama/prices/calculation/c_calculation'
		, 'forms/ama/prices/purchase/c_purchase'
		, 'forms/ama/prices/additional/c_additional'
		, 'forms/ama/prices/tabs/c_prices_tabs'

		, 'forms/ama/biddings/zob110/c_zob110'
		, 'forms/ama/biddings/zob110/02form/c_zob110_form'
		, 'forms/ama/biddings/zob110/13cntr/c_zob110_cntr'
	],
	function (collect)
	{
		return collect([
		  'lot'
		, 'lots'
		, 'bidding'
		, 'bidding_main'
		, 'biddings'
		, 'kmobjects'
		, 'bidding_view'
		, 'demo-biddings'
		, 'transit-biddings'

		, 'prices-contract'
		, 'prices-calculation'
		, 'prices-purchase'
		, 'prices-additional'
		, 'prices-tabs'

		, 'zob110'
		, 'zob110_form'
		, 'zob110_cntr'
		], Array.prototype.slice.call(arguments,1));
	}
);