define([
	  'forms/base/c_binded'
	, 'tpl!forms/base/store_status/v_store_status.html'
	, 'forms/base/h_times'
],
function (c_binded, tpl, h_times)
{
	return function()
	{
		var controller = c_binded(tpl);

		controller.revision = 0;

		controller.OnIncrementRevision= function()
		{
			this.revision++;
			var sel = this.binding.form_div_selector;
			$(sel + ' span.revision span.value').text(this.revision);
		}

		controller.OnGetFormContent= function()
		{
			this.time_GetFormContent = h_times.safeDateTime();
			var sel = this.binding.form_div_selector;
			$(sel + ' span.time-get-form-content span.value').text(this.time_GetFormContent.toLocaleTimeString());
			$(sel + ' span.time-get-form-content').show();
			$(sel + ' span.external-revision span.value').text(this.revision);
			$(sel + ' span.external-revision').show();
		}

		controller.OnSetFormContent = function ()
		{
			this.revision = 0;
			this.time_SetFormContent = h_times.safeDateTime();
			if (this.binding)
			{
				var sel = this.binding.form_div_selector;
				$(sel + ' span.time-get-form-content span.value').text('');
				$(sel + ' span.time-get-form-content').hide();
				$(sel + ' span.external-revision span.value').text('');
				$(sel + ' span.external-revision').hide();
			}
		}

		return controller;
	}
});
