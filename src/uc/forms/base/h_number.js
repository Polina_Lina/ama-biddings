﻿define(function ()
{
	var h_number = {};

	h_number.Format= function(num)
	{
		var snum = num + '';
		var parts = [];
		for (var pos0 = 0, pos1 = snum.length % 3; pos1 < snum.length; pos0 = pos1, pos1 += 3)
		{
			if (pos0!=pos1)
				parts.push(snum.substring(pos0, pos1));
		}
		parts.push(snum.substring(pos0, pos1));
		return parts.join(' ');
	}

	return h_number;
});