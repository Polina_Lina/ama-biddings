// node optimizers\r.js -o baseUrl=. name=optimizers/almond include=forms/fms/extension out=..\built\fms.js wrap=true
({
    baseUrl: "..",
    include: ['forms/ama/e_ama_biddings_demo'],
    name: "optimizers/almond",
    out: "..\\built\\ama-biddings-demo.js",
    wrap: true,
    map:
    {
      '*':
      {
        tpl: 'js/libs/tpl',
        txt: 'js/libs/txt'
      }
    }
})