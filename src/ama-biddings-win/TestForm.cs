﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ama_biddings_win
{
    public partial class TestForm : Form
    {
        public TestForm()
        {
            InitializeComponent();
        }

        private void TestForm_Load(object sender, EventArgs e)
        {
            string relative_path_to_index_html = @"..\..\..\ama\index.html";
            string path_to_app_exe_folder = Path.GetDirectoryName(Application.ExecutablePath);
            string path_to_index_html = Path.Combine(path_to_app_exe_folder, relative_path_to_index_html);

            webBrowser.ObjectForScripting = new ExternalHelper(webBrowser);

            webBrowser.Navigate(new Uri(path_to_index_html, System.UriKind.Absolute));
        }
    }
}
