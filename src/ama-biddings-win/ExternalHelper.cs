﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.IO;
using System.Net;
using System.Threading;
using System.Collections.Specialized;

namespace ama_biddings_win
{
    public interface IExternalAjaxHelper
    {
        void ajax(object options);
    }

    [ComVisible(true)]
    public class ExternalHelper : IExternalAjaxHelper
    {
        System.Windows.Forms.WebBrowser m_Browser;
        System.ComponentModel.BackgroundWorker m_BackgrowndWorker;

        object ajax_options= null;

        public ExternalHelper(System.Windows.Forms.WebBrowser browser)
        {
            m_Browser = browser;
            m_BackgrowndWorker = new System.ComponentModel.BackgroundWorker();
            m_BackgrowndWorker.DoWork += m_BackgrowndWorker_DoWork;
            m_BackgrowndWorker.RunWorkerAsync();
        }

        public void ajax(object options)
        {
            ajax_options = options;
        }

        string HttpGet(string url, out HttpStatusCode status)
        {
            HttpWebRequest request = (HttpWebRequest)System.Net.WebRequest.Create(url);
            return ReadResponce(request, out status);
        }

        static string ReadResponceText(HttpStatusCode status, StreamReader readStream)
        {
            if (HttpStatusCode.OK==status)
            {
                return readStream.ReadToEnd();
            }
            else
            {
                StringBuilder sb = new StringBuilder();
                for (int count = 0; count < 100; count++)
                {
                    string line = readStream.ReadLine();
                    if (null==line)
                    {
                        break;
                    }
                    else
                    {
                        sb.AppendLine(line);
                    }
                }
                return sb.ToString();
            }
        }

        static string ReadResponce(HttpWebRequest request, out HttpStatusCode status)
        {
            using (System.Net.HttpWebResponse response = (System.Net.HttpWebResponse)request.GetResponse())
            using (Stream receiveStream = response.GetResponseStream())
            using (StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8))
            {
                status = response.StatusCode;
                return ReadResponceText(status, readStream);
            }
        }

        string HttpPost(string url, string data, out HttpStatusCode status)
        {
            HttpWebRequest request = (HttpWebRequest)System.Net.WebRequest.Create(url);

            request.Method = "POST";
            request.ContentLength = data.Length;
            request.ContentType = "application/x-www-form-urlencoded";

            using (Stream requestStream= request.GetRequestStream())
            {
                ASCIIEncoding ascii = new ASCIIEncoding();
                byte[] postBytes = ascii.GetBytes(data);
                requestStream.Write(postBytes, 0, postBytes.Length);
                requestStream.Flush();
                requestStream.Close();
            }

            return ReadResponce(request, out status);
        }

        void m_BackgrowndWorker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            for (;;)
            {
                object options= ajax_options;
                if (null==options)
                {
                    Thread.Sleep(10);
                }
                else
                {
                    ajax_options= null;
                    string url = (string)GetJSField(options, "url");
                    if (!url.StartsWith("http"))
                        url = "http://trust.dev" + url;
                    string type = (string)GetJSField(options, "type");
                    HttpStatusCode status = HttpStatusCode.InternalServerError;
                    string result_text= string.Empty;
                    if ("GET" == type)
                    {
                        result_text = HttpGet(url, out status);
                    }
                    else
                    {
                        object odata = GetJSField(options, "data");
                        string data = odata as string;
                        if (null != data)
                        {
                            result_text = HttpPost(url, data, out status);
                        }
                    }
                    m_Browser.Invoke(new System.Windows.Forms.MethodInvoker(delegate{
                        m_Browser.Document.InvokeScript("ajax_callback", new object[] { (int)status, result_text });
                    }));
                }
            }
        }

        static object GetJSField(object js_res, string field_name)
        {
            return js_res.GetType().InvokeMember(field_name, System.Reflection.BindingFlags.GetProperty, null, js_res, new object[] { });
        }
    }
}
