// данный файл содержит функции, предназначенные для взаимодействия ПАУ и раздела торгов

function safe_log(a)
{
	try
	{
		if (console && safe_log)
			safe_log(a);
	}
	catch (e)
	{
	}
}

// сохранить информацию о торгах в ПАУ
function global_external_AMA_SaveBiddings(biddings)
{
	safe_log('global_external_SaveBiddings {');
	safe_log(biddings);
	safe_log('global_external_SaveBiddings }');

	alert('global_external_AMA_SaveBiddings()');
}

// выгрузить информацию о торгах из ПАУ..
function global_external_AMA_LoadBiddings()
{
	var res= null;
	safe_log('global_external_AMA_LoadBiddings {');
	safe_log(res);
	safe_log('global_external_AMA_LoadBiddings }');

	alert('global_external_AMA_LoadBiddings()');

	return res;
}

/* получить информацию о конкурсной массе

вы формате типа:

[
			{
				id: 0
				, text: 'Здание многофункциональное к/н 47:14:1203001:145, пл. 1000 кв. м., по адресу: Удмуртская Республика, г. Ижевск, Песочная 13'
				, kmobject:
					{
						id: 0
						, СокращённоеНазвание: 'Здание многофункциональное к/н 47:14:1203001:145, пл. 1000 кв. м., по адресу: Удмуртская Республика, г. Ижевск, Песочная 13'
						, Группа: 'Недвижимость'
						, ОценочнаяСтоимость: 100000009
						, Информация_об_объекте:
							{
								Кадастровый_номер: '47:14:1203001:145'
								, Вид: 'Здание'
								, Назначение: 'Сооружение многофункциональное'
								, Площадь: 1000
								, Адрес: 'Удмуртская Республика, г. Ижевск, Песочная 13'
							}
						, Гос_регистрация_права:
							{
								Дата: '06.12.2011'
								, Номер: '71-АГ 555734'
								, Основание: 'Договор продажи №23'
								, Вид_права: 'Собственность'
							}
					}
			}
			,{
				id: 1
				, text: 'Котомка1'
				, kmobject:
					{
						id: 1
						, СокращённоеНазвание: 'ИЖ27175-036 2007 года Зелёный/Авокадо'
						, Группа: 'автотранспорт'
						, ОценочнаяСтоимость: 100000

						, Дата_постановка_на_учёт: '22.03.2011'
						, Модель: 'ИЖ27175-036'
						, Год_выпуска: 2007
						, Регистрационный_номер: 'А395ЕО18'
						, Цвет: 'Зелёный'
						, Гамма: 'Авокадо'
						, Номер:
							{
								  двигателя: 'B625149'
								, кузова: 'XWK272175'
								, шасси: '070016089'
							}
						, ПТС_номер: '18МН497990'
						, Свидетельство_номер: '18УО525192'
						, Мощность:
							{
								лс: 74.5
								,квт: 1568
							}
						, VIN: 'XWK27175070016089'
					}
			}
			, {
				id: 4
				, text: 'Котомка2'
				, kmobject:
					{
						id: 4
						, СокращённоеНазвание: 'ВАЗ21041-30 2009 года Синий/Калипсо'
						, Группа: 'автотранспорт'
						, ОценочнаяСтоимость: 100000

						, Дата_постановка_на_учёт: '22.03.2011'
						, Модель: 'ВАЗ21041-30'
						, Год_выпуска: 2009
						, Регистрационный_номер: 'А384ЕО18'
						, Цвет: 'Синий'
						, Гамма: 'Калипсо'
						, Номер:
							{
								двигателя: '9164726'
								, кузова: 'XWK21041'
								, шасси: '090069615'
							}
						, ПТС_номер: '18МТ073043'
						, Свидетельство_номер: '18УО525191'
						, Мощность:
							{
								лс: 74.1
								, квт: 1568
							}
						, VIN: 'XWK21041090069615'
					}
			}
			, {
				id: 2
				, text: 'Задолженность в 10000 рублей от ООО Талан'
				, kmobject:
					{
						id: 2
						, СокращённоеНазвание: 'Задолженность в 1000000 рублей от ООО Талан'
						, Группа: 'дебиторская задолженность'
						, ОценочнаяСтоимость: 100000
					}
			}
			, {
				id: 3
				, text: '300 куб м дров '
				, kmobject:
					{
						id: 3
						, СокращённоеНазвание: '300 куб м дров'
						, Группа: 'готовая продукция'
						, ОценочнаяСтоимость: 200000
					}
			}
		]

*/
function global_external_AMA_GetKMObjects(term)
{
	var res = [];
	safe_log('global_external_AMA_GetKMObjects {');
	safe_log(res);
	safe_log('global_external_AMA_GetKMObjects }');

	alert('global_external_AMA_GetKMObjects()');

	return res;
}

/*
получить информацию о текущей процедуре
в формате типа:
			{
				Должник:
					{
						Наименование: "ООО Роги и ноги"
						, ИНН: "6449013711"
						, ОГРН: "1026402000657"
					}
				, Арбитражный_управляющий:
					{
						Фамилия: "Парамонов"
						, Имя: "Парамон"
						, Отчество: "Парамонович"
						, ИНН: "5460000016"
					}
				, СРО: "НП СРО АУ Левое"
				, АС: "Левый арбитражный суд Тофаларской автономной области"
				, Номер_дела_о_банкротстве: "234534345"
			};
*/
function global_external_AMA_GetProcedureData()
{
	var res = {};
	safe_log('global_external_AMA_GetProcedureData() {');
	safe_log(res);
	safe_log('global_external_AMA_GetProcedureData() }');

	alert('global_external_AMA_GetProcedureData()');
	return res;
}

// перекрывает ajax транспорт..
$(function ()
{
	$.ajaxTransport
	(
		'+*',
		function (options, originalOptions, jqXHR)
		{
			var external_ajax_transport =
			{
				send: function (headers, completeCallback)
				{
					window.ajax_callback = function (status, responceText)
					{
						completeCallback(status, 200 === status ? 'success' : 'error', { text: responceText });
					}
					window.external.ajax(options);
				}
				, abort: function ()
				{
				}
			}
			return external_ajax_transport;
		}
	);
});