{
	"Версия_формата": "2",
	"Электронная_торговая_площадка": {
		"Наименование": "Межрегиональная Электронная Торговая Система",
		"IdTradePlace": "127"
	},
	"Банкротство": {
		"Должник": {
			"BankruptId": "295009",
			"Bankrupt_Category_Code": "SimplePerson",
			"Тип": "Физическое лицо",
			"Физическое_лицо": {
				"Фамилия": "Спичка",
				"Имя": "Юрий",
				"Отчество": "Иванович",
				"ИНН": "253908042463",
				"СНИЛС": "05843298994",
				"Дата_рождения": "21.05.1961",
				"Место_рождения": "с. Сергеевка, Славянского района, Донецской области"
			},
			"Адрес": "Приморский край, г. Владивосток, ул. Глинки, д. 13"
		},
		"Номер_дела_о_банкротстве": "А51-12739/2019",
		"Арбитражный_управляющий": {
			"Фамилия": "Грачёв",
			"Имя": "Артем",
			"Отчество": "Олегович",
			"ИНН": "254000411485",
			"СНИЛС": "14429391571",
			"Почтовый_адрес": "690106, гор. Владивосток-106, а/я 116",
			"СРО_АУ": {
				"Наименование": "ААУ \"ЦФОП АПК\" - Ассоциация арбитражных управляющих \"Центр финансового оздоровления предприятий агропромышленного комплекса\"",
				"ОГРН": "1107799002057",
				"ИНН": "7707030411",
				"Адрес": "107031, г Москва, ул. Б. Дмитровка, д. 32, стр. 1"
			}
		}
	},
	"Организатор_торгов": {
		"Информация_в_объявление_для_участников_торгов": {
			"адрес_электронной_почты": "",
			"номер_контактного_телефона": "",
			"почтовый_адрес": "",
			"дополнительные_сведения": ""
		}
	},
	"Форма_проведения_торгов": "Закрытый аукцион",
	"Форма_представления_предложений_цены": "открытая",
	"Торги_повторные": "false",
	"Лоты": [
		{
			"Номер": "1",
			"Состав_характеристики_описание": "Карабин Вепрь-308 308Win № ТН 3655 2007г.в. ",
			"Адрес_местонахождения": "",
			"Порядок_ознакомления": "",
			"Начальная_цена_в_рублях": "25000",
			"Величина_повышения_начальной_цены_в_рублях": "1250.00",
			"Размер_задатка_в_рублях": "5000.00",
			"Расчетные_формулы": {
				"Величина_повышения_начальной_цены": {
					"Процентов_от_начальной_цены": "5.00"
				},
				"Размер_задатка": {
					"Процентов_от_начальной_цены": "20"
				}
			},
			"КлассификацияЕФРСБ": [
				{
					"Код": "0104024",
					"Наименование": "Оружие спортивное, охотничье и военная техника двойного применения"
				}
			]
		},
		{
			"Номер": "2",
			"Состав_характеристики_описание": "Травматический пистолет ИЖ-78-9т кал. 9мм, №073381977 2007г.в. ",
			"Адрес_местонахождения": "",
			"Порядок_ознакомления": "",
			"Начальная_цена_в_рублях": "10000",
			"Величина_повышения_начальной_цены_в_рублях": "500.00",
			"Размер_задатка_в_рублях": "2000.00",
			"Расчетные_формулы": {
				"Величина_повышения_начальной_цены": {
					"Процентов_от_начальной_цены": "5.00"
				},
				"Размер_задатка": {
					"Процентов_от_начальной_цены": "20"
				}
			},
			"КлассификацияЕФРСБ": [
				{
					"Код": "0104024",
					"Наименование": "Оружие спортивное, охотничье и военная техника двойного применения"
				}
			]
		},
		{
			"Номер": "3",
			"Состав_характеристики_описание": "Малокалиберная винтовка CZ-M411 22WMR, № 00628. ",
			"Адрес_местонахождения": "",
			"Порядок_ознакомления": "",
			"Начальная_цена_в_рублях": "32000",
			"Величина_повышения_начальной_цены_в_рублях": "1600.00",
			"Размер_задатка_в_рублях": "6400.00",
			"Расчетные_формулы": {
				"Величина_повышения_начальной_цены": {
					"Процентов_от_начальной_цены": "5.00"
				},
				"Размер_задатка": {
					"Процентов_от_начальной_цены": "20"
				}
			},
			"КлассификацияЕФРСБ": [
				{
					"Код": "0104024",
					"Наименование": "Оружие спортивное, охотничье и военная техника двойного применения"
				}
			]
		},
		{
			"Номер": "4",
			"Состав_характеристики_описание": "Ружье Fabarm Lion H368  Max 4 кал. 12-76, № 7022175/8046321. ",
			"Адрес_местонахождения": "",
			"Порядок_ознакомления": "",
			"Начальная_цена_в_рублях": "25000",
			"Величина_повышения_начальной_цены_в_рублях": "1250.00",
			"Размер_задатка_в_рублях": "5000.00",
			"Расчетные_формулы": {
				"Величина_повышения_начальной_цены": {
					"Процентов_от_начальной_цены": "5.00"
				},
				"Размер_задатка": {
					"Процентов_от_начальной_цены": "20"
				}
			},
			"КлассификацияЕФРСБ": [
				{
					"Код": "0104024",
					"Наименование": "Оружие спортивное, охотничье и военная техника двойного применения"
				}
			]
		},
		{
			"Номер": "5",
			"Состав_характеристики_описание": "Карабин Browning Long Trac 30-06 Spr., № 311MN17119.",
			"Адрес_местонахождения": "",
			"Порядок_ознакомления": "",
			"Начальная_цена_в_рублях": "125000",
			"Величина_повышения_начальной_цены_в_рублях": "6250.00",
			"Размер_задатка_в_рублях": "25000.00",
			"Расчетные_формулы": {
				"Величина_повышения_начальной_цены": {
					"Процентов_от_начальной_цены": "5.00"
				},
				"Размер_задатка": {
					"Процентов_от_начальной_цены": "20"
				}
			},
			"КлассификацияЕФРСБ": [
				{
					"Код": "0104024",
					"Наименование": "Оружие спортивное, охотничье и военная техника двойного применения"
				}
			]
		},
		{
			"Номер": "6",
			"Состав_характеристики_описание": "Ружье Бекас-авто кал.12/76, № ХХ5054. ",
			"Адрес_местонахождения": "",
			"Порядок_ознакомления": "",
			"Начальная_цена_в_рублях": "25000",
			"Величина_повышения_начальной_цены_в_рублях": "1250.00",
			"Размер_задатка_в_рублях": "5000.00",
			"Расчетные_формулы": {
				"Величина_повышения_начальной_цены": {
					"Процентов_от_начальной_цены": "5.00"
				},
				"Размер_задатка": {
					"Процентов_от_начальной_цены": "20"
				}
			},
			"КлассификацияЕФРСБ": [
				{
					"Код": "0104024",
					"Наименование": "Оружие спортивное, охотничье и военная техника двойного применения"
				}
			]
		}
	],
	"Регламенты": {
		"Регламент_проведения_торгов": {
			"Оформление_участия": {
				"Порядок_оформления_участия_в_торгах": "",
				"Требования_к_оформлению_документов": "",
				"Перечень_представляемых_участниками_торгов_документов": []
			},
			"Представление_заявок_об_участии": {
				"Порядок": "",
				"Срок_и_время": {
					"начало": {
						"дата": "22.12.2021",
						"время": "00:00"
					},
					"окончание": {
						"дата": "03.02.2022",
						"время": "23:59"
					}
				}
			},
			"Внесение_задатков": {
				"Порядок": "",
				"Оговорка_о_возврате": "",
				"Реквизиты_для_перечисления_денежных_средств": {
					"Текстом": ""
				},
				"Сроки": {
					"текстом": ""
				}
			},
			"Допуск_к_торгам": {
				"Порядок": ""
			},
			"Представление_предложений_о_цене": {
				"Срок_и_время": {
					"начало": {
						"дата": "09.02.2022",
						"время": "10:00"
					}
				}
			}
		},
		"Регламент_завершения_торгов": {
			"Выявление_победителей": {
				"Порядок": "",
				"Критерии": ""
			},
			"Подведение_результатов": {
				"Дата_и_время": {
					"дата": "09.02.2022",
					"время": "10:00"
				},
				"Порядок": "",
				"Место": ""
			}
		},
		"Регламент_купли_продажи": {
			"Заключение_договора": {
				"Порядок": "",
				"Срок": ""
			},
			"Платежи": {
				"Сроки": "",
				"Реквизиты_для_перечисления_денежных_средств": {
					"Текстом": ""
				},
				"Оговорка_об_учете_задатка": ""
			},
			"Передача_имущества": {
				"Порядок": "",
				"Оговорка_о_расходах_на_регистрацию_права_собственности": ""
			}
		}
	},
	"Опубликованные_объявления_о_проведении_торгов": {
		"Объявление_о_проведении_торгов_в_ЕФРСБ": {
			"Номер": "7898555",
			"дата": "20.12.2021",
			"MessageGUID": "BFDE2727DE7AC988CE24F2C375B94506",
			"Id": "7898555",
			"Text": "Организатор торгов – финансовый управляющий Спичка Юрия Ивановича (д.р. 21.05.1961г., место рождения: с. Сергеевка Славянского района Донецской области, ИНН 253908042463, СНИЛС 058-432-989-94, место жительства: г. Владивосток, ул. Глинки, д.13) Грачёв Артем Олегович (ИНН 254000411485, СНИЛС 144-293-915 71), член ААУ «ЦФОП АПК» (ОГРН 1107799002057, ИНН 7707030411, г. Москва, ул. Б.Дмитровка,д.32,стр.1), тел.: 8 (953) 211-25-98, e-mail: grachev.ao@inbox.ru, адрес: 690106, г.Владивосток-106, а/я 116, действующий на основании Определения Арбитражного суда Приморского края от 23.06.2021г. по делу № А51-12739/2019 сообщает о проведении «09» февраля 2022 года в 10–00 ч. (время Московское, далее Мск.) закрытых электронных торгов в форме аукциона с открытой формой подачи предложения о цене имущества Спичка Юрия Ивановича на электронной площадке оператора торгов ООО «МЭТС», сайт площадки https://www.m-ets.ru. На торги выставляется следующее имущество должника:\nЛот 1. Карабин Вепрь-308 308Win № ТН 3655 2007г.в. Начальная цена: 25 000 рублей. Лот 2. Травматический пистолет ИЖ-78-9т кал. 9мм, №073381977 2007г.в. Начальная цена: 10 000 рублей.\nЛот 3. Малокалиберная винтовка CZ-M411 22WMR, № 00628. Начальная цена: 32 000 рублей.\nЛот 4. Ружье Fabarm Lion H368  Max 4 кал. 12-76, № 7022175/8046321. Начальная цена: 25 000 рублей.\nЛот 5. Карабин Browning Long Trac 30-06 Spr., № 311MN17119. Начальная цена: 125 000 рублей.\nЛот 6. Ружье Бекас-авто кал.12/76, № ХХ5054. Начальная цена: 25 000 рублей.\nПрием заявок будет осуществляться с 00:00 (Мск.) 22.12.2021 г. по 23:59 (Мск.) 03.02.2022 г. Шаг аукциона – 5 % от начальной продажной цены Имущества.\nК участию в закрытых торгах допускаются лица, соответствующие требованиям, указанным в статье 20 ФЗ «Об оружии» № 150-ФЗ от 13.12.1996г., которые могут быть признаны покупателями имущества по законодательству РФ, своевременно подавшие заявку на участие в закрытых торгах в электронном виде, а так же своевременно перечислившие задаток на счет Спичка Юрия Ивановича.\nОбязательные требования к участникам закрытых торгов: Наличие у участника торгов действующей лицензии на торговлю гражданским и служебным оружием или на коллекционирование или экспонирование оружия (для юридических лиц). Наличие у участника торгов действующей лицензии на приобретение оружия, его коллекционирование или экспонирование (для физических лиц). Наличие у участника торгов статуса государственной военизированной организации.\nК заявке на участие в торгах участники представляют следующие документы: Юридические лица - выписку из ЕГРЮЛ. Индивидуальные предприниматели - выписку из ЕГРИП. Физические лица - копию документа, удостоверяющего личность. Также к заявке прилагается документ, подтверждающий полномочия лица на осуществление действий от имени заявителя, копия платежного поручения об оплате задатка, а так же действующая лицензия на торговлю гражданским и служебным оружием или на коллекционирование или экспонирование оружия (для юридических лиц), действующая лицензия на приобретение оружия, его коллекционирование или экспонирование (для физических лиц).\nДокументы, прилагаемые к заявке, представляются в форме электронных документов, подписанных электронной подписью заявителя. Заявка на участие в торгах оформляется заявителем произвольно в электронной форме на сайте площадки на русском языке и должна содержать: наименование, организационно-правовую форму, место нахождения, почтовый адрес заявителя (для юридического лица); фамилия, имя, отчество, паспортные данные, сведения о месте жительства заявителя (для физического лица); номер контактного телефона, адрес электронной почты заявителя; сведения о наличии или об отсутствии заинтересованности заявителя по отношению к должнику, кредиторам, финансовому управляющему и о характере этой заинтересованности; сведения об участии в капитале заявителя финансового управляющего, а также саморегулируемой организации арбитражных управляющих, членом или руководителем которой является финансовый управляющий. Заявка должна содержать обязательство соблюдать требования, указанные в сообщении о проведении торгов.\nОзнакомление участников с условиями, характеристиками имущества и документацией по адресу регистрации Спичка Юрия Ивановича: Приморский край, г. Владивосток, ул. Глинки, д.13.\nДля участия заявитель вносит задаток в срок не позднее 03.02.2022 г. за Лот № 1-6 20% от начальной цены лота на счет Спичка Юрия Ивановича (счет № 40817810950002497247, открытый в Дальневосточный банк ПАО «Сбербанк», кор/счет № 30101810600000000608, БИК 040813608). Задаток считается внесенным по факту поступления денежных средств на счет Спичка Юрия Ивановича. Победителем аукциона признается участник, предложивший наиболее высокую цену. Итоги торгов будут подведены на сайте оператора электронной площадки https://www.m-ets.ru в день проведения торгов. В течение 5 дней с даты подписания протокола организатор торгов направляет победителю предложение заключить договор купли-продажи. Оплата по договору – в течении 30 календарных дней с момента подписания на счет Спичка Юрия Ивановича (счет № 40817810950000413964, открытый в Дальневосточный банк ПАО «Сбербанк», кор/счет № 30101810600000000608, БИК 040813608).\n"
		}
	},
	"Приложения": [
		{
			"Файл": "Договор о задатке.docx",
			"Hash": "B3E613D55726AA046EF61B729F7C761D35818A7463ACDEBFE3B6C0A69D1BBAF54DCC8ACBCF72920A0179613A55AF1E22AAF5CF54E223517CB9DCCFDC9D99CE3D"
		},
		{
			"Файл": "Проект договора купли-продажи.docx",
			"Hash": "4CF920E3E6A069526FFBBB183500019128CF7D09AC8948DAACE35D0A07E2E61E5A6C4EF1E61672AB1842D93C9E9F48F722825D1C77CC65CC69910FACE76CA316"
		}
	]
}