@pushd %~dp0

@set JSPATH=..\..\scripts\
@set XSDPATH=..\..\..\..\src\uc\forms\ama\biddings\base\cfb\v2\

@call CScript //nologo %JSPATH%test_xsd.js 0_cfb.xml              %XSDPATH%cfb.xsd 0_cfb.result.xml
@call CScript //nologo %JSPATH%test_xsd.js 6_efrsb2cfb.etalon.xml %XSDPATH%cfb.xsd 6_efrsb2cfb.result.xml

@set XSDPATH=..\..\..\..\src\uc\forms\ama\biddings\base\mets\
@call CScript //nologo %JSPATH%test_xsd.js 11_encode_cfb2mets.etalon.xml %XSDPATH%mets.xsd 11_encode_cfb2mets.result.xml

@popd