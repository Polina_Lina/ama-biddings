del transit\prep-transit.sql

call :prep-from-dir CA_ALOT_7906091
call :prep-from-dir CA_METS_7898555
call :prep-from-dir CC_SAST_4639277
call :prep-from-dir CO_METS_7898030
call :prep-from-dir OA_ALOT_7871624
call :prep-from-dir OA_ALOT_7876355
call :prep-from-dir OA_ALOT_7879100
call :prep-from-dir OA_METS_7733055
call :prep-from-dir OA_METS_7850161
call :prep-from-dir OA_METS_7850500
call :prep-from-dir OA_SAST_7791077
call :prep-from-dir OA_SAST_7842829
call :prep-from-dir OA_SAST_7866887
call :prep-from-dir OC_METS_7818784
call :prep-from-dir PO_ALOT_7899555
call :prep-from-dir PO_METI_7941187
call :prep-from-dir PO_METS_7906480
call :prep-from-dir PO_SAST_7862143

exit

:prep-from-dir
rem copy %1\0_cfb.xml                     transit\%1.cfb.xml
rem copy %1\11_encode_cfb2mets.etalon.xml transit\%1.mets.xml

set txt=alter table TransitBidding change column body body longblob;
echo %txt% >> transit\prep-transit.sql

set txt=insert into TransitBidding set token_bidding='%1.cfb',  body='
echo %txt% >> transit\prep-transit.sql

copy transit\prep-transit.sql + %1\0_cfb.xml transit\prep-transit.sql

set txt=';
echo %txt% >> transit\prep-transit.sql

set txt=insert into TransitBidding set token_bidding='%1.mets', body='
echo %txt% >> transit\prep-transit.sql

copy transit\prep-transit.sql + %1\11_encode_cfb2mets.etalon.xml transit\prep-transit.sql

set txt=';
echo %txt% >> transit\prep-transit.sql

exit /B