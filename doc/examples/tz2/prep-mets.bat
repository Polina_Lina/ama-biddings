set dirname=for-mets
mkdir %dirname%

rd /S /Q %dirname%

call :prep-from-dir CA_ALOT_7906091
call :prep-from-dir CA_METS_7898555
call :prep-from-dir CC_SAST_4639277
call :prep-from-dir CO_METS_7898030
call :prep-from-dir OA_ALOT_7871624
call :prep-from-dir OA_ALOT_7876355
call :prep-from-dir OA_ALOT_7879100
call :prep-from-dir OA_METS_7733055
call :prep-from-dir OA_METS_7850161
call :prep-from-dir OA_METS_7850500
call :prep-from-dir OA_SAST_7791077
call :prep-from-dir OA_SAST_7842829
call :prep-from-dir OA_SAST_7866887
call :prep-from-dir OC_METS_7818784
call :prep-from-dir PO_ALOT_7899555
call :prep-from-dir PO_METI_7941187
call :prep-from-dir PO_METS_7906480
call :prep-from-dir PO_SAST_7862143

copy ..\..\..\src\uc\forms\ama\biddings\base\cfb\v2\cfb.xsd %dirname%\
copy ..\..\..\src\uc\forms\ama\biddings\base\mets\mets.xsd %dirname%\
copy readme-mets.txt %dirname%\readme.txt

exit

:prep-from-dir

mkdir %dirname%\%1
copy %1\0_cfb.xml                     %dirname%\%1\%1.cfb.xml
copy %1\11_encode_cfb2mets.etalon.xml %dirname%\%1\%1.mets.xml
copy %1\0_efrsb.xml                   %dirname%\%1\%1.efrsb.xml
exit /B