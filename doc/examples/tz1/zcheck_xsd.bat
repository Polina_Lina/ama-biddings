@pushd %~dp0

@set JSPATH=..\scripts\
@set XSDPATH=..\..\..\src\uc\forms\ama\biddings\base\cfb\v1\

@call CScript //nologo %JSPATH%test_xsd.js bidding1.xml %XSDPATH%cfb.xsd bidding1.result.xml

@set XSDPATH=..\..\..\src\uc\forms\ama\biddings\base\cfb\v2\

@call CScript //nologo %JSPATH%test_xsd.js bidding1-v2.etalon.xml %XSDPATH%cfb.xsd bidding1-v2.result.xml

@popd